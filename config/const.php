<?php

use App\Admin;
use App\Manager;
use App\User;

return [
    'message' => [
        'messagables' => [
            User::class,
            Manager::class,
            Admin::class
        ]
    ],
    'apartment' => [
        'type' => [
            1 => 'Nhà trọ bình dân',
            2 => 'Ký túc xá, homestay',
            3 => 'Chung cư',
            4 => 'Chung cư mini',
        ]
    ],
    'room' => [
        'public_room' => 1,
        'private_room' => 2,
        'types' => [
            1 => 'Vệ sinh khép kín',
            2 => 'Vệ sinh chung',
        ],
        'ultilities' => [
            1 => 'Khu giặt giũ, phơi phóng',
            2 => 'Truyền hình',
            3 => 'Internet',
            4 => 'Nơi để xe máy',
            5 => 'Bếp nấu ăn',
            6 => 'Bình nóng lạnh',
            7 => 'Điều hoà',
            8 => 'Tủ lạnh',
            9 => 'Máy giặt',
            10 => 'Giường ngủ',
            11 => 'Tủ quần áo',
        ],
        'statuses' => [
            'empty' => 1,
            'leased' => 2,
            'unavailable' => 3,
        ]
    ],
    'payment' => [
        'recurring' => [
            1 => 'Hàng tháng',
            2 => '3 tháng',
            3 => '6 tháng',
            4 => '1 năm',
            5 => 'Thoả thuận'
        ],
    ],
    'search' => [
        'sort_by' => [
            '' => 'acreage'
        ]
    ],
    'invoice' => [
        'types' => [
            'all' => [
                'code' => "",
                'text' => 'Tất cả'
            ],
            'house_cost' => [
                'code' => "App\\Contract",
                'text' => 'Tiền nhà'
            ],
            'living_cost' => [
                'code' => "App\\LivingCost",
                'text' => 'Tiền sinh hoạt phí'
            ],
        ],
        'statuses' => [
            'all' => [
                'code' => 0,
                'text' => 'Tất cả'
            ],
            'paid' => [
                'code' => 1,
                'text' => 'Đã thanh toán'
            ],
            'not_paid' => [
                'code' => 2,
                'text' => 'Chưa thanh toán'
            ],
        ]
    ],
    'gender' => [
        'types' => [
            [
                'code' => 1,
                'text' => 'Nữ',
            ],
            [
                'code' => 2,
                'text' => 'Nam',
            ],
        ]
    ]
];
