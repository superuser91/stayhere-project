<?php

namespace App\Notifications;

use App\Invoice;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\Notification as MessagingNotification;
use App\Fcm\FcmChannel;

class NewLivingInvoiceNotification extends Notification
{
    use Queueable;

    public $invoice;
    public $months;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $months = [];
        foreach (array_column($this->invoice->months, 'month') as $month) {
            $months[] = Carbon::parse($month)->format('m/Y');
        }
        $this->months = $months;
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $data = $this->toArray($notifiable);

        $notification = MessagingNotification::create('Thông báo hệ thống')
            ->withBody('Thông báo thu tiền sinh hoạt phí tháng ' . implode($this->months));

        return [
            'data' => $data,
            'notification' => $notification
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'human_readable' => 'Thông báo thu tiền sinh hoạt phí tháng ' . implode($this->months),
            'invoiceId' => $this->invoice->id,
            'deeplink' => 'stayhere://LivingInvoice',
            'dataType' => 'LivingInvoice'
        ];
    }
}
