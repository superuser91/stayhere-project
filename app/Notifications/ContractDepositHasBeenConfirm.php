<?php

namespace App\Notifications;

use App\Fcm\FcmChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\Notification as MessagingNotification;

class ContractDepositHasBeenConfirm extends Notification
{
    use Queueable;

    public $contract;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contract)
    {
        $this->contract = $contract;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $data = $this->toArray($notifiable);

        $notification = MessagingNotification::create('Thông báo hệ thống')
            ->withBody('Chủ nhà xác nhận bạn đã đóng tiền cọc');

        return [
            'data' => $data,
            'notification' => $notification
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'human_readable' => 'Chủ nhà xác nhận bạn đã đóng tiền cọc',
            'contractId' => $this->contract->id,
            'deeplink' => 'stayhere://Contract',
            'dataType' => 'Contract'
        ];
    }
}
