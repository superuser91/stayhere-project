<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\Notification as MessagingNotification;
use App\Fcm\FcmChannel;


class OrderHasBeenAccepted extends Notification
{
    use Queueable;

    public $roomRequest;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($roomRequest)
    {
        $this->roomRequest = $roomRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $data = $this->toArray($notifiable);

        $notification = MessagingNotification::create('Thông báo hệ thống')
            ->withBody('Yêu cầu thuê phòng đã được chấp nhận');

        return [
            'data' => $data,
            'notification' => $notification
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'human_readable' => 'Yêu cầu thuê phòng đã được chấp nhận',
            'roomRequestId' => $this->roomRequest->id,
            'deeplink' => 'stayhere://RoomRequest',
            'dataType' => 'RoomRequest'
        ];
    }
}
