<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\Notification as MessagingNotification;
use App\Fcm\FcmChannel;

class NewPublishRoom extends Notification
{
    use Queueable;

    public $room;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($room)
    {
        $this->room = $room;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', FcmChannel::class];
    }

    public function toFcm($notifiable)
    {
        $data = $this->toArray($notifiable);

        $notification = MessagingNotification::create('Thông báo hệ thống')
            ->withBody('[QC] Phòng trọ mới tại: ' . $this->room->apartment->location['desc']);

        return [
            'data' => $data,
            'notification' => $notification
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'human_readable' => '[QC] Phòng trọ mới tại: ' . $this->room->apartment->location['desc'],
            'roomId' => $this->room->id,
            'deeplink' => 'stayhere://Room',
            'dataType' => 'Room'
        ];
    }
}
