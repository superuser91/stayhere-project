<?php

namespace App\View;

use App\Notifications\NewMessageNotification;
use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class NotificationComposer
{
    public function __construct()
    {
        // $this->var = $var;
    }

    public function compose(View $view)
    {
        $roomRequestsCount = Order::whereHas('room', function ($room) {
            $room->whereHas('apartment', function ($apartment) {
                $apartment->where('manager_id', auth()->id());
            });
        })->count();

        $unreadMessages = auth()->user()->unreadNotifications()
            ->where('type', NewMessageNotification::class)
            ->select(
                'data->message->messagable_type as messagable_type',
                'data->message->messagable_id as messagable_id',
            )
            ->get()
            ->countBy(function ($email) {
                return $email->messagable_type . '::' . $email->messagable_id;
            });;

        $notifications = auth()->user()->notifications()
            ->whereNotIn('type', [NewMessageNotification::class])
            ->latest()
            ->take(10)
            ->get();

        $unreadNotificationsCount = auth()->user()->unreadNotifications()
            ->whereNotIn('type', [NewMessageNotification::class])
            ->count();

        $view->with([
            'unreadNotificationsCount' => $unreadNotificationsCount,
            'notifications' => $notifications,
            'roomRequestsCount' => $roomRequestsCount,
            'unreadMessages' => $unreadMessages,
        ]);
    }
}
