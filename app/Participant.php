<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $fillable = [
        'conversation_id',
        'messagable_type',
        'messagable_id',
    ];

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function messagable()
    {
        return $this->morphTo();
    }
}
