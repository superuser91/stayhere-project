<?php

namespace App;

use App\Notifications\NewMessageNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Conversation extends Model
{
    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->latest();
    }

    public function recentMessages()
    {
        return $this->hasMany(Message::class)->latest()->take(15);
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->latest();
    }

    public function getUnreadMessagesAttribute()
    {
        return auth()->user()->unreadNotifications()
            ->where('type', NewMessageNotification::class)
            ->where('data->message->conversation_id', $this->id)
            ->select('data->message->data as message')
            ->get();
    }
}
