<?php

namespace App;

use App\Traits\FcmNotifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Manager extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'address',
        'phone_number',
        'social_medias',
        'avatar',
    ];

    protected $casts = [
        'fcm_tokens' => 'array'
    ];

    protected $hidden = [
        'password'
    ];

    public function receivesBroadcastNotificationsOn()
    {
        return 'manager.' . $this->id;
    }

    public function toSearchableArray()
    {
        $array =  $this->only([
            'id',
            'name',
            'email',
            'phone_number',
            'avatar'
        ]);

        $array['messagable_type'] = get_class($this);

        return $array;
    }

    public function messages()
    {
        return $this->morphToMany(Message::class, 'messagable');
    }

    public function participants()
    {
        return $this->morphMany(Participant::class, 'messagable');
    }

    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }

    public function profile()
    {
        return $this->hasOne(ManagerProfile::class);
    }
}
