<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ultility extends Model
{
    protected $fillable = [
        'name',
        'icon',
        'app_icon_path'
    ];
}
