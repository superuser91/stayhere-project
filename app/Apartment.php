<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Apartment extends Model
{
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'manager_id',
        'location',
        'description',
        'ultilities',
        'images',
        'province_id',
        'district_id',
        'ward_id',
        'thumbnail',
    ];

    protected $casts = [
        'location' => 'array',
        'description' => 'array',
        'images' => 'array'
    ];

    protected $touches = ['rooms'];

    public function toSearchableArray()
    {
        $this->load(['publicRooms', 'manager']);

        $array = $this->toArray();

        $array['_geoloc'] = [
            'lat' => $array['location']['lat'],
            'lng' => $array['location']['lon'],
        ];

        $array['public_rooms_count'] = $this->publicRooms->count();

        return $array;
    }

    public function manager()
    {
        return $this->belongsTo(Manager::class);
    }

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function publicRooms()
    {
        return $this->hasMany(Room::class)
            ->where('is_public', config('const.room.public_room'));
    }

    public function emptyRooms()
    {
        return $this->hasMany(Room::class)
            ->whereNull('contract_id');
    }

    public function leasedRooms()
    {
        return $this->hasMany(Room::class)
            ->whereNotNull('contract_id');
    }
}
