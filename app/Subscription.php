<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'user_id',
        'location',
        'radius',
        'min_acreage',
        'max_acreage',
        'min_budget',
        'max_budget',
        'is_active',
    ];

    protected $casts = [
        'location' => 'json'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
