<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteRoom extends Model
{
    protected $fillable = [
        'user_id',
        'room_id',
    ];
}
