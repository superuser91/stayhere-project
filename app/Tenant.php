<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $fillable = [
        'room_id',
        'full_name',
        'birth_date',
        'gender',
        'province_id',
        'district_id',
        'ward_id',
        'user_id',
    ];

    protected $casts = [
        'infomation' => 'json'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
