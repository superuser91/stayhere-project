<?php

namespace App\Providers;

use App\Messagable;
use App\View\NotificationComposer;
use DebugBar\DebugBar;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Messagable::bootSearchable();

        view()->composer('manager.layouts.app', NotificationComposer::class);
    }
}
