<?php

namespace App\Http\Middleware;

use Closure;

class CheckFilledProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(is_null($request->user('manager')->profile)){
            return redirect();
        }

        return $next($request);
    }
}
