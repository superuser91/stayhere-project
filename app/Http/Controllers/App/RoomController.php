<?php

namespace App\Http\Controllers\App;

use App\FavoriteRoom;
use App\Room;
use Illuminate\Http\Request;

class RoomController
{
    public function show(Request $request, Room $room)
    {
        $room->load('apartment.manager');

        return view('app.rooms.show')->with([
            'room' => $room
        ]);
    }

    public function addFavoriteRoom(Request $request, $id)
    {
        FavoriteRoom::firstOrCreate([
            'user_id' => auth()->id(),
            'room_id' => $id
        ]);

        return back();
    }

    public function removeFavoriteRoom(Request $request, $id)
    {
        FavoriteRoom::where('user_id', auth()->id())
            ->where('room_id', $id)
            ->delete();

        return back();
    }
}
