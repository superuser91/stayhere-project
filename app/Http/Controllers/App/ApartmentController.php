<?php

namespace App\Http\Controllers\App;

use App\Apartment;
use Illuminate\Http\Request;

class ApartmentController
{
    public function show(Request $request, Apartment $apartment)
    {
        $apartment->load(['emptyRooms', 'manager']);

        return view('app.apartments.show')->with([
            'apartment' => $apartment
        ]);
    }
}
