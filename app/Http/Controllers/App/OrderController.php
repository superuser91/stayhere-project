<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Notifications\UserRequestRoom;
use App\Order;
use App\Room;
use Illuminate\Http\Request;
use Kreait\Firebase\Messaging\CloudMessage;

class OrderController extends Controller
{
    public function createOrder(Request $request, $id)
    {
        $room = Room::where('id', $id)->with('apartment.manager')->first();

        $order = Order::firstOrCreate([
            'user_id' => auth()->id(),
            'room_id' => $id,
            'manager_approve_at' => null
        ]);

        $room->apartment->manager->notify(new UserRequestRoom(auth()->user(), $room));

        return back();
    }
}
