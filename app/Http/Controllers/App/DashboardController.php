<?php

namespace App\Http\Controllers\App;

use App\Apartment;
use App\Http\Controllers\Controller;
use App\Services\Apartment\RecommendationService;
use App\Services\Room\SearchService;
use App\Utils\JsonDB;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;

class DashboardController extends Controller
{
    public function index(
        Request $request,
        SearchService $searchService,
        RecommendationService $recommendationService
    ) {
        $locations = json_decode(JsonDB::locations(), true);

        if (isset($request['address'])) {
            $apartments = $searchService->searchBuilder($request->all())->get();
        } else {
            $geoCodingInformation = geoip($request->ip());
            $apartments = $recommendationService->recommendationBuilder($geoCodingInformation)->paginate();
        }

        $data = Apartment::whereIn('id', $apartments->pluck('id'))
            ->with('publicRooms', 'manager')
            ->paginate();

        // dd($data);

        return view('app.dashboard')->with([
            'locations' => $locations['data'],
            'apartments' => $data
        ]);
    }
}
