<?php

namespace App\Http\Controllers\App;

use App\Contract;
use App\Http\Controllers\Controller;
use App\Notifications\UserAcceptContract;
use App\Notifications\UserDeclineContract;
use App\Tenant;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    public function acceptContract(Request $request, Contract $contract)
    {
        if (auth()->id() != $contract->user_id) {
            abort(403);
        }

        $contract->load(['room.apartment.manager', 'order']);

        $contract->update([
            'user_confirm_at' => now()
        ]);

        $contract->room->update([
            'tenant_id' => auth()->id(),
            'is_public' => config('const.room.private_room')
        ]);

        $contract->room->apartment->searchable();

        $contract->order->delete();

        $contract->room->apartment->manager->notify(new UserAcceptContract($contract));

        $tenant = Tenant::updateOrCreate([
            'room_id' => $contract->room->id,
            'user_id' => auth()->id(),
        ], [
            'full_name' => auth()->user()->name,
            'birth_date' => auth()->user()->profile->birth_date ?? null,
            'gender' =>  auth()->user()->profile->gender ?? null,
            'province_id' =>  auth()->user()->profile->province_id ?? null,
            'district_id' =>  auth()->user()->profile->district_id ?? null,
            'ward_id' =>  auth()->user()->profile->ward_id ?? null,
        ]);

        return dd($contract);
    }

    public function declineContract(Request $request, Contract $contract)
    {
        if (auth()->id() != $contract->user_id) {
            abort(403);
        }

        $contract->load(['room.apartment.manager']);

        $contract->delete();

        $contract->room->apartment->manager->notify(new UserDeclineContract($contract));

        return back();
    }
}
