<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\UserRequestRoom;
use App\Order;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * @OA\Get(
     *      path="/orders",
     *      operationId="getOrderList",
     *      tags={"Order"},
     *      summary="getOrderList",
     *      description="getOrderList",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     *
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::where('user_id', auth()->id())
            ->with(['room.apartment.manager', 'contract' => function ($contract) {
                $contract->withTrashed();
            }])
            ->withTrashed()
            ->latest()
            ->paginate();
    }

    /**
     * @OA\POST(
     *      path="/orders",
     *      operationId="createOrder",
     *      tags={"Order"},
     *      summary="createOrder",
     *      description="createOrder",
     *      @OA\Parameter(
     *          name="room_id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="price",
     *          in="query",
     *          @OA\Schema(
     *              type="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_confirmed",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_purchased",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="order_last",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function createOrder(Request $request, $id)
    {
        $room = Room::where('id', $id)->with('apartment.manager')->first();

        $order = Order::where('user_id', auth()->id())
            ->where('room_id', $id)
            ->first();

        if (is_null($order)) {
            $order = Order::create([
                'user_id' => auth()->id(),
                'room_id' => $id,
            ]);

            $room->apartment->manager->notify(new UserRequestRoom(auth()->user(), $room));
        }

        return $order;
    }

    /**
     * @OA\Get(
     *      path="/orders/{id}",
     *      operationId="showOrder",
     *      tags={"Order"},
     *      summary="showOrder",
     *      description="showOrder",
     *      @OA\Parameter(
     *          name="id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with([
            'room.apartment.manager',
            'contract' => function ($contract) {
                $contract->withTrashed();
            }
        ])->withTrashed()
            ->find($id);

        if (!$order) {
            return response()->json([
                'error' => true,
                'message' => 'Not found'
            ], 404);
        }

        return $order;
    }

    public function update(Request $request, Order $order)
    {
        $validation = Validator::make($request->all(), [
            'room_id' => 'required|exists:rooms,id',
            'price' => 'required|min:0',
            'is_confirmed' => 'nullable',
            'is_purchased' => 'nullable',
            'order_last' => 'nullable',
        ]);

        $request['user_id'] = auth()->id();

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return response()->json([], 204);
    }
}
