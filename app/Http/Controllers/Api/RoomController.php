<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\UserRequestRoom;
use App\Order;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller
{
    public function show(Request $request, $id)
    {
        $room = Room::with(['apartment.manager'])
            ->where('is_public', config('const.room.public_room'))
            ->where('id', $id)
            ->first();

        if (is_null($room)) {
            return response()->json([
                'message' => "Phòng này hiện tại không khả dụng!"
            ], 404);
        }

        return $room;
    }
}
