<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);

        if (!$review) {
            return response()->json([
                'error' => true,
                'message' => 'Not found'
            ]);
        }

        return $review;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $validation = Validator::make($request->all(), [
            'reviewable_type',
            'reviewable_id',
            'score' => 'required|numeric',
            'comment' => 'nullable|max:1000',
            'images' => 'nullable|array',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return $review->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Review::destroy($id);
    }
}
