<?php

/**
 * @OA\Info(
 * version="1.0.0",
 * title="Stayhere API",
 * description="Stayhere description",
 * @OA\Contact(
 * email="dung199551@gmail.com"
 * ),
 * @OA\License(
 * name="Apache 2.0",
 * url="http://www.apache.org/licenses/LICENSE-2.0.html"
 * )
 * )
 */

/**
 * @OA\Server(
 * url="http://api.stayhere.tk/v1",
 * description="Stayhere API v1"
 * )
 */

/** @OA\SecurityScheme(
 * securityScheme="bearerAuth",
 * in="header",
 * name="bearerAuth",
 * type="http",
 * scheme="bearer",
 * )
 */
