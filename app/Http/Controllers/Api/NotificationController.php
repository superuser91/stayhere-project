<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Notifications\NewMessageNotification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $notifications = auth()->user()->notifications()
            ->whereNotIn('type', [NewMessageNotification::class])
            ->latest()
            ->paginate();

        return $notifications;
    }

    public function unreadMessages(Request $request)
    {
        $unreadMessages = auth()->user()->unreadNotifications()
            ->where('type', NewMessageNotification::class)
            ->select(
                'data->message->messagable_type as messagable_type',
                'data->message->messagable_id as messagable_id',
            )
            ->get()
            ->countBy(function ($email) {
                return $email->messagable_type . '::' . $email->messagable_id;
            });;

        return $unreadMessages;
    }

    public function markAsRead(Request $request, $id)
    {
        return auth()->user()->unreadNotifications()
            ->where('id', $id)
            ->update(['read_at' => now()]);
    }

    public function markAllAsRead(Request $request, $id)
    {
        return auth()->user()->unreadNotifications()
            ->markAsRead();
    }
}
