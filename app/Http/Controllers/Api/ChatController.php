<?php

namespace App\Http\Controllers\Api;

use App\Conversation;
use App\Manager;
use App\Message;
use App\Notifications\NewMessageNotification;
use App\Participant;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController
{
    public function listConversation()
    {
        $conversations = Conversation::has('messages')
            ->whereHas('participants', function ($participant) {
                $participant->where('messagable_type', User::class)
                    ->where('messagable_id', auth()->id());
            })
            ->orderBy('updated_at', 'desc')
            ->with([
                'participants' => function ($messagable) {
                    $messagable->where('messagable_type', '!=', User::class)
                        ->orWhere('messagable_id', '!=', auth()->id());
                },
                'participants.messagable',
                'lastMessage',
            ])
            ->get();

        $conversations = $conversations->map(function ($conversation) {
            return $conversation->load('recentMessages');
        });

        return response()->json([
            'data' => $conversations
        ]);
    }

    public function getConversation(Request $request, $id)
    {
        $conversation = Conversation::with([
            'participants' => function ($messagable) {
                $messagable->where('messagable_type', '!=', User::class)
                    ->orWhere('messagable_id', '!=', auth()->id());
            },
            'participants.messagable',
            'lastMessage',
            'recentMessages'
        ])->find($id);

        if (is_null($conversation)) {
            abort(404);
        }

        return $conversation;
    }

    public function sendMessage(Request $request, $id)
    {
        $conversation = Conversation::find($id);
        if (is_null($conversation)) {
            abort(404);
        }
        try {
            $isPartner = Participant::where('conversation_id', $conversation->id)
                ->where([
                    ['messagable_type', '=', User::class],
                    ['messagable_id', '=', auth()->id()],
                ])->exists();

            if (!$isPartner) {
                abort(403);
            }

            $validator = Validator::make($request->all(), [
                'type' => 'required|in:text,image,audio,video',
                'content' => 'required|string'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => $validator->messages()
                ], 422);
            }

            $message = Message::create([
                'conversation_id' => $conversation->id,
                'data' => ['type' => $request['type'], 'content' => $request['content']],
                'messagable_id' => auth()->id(),
                'messagable_type' => User::class
            ]);

            $conversation->touch();

            foreach ($conversation->participants as $participant) {
                if (
                    $participant['messagable_type'] != User::class
                    || $participant['messagable_id'] != auth()->id()
                ) {
                    $participant = ($participant['messagable_type'])::find($participant['messagable_id']);
                    $participant->notify(new NewMessageNotification($message, auth()->user()));
                }
            }

            return response()->json([
                'data' => $message
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }
    }

    public function createConversation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'messagable_type' => 'bail|required|string|in:' . implode(",", config('const.message.messagables')),
            'messagable_id' => 'bail|required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        if (is_null($request['messagable_type']::find($request['messagable_id']))) {
            return response()->json([
                'error' => true,
                'message' => 'Không tìm thấy người dùng này!'
            ], 404);
        }

        $participants = [
            ['messagable_type' => User::class, 'messagable_id' => auth()->id()],
            ['messagable_type' => $request['messagable_type'], 'messagable_id' => $request['messagable_id']],
        ];

        $conversation = Conversation::where(function ($conversation) use ($participants) {
            foreach ($participants as $participant) {
                $conversation->whereHas('participants', function ($participants) use ($participant) {
                    $participants->where('messagable_type', $participant['messagable_type'])
                        ->where('messagable_id', $participant['messagable_id']);
                });
            }
        })->withCount('participants')
            ->having('participants_count',  2)
            ->first();

        if (is_null($conversation)) {
            $conversation = Conversation::create([]);
            foreach ($participants as $participant) {
                $conversation->participants()->save(new Participant([
                    'messagable_type' => $participant['messagable_type'],
                    'messagable_id' => $participant['messagable_id']
                ]));
            }
        }

        $conversation->load([
            'participants' => function ($messagable) {
                $messagable->where('messagable_type', '!=', Manager::class)
                    ->orWhere('messagable_id', '!=', auth()->id());
            },
            'participants.messagable',
            'lastMessage',
            'recentMessages'
        ]);

        return $conversation;
    }

    public function createConversationWithManager(Request $request, $managerId)
    {
        $manager = Manager::find($managerId);

        if (is_null($manager)) {
            return response()->json([
                'error' => true,
                'message' => 'Không tìm thấy người dùng này!'
            ], 404);
        }

        $participants = [
            ['messagable_type' => User::class, 'messagable_id' => auth()->id()],
            ['messagable_type' => Manager::class, 'messagable_id' => $managerId],
        ];

        $conversation = Conversation::where(function ($conversation) use ($participants) {
            foreach ($participants as $participant) {
                $conversation->whereHas('participants', function ($participants) use ($participant) {
                    $participants->where('messagable_type', $participant['messagable_type'])
                        ->where('messagable_id', $participant['messagable_id']);
                });
            }
        })->withCount('participants')
            ->having('participants_count',  2)
            ->first();

        if (is_null($conversation)) {
            $conversation = Conversation::create([]);
            foreach ($participants as $participant) {
                $conversation->participants()->save(new Participant([
                    'messagable_type' => $participant['messagable_type'],
                    'messagable_id' => $participant['messagable_id']
                ]));
            }
        }

        $conversation->load([
            'participants' => function ($messagable) {
                $messagable->where('messagable_type', '!=', User::class)
                    ->orWhere('messagable_id', '!=', auth()->id());
            },
            'participants.messagable',
            'lastMessage',
            'recentMessages'
        ]);

        return $conversation;
    }
}
