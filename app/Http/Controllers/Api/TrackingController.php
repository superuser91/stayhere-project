<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\LivingCost;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TrackingController extends Controller
{
    public function livingCostList(Request $request)
    {
        $livingCostList = LivingCost::where('user_id', auth()->id())
            ->has('invoice')
            ->with(['room.apartment'])
            ->orderBy('month', 'desc')
            ->paginate();

        return $livingCostList;
    }

    public function showLivingCost(Request $request, $id)
    {
        $livingCost = LivingCost::findOrFail($id);

        return $livingCost;
    }

    public function subscription(Request $request)
    {
        $subscription = Subscription::firstOrCreate([
            'user_id' => auth()->id()
        ]);

        return $subscription->refresh();
    }

    public function updateSubscription(Request $request)
    {
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'location' => 'nullable|array',
            'location.desc' => 'nullable|string|max:2048',
            'location.lat' => 'nullable|numeric',
            'location.lon' => 'nullable|numeric',
            'radius' => 'nullable|numeric'
        ], [], [
            'location.desc' => 'khu vực',
            'radius' => 'bán kính',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => collect($validator->messages())->flatten()->first()
            ], 422);
        }

        $subscription = Subscription::where('user_id', auth()->id())->first();

        $subscription->update([
            'location' => $request['location'],
            'radius' => $request['radius'],
            'min_acreage' => $request['min_acreage'],
            'max_acreage' => $request['max_acreage'],
            'min_budget' => $request['min_budget'],
            'max_budget' => $request['max_budget'],
            'is_active' => $request['is_active'] ?? 0,
        ]);

        return $subscription;
    }
}
