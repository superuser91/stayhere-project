<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        return Invoice::whereHas('room.contract.user', function ($user) {
            $user->where('id', auth()->id());
        })->with('costable')
            ->paginate();
    }

    public function show(Request $request, $id)
    {
        $invoice = Invoice::with(
            'room.contract.user',
            'room.apartment:location,id',
            'costable'
        )->where('id', $id)
            ->first();

        if (is_null($invoice)) {
            return response([], 404);
        }

        return $invoice;
    }
}
