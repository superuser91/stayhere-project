<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Apartment\RecommendationService;
use Illuminate\Http\Request;

class RecommendationController extends Controller
{
    public function recommendation(Request $request, RecommendationService $recommendationService)
    {
        if (!empty($request->lat) && !empty($request->lon)) {
            $geoCodingInformation = $request->only('lat', 'lon');
        } else {
            $coordinate = geoip($request->ip());
            $geoCodingInformation = [
                'lat' => $coordinate->lat,
                'lon' => $coordinate->lon
            ];
        }

        $result = $recommendationService
            ->recommendationBuilder($geoCodingInformation)
            ->paginate()
            ->appends($request->all());

        $response = $result->toArray();

        $data = $result->load('publicRooms', 'manager');

        $response['data'] = $data;

        return $response;
    }
}
