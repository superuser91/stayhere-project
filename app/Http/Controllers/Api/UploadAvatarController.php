<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UploadAvatarController
{
    /**
     * @OA\Post(
     *      path="/avatar",
     *      operationId="Profile",
     *      tags={"Profile"},
     *      summary="Update profile",
     *      description="Update avatar",
     *     	@OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="avatar",
     *                      description="Avatar",
     *                      type="file",
     *                      @OA\Items(type="string", format="binary")
     *                   ),
     *               ),
     *           ),
     *       ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'avatar' => 'required|file|image|max:5000',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => $validator->messages()
                ]);
            }

            $avatar = $request->file('avatar');

            $filePath = $avatar->storeAs('public/' . auth()->id(), 'avatar.' . $avatar->extension());

            $avatarUrl = Storage::url($filePath);

            auth()->user()->update([
                'avatar' => $avatarUrl
            ]);

            return response()->json([
                'data' => [
                    'avatar' => $avatarUrl
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [
                    $e->getMessage()
                ]
            ]);
        }
    }
}
