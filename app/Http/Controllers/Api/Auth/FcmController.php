<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FcmController extends Controller
{
    public function updateToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fcm_token' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        auth()->user()->update([
            'fcm_token' => $request['fcm_token']
        ]);

        return auth()->user();
    }

    public function removeToken(Request $request)
    {
        auth()->user()->update([
            'fcm_token' => null
        ]);

        return auth()->user();
    }
}
