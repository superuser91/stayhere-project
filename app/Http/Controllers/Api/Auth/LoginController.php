<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;

class LoginController
{
    /**
     * @OA\Post(
     *      path="/login",
     *      operationId="Login",
     *      tags={"Authentication"},
     *      summary="",
     *      description="",
     *      @OA\Parameter(
     *          name="email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *     )
     */
    public function login(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required|string',
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => collect($validation->messages())->flatten()->first()
                ], 422);
            }

            $validator = Validator::make(['email' => $request['email']], [
                'email' => 'email'
            ]);

            if ($validator->fails()) {
                $user = User::where('phone_number', $request['email'])->first();
            } else {
                $user = User::where('email', $request['email'])->first();
            }

            if (is_null($user)) {
                return response()->json([
                    'error' => true,
                    'message' => 'Không tìm thấy thông tin tài khoản trong hệ thống'
                ], 422);
            }

            if (!Hash::check($request['password'], $user->password)) {
                return response()->json([
                    'error' => true,
                    'message' => 'Thông tin đăng nhập không chính xác'
                ], 422);
            }

            return response()->json([
                'token' => $user->createToken(config('app.name')),
                'user' => $user
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function loginWithFacebook(Request $request)
    {
        try {
            $fbUser = json_decode(json_encode(Socialite::driver('facebook')
                ->userFromToken($request['accessToken'])), true);

            $validator = Validator::make($fbUser, [
                'email' => 'required|email|exists:users,email',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>  collect($validator->messages())->flatten()->first()
                ], 422);
            }

            $user = User::firstOrCreate([
                "email" => $fbUser['email'],
                "fb_id" => $fbUser['id']
            ], [
                "name" => $fbUser['name'],
                "avatar" => $fbUser['avatar'],
            ]);

            return response()->json([
                'token' => $user->createToken(config('app.name')),
                'user' => $user
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'error' => true,
                'message' => 'Email liên kết với tài khoản đã tồn tại, vui lòng liên kết với Facebook để có thể sử dụng chức năng này'
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Lỗi: ' . $e->getMessage()
            ], 500);
        }
    }

    public function loginWithGoogle(Request $request)
    {
        try {
            $user = User::firstOrCreate([
                "email" => $request['email'],
                "google_id" => $request['id']
            ], [
                "name" => $request['name'],
                "avatar" => $request['photo'],
            ]);

            return response()->json([
                'token' => $user->createToken(config('app.name')),
                'user' => $user
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'error' => true,
                'message' => 'Email liên kết với tài khoản đã tồn tại, vui lòng liên kết với Google để có thể sử dụng chức năng này'
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Lỗi hệ thống'
            ], 500);
        }
    }
}
