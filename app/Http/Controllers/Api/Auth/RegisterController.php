<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController
{
    /**
     * @OA\Post(
     *      path="/register",
     *      operationId="getApartmentList",
     *      tags={"Authentication"},
     *      summary="",
     *      description="",
     *      @OA\Parameter(
     *          name="name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password_confirmation",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="gender",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *     )
     */
    public function register(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users,email',
                'phone_number' => 'required|unique:users,phone_number',
                'password' => 'required|confirmed|min:6',
                'gender' => 'nullable|numeric'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => collect($validation->messages())->flatten()->first()
                ], 422);
            }

            $request['password'] = bcrypt($request['password']);

            $user = User::create($request->all());

            return response()->json([
                'token' => $user->createToken(config('app.name')),
                'user' => $user->refresh()
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
