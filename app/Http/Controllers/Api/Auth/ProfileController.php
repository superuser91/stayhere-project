<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController
{
    /**
     * @OA\Get(
     *      path="/profile",
     *      operationId="Profile",
     *      tags={"Profile"},
     *      summary="Show profile",
     *      description="Show profile",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function profile()
    {
        return auth()->user()->load('profile');
    }

    /**
     * @OA\Post(
     *      path="/profile",
     *      operationId="Profile",
     *      tags={"Profile"},
     *      summary="Update profile",
     *      description="Update profile",
     *      @OA\Parameter(
     *          name="name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="gender",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), []);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => collect($validator->message())->flatten()->first()
            ], 422);
        }

        auth()->user()->update($request->only([
            'name',
            'gender',
            'phone_number'
        ]));

        return response()->json([
            'data' => auth()->user()
        ]);
    }
}
