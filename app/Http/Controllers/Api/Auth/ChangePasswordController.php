<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController
{
    /**
     * @OA\Post(
     *      path="/change-password",
     *      operationId="changePassword",
     *      tags={"Authentication"},
     *      summary="",
     *      description="",
     *      @OA\Parameter(
     *          name="old_password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="password_confirmation",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=204,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=500, description="Server error"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function changePassword(Request $request)
    {
        $password = auth()->user()->password;
        try {
            $validator = Validator::make($request->all(), [
                'old_password' => [
                    function ($attr, $val, $fail) use ($password) {
                        if (!empty($password) && empty($val)) {
                            $fail('Mật khẩu hiện tại không được để trống');
                            return;
                        }
                        if (!empty($password) && !Hash::check($val, $password)) {
                            $fail('Mật khẩu hiện tại không đúng');
                            return;
                        }
                    },
                ], 'password' => 'required|confirmed|min:6',
            ], [], [
                'old_password' => 'mật khẩu hiện tại',
                'password' => 'mật khẩu mới'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => collect($validator->messages())->flatten()->first()
                ], 422);
            }

            auth()->user()->update([
                'password' => bcrypt($request['password'])
            ]);

            return response()->json([]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
