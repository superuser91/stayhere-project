<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class ConnectController
{
    public function connectToFacebook(Request $request)
    {
        try {
            $fbUser = json_decode(json_encode(Socialite::driver('facebook')
                ->userFromToken($request['accessToken'])), true);

            $user = auth()->user();

            $user->update([
                'fb_id' => $fbUser['id'],
                "avatar" => $fbUser['avatar'],
            ]);

            return response()->json([
                'message' => 'Kết nối thành công',
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'error' => true,
                'message' => 'Lỗi: Tài khoản Facebook này đã được liên kết với một tài khoản khác trong hệ thống'
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function connectToGoogle(Request $request)
    {
        try {
            $user = auth()->user();

            if ($user->google_id) {
                return response()->json([
                    'message' => 'Tài khoản đã liên kết với Google',
                ]);
            }

            $user->update([
                "google_id" => $request['id'],
                "avatar" => $request['photo'],
            ]);

            return response()->json([
                'message' => 'Kết nối thành công',
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'error' => true,
                'message' => 'Lỗi: Tài khoản Google này đã được liên kết với một tài khoản khác trong hệ thống'
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
