<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    /**
     * @OA\Get(
     *      path="/articles",
     *      operationId="getProjectsList",
     *      tags={"Article"},
     *      summary="Get list of apartments",
     *      description="Returns list of apartments",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *     )
     *
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Article::paginate();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'nullable',
            'thumbnail' => 'nullable',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return Article::create($request->all());
    }

    /**
     * @OA\Get(
     *      path="/articles/{id}",
     *      operationId="show Article",
     *      tags={"Article"},
     *      summary="show apartments",
     *      description="Returns apartment",
     *      @OA\Parameter(
     *          name="id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *     )
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article =  Article::find($id);

        if (!$article) {
            return response()->json([
                'error' => true,
                'message' => 'Not found'
            ], 400);
        }

        return $article;
    }

    public function update(Request $request, Article $article)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'content' => 'nullable',
            'thumbnail' => 'nullable',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return $article->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        return Article::destroy($id);
    }
}
