<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SaveRecentSearch;
use App\Services\Room\SearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SearchController extends Controller
{
    /**
     * @OA\Get(
     *      path="/search",
     *      operationId="search",
     *      tags={"Search"},
     *      summary="Search post",
     *      description="Returns list of apartments",
     *      @OA\Parameter(
     *          name="address",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="district_id",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="province_id",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="ward_id",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="price_from",
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="price_to",
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="acreage_from",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="acreage_to",
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="sort_by",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *     )
     */
    public function index(Request $request, SearchService $searchService)
    {
        $validation = Validator::make($request->all(), [
            'address' => 'nullable|string|max:255',
            'ward_id' => 'nullable',
            'district_id' => 'required_with:ward_id|nullable|string',
            'province_id' => 'required_with:district_id|nullable|string',
            'price_from' => 'nullable|numeric',
            'price_to' => 'nullable|numeric',
            'acreage_from' => 'nullable|numeric',
            'acreage_to' => 'nullable|numeric',
            'sort_by' => 'nullable|string'
        ]);

        if ($validation->fails()) {
            return response()->json([
                'error' => true,
                'messages' => $validation->messages()
            ], 422);
        }

        if ($request->user('api')) {
            dispatch(new SaveRecentSearch($request->user('api')->id, $request['address']));
        }
    }

    public function getRecentSearch()
    {
        return auth()
            ->user()
            ->recentSearch()
            ->orderBy('updated_at', 'desc')
            ->take(5)
            ->get();
    }
}
