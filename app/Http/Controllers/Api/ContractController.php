<?php

namespace App\Http\Controllers\Api;

use App\Contract;
use App\Http\Controllers\Controller;
use App\Notifications\UserAcceptContract;
use App\Notifications\UserDeclineContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{
    public function showContract(Request $request,  $id)
    {
        $contract = Contract::withTrashed()->findOrFail($id);

        return $contract;
    }

    public function acceptContract(Request $request, Contract $contract)
    {
        DB::beginTransaction();
        try {
            if (auth()->id() != $contract->user_id) {
                abort(403);
            }

            if (!(empty($contract->user_confirm_at))) {
                abort(422);
            }

            $contract->load(['room.apartment.manager', 'order']);

            $contract->update([
                'user_confirm_at' => now()
            ]);

            $contract->room->apartment->manager->notify(new UserAcceptContract($contract));

            DB::commit();

            return $contract;
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }
    }

    public function declineContract(Request $request, $id)
    {
        $contract = Contract::with([
            'room.apartment.manager',
            'order'
        ])->findOrFail($id);

        DB::beginTransaction();
        try {
            if (auth()->id() != $contract->user_id) {
                abort(403);
            }

            if (!(empty($contract->user_confirm_at))) {
                abort(422);
            }

            $contract->room->apartment->manager->notify(new UserDeclineContract($contract));

            $contract->update([
                'user_canceled_at' => now()
            ]);

            $contract->order->delete();

            $contract->delete();

            DB::commit();

            return response([]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }
    }
}
