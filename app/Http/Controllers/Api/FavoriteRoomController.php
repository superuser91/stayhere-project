<?php

namespace App\Http\Controllers\Api;

use App\FavoriteRoom;
use App\Http\Controllers\Controller;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavoriteRoomController extends Controller
{
    /**
     * @OA\Get(
     *      path="/favorites",
     *      operationId="getFavoriteList",
     *      tags={"Favorite"},
     *      summary="getFavoriteList",
     *      description="getFavoriteList",
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     *
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->favoriteRooms;
    }

    /**
     * @OA\POST(
     *      path="/favorites",
     *      operationId="storeFavoriteRoom",
     *      tags={"Favorite"},
     *      summary="store new favorite room",
     *      description="store new favorite room",
     *      @OA\Parameter(
     *          name="room_id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function store(Request $request, Room $room)
    {
        return FavoriteRoom::firstOrCreate([
            'user_id' => auth()->id(),
            'room_id' => $room->id
        ]);
    }

    public function destroy($roomId)
    {
        FavoriteRoom::where('room_id', $roomId)
            ->where('user_id', auth()->id())
            ->delete();

        return response([], 204);
    }
}
