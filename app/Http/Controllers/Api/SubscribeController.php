<?php

namespace App\Http\Controllers\Api;

use App\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubscribeController
{
    /**
     * @OA\POST(
     *      path="/subscribe",
     *      operationId="subscribeRoom",
     *      tags={"Subscribe"},
     *      summary="subscribeRoom",
     *      description="subscribeRoom",
     *      @OA\Parameter(
     *          name="location",
     *          in="query",
     *          @OA\Schema(
     *              type="object"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="radius",
     *          in="query",
     *          @OA\Schema(
     *              type="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="min_acreage",
     *          in="query",
     *          @OA\Schema(
     *              type="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="max_budget",
     *          in="query",
     *          @OA\Schema(
     *              type="float"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_active",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(response=200,description="successful operation", @OA\JsonContent()),
     *      @OA\Response(response=400, description="Bad request"),
     *      security={
     *          {"bearerAuth": {}}
     *      }
     *     )
     */
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location' => 'nullable',
            'radius' => 'nullable',
            'min_acreage' => 'nullable',
            'max_budget' => 'nullable',
            'is_active' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()
            ], 400);
        }

        try {
            $subscribe = Subscribe::updateOrCreate([
                'user_id' => auth()->id()
            ], $request->except(['user_id']));
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }

        return response()->json([
            'data' => $subscribe
        ]);
    }
}
