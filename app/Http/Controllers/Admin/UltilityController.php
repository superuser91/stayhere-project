<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ultility;
use Illuminate\Http\Request;
use PHPUnit\Util\Xml\Validator;

class UltilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ultility::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'icon' => 'nullable|max:255',
            'app_icon_path' => 'nullable|max:255',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return Ultility::firstOrCreate($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Ultility::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'icon' => 'nullable|max:255',
            'app_icon_path' => 'nullable|max:255',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return Ultility::update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Ultility::destroy($id);
    }
}
