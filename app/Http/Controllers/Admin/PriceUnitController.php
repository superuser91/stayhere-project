<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PriceUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PriceUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PriceUnit::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'acronym' => 'required|max:255',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return PriceUnit::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PriceUnit::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'acronym' => 'required|max:255',
        ]);

        if ($validation->fails()) {
            return response()->json(
                [
                    'error' => true,
                    'messages' => $validation->messages()
                ],
                400
            );
        }

        return PriceUnit::update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return PriceUnit::destroy($id);
    }
}
