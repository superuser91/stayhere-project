<?php

namespace App\Http\Controllers\Manage;

use App\Apartment;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Tenant;
use App\Utils\JsonDB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Util\Json;

class StatisticController extends Controller
{
    public function incomeSummary(Request $request)
    {
        $request['year'] = $request['year'] ?? date('Y');

        $data = Invoice::whereHas('room', function ($room) {
            $room->whereHas('apartment', function ($apartment) {
                $apartment->where('manager_id', auth('manager')->id());
            });
        })
            ->where(DB::raw('YEAR(paid_at)'), $request['year'])
            ->whereNotNull('paid_at')
            ->select(
                'costable_type',
                DB::raw('MONTH(paid_at) as month'),
                DB::raw('sum(amount) as amount'),
            )
            ->groupBy('month', 'costable_type')
            ->orderBy('month')
            ->get();

        $houseCost = $livingCost = [];
        for ($i = 0; $i < 12; $i++) {
            $houseCost[$i] = $data->where('costable_type', '=', config('const.invoice.types.house_cost.code'))
                ->firstWhere('month', '=', ($i + 1))
                ->amount ?? 0;
            $livingCost[$i] = $data->where('costable_type', '=', config('const.invoice.types.living_cost.code'))
                ->firstWhere('month', '=', ($i + 1))
                ->amount ?? 0;
        }

        $years = Invoice::whereNotNull('paid_at')
            ->select(DB::raw('YEAR(paid_at) as year'))
            ->distinct()
            ->pluck('year');

        return view('manager.statistics.income')->with([
            'houseCost' => $houseCost,
            'livingCost' => $livingCost,
            'years' => $years
        ]);
    }

    public function tenantSummary(Request $request)
    {
        $locations = json_decode(JsonDB::locations(), true);

        $apartments = Apartment::where('manager_id', auth()->id())->get();

        $tenants = Tenant::whereHas('room', function ($room) use ($request) {
            $room->wherehas('apartment', function ($apartment) use ($request) {
                $apartment->when(!empty($request['apartment_id']), function ($query) use ($request) {
                    $query->where('apartment_id', $request['apartment_id']);
                });
                $apartment->whereHas('manager', function ($manager) {
                    $manager->where('id', auth()->id());
                });
            });
        })->with('room.apartment')
            ->orderBy('room_id')
            ->orderBy('full_name')
            ->paginate();

        // dd($locations);

        return view('manager.statistics.tenants')->with([
            'locations' => collect($locations['data']),
            'tenants' => $tenants,
            'apartments' => $apartments
        ]);
    }

    public function invoiceSummary(Request $request)
    {
        $invoices = Invoice::whereHas('room', function ($room) {
            $room->whereHas('apartment', function ($apartment) {
                $apartment->where('manager_id', auth('manager')->id());
            });
        })->where(function ($invoice) use ($request) {
            $invoice->when(!empty($request['costable_type']), function ($query) use ($request) {
                $query->where('costable_type', $request['costable_type']);
            });
            $invoice->when(!empty($request['payment_status']), function ($query) use ($request) {
                if ($request['payment_status'] == config('const.invoice.statuses.paid.code')) {
                    return $query->whereNotNull('paid_at');
                }
                return $query->whereNull('paid_at');
            });
        })
            ->orderBy('created_at')
            ->with('room.apartment')
            ->paginate();
        // dd($invoices);
        return view('manager.statistics.invoices')->with([
            'invoices' => $invoices
        ]);
    }

    public function showInvoice(Request $request, $id)
    {
        $invoice = Invoice::where('id', $id)->with('room.apartment')->first();

        return view('manager.statistics.invoice')->with([
            'invoice' => $invoice
        ]);
    }

    public function confirmPaid(Request $request, Invoice $invoice)
    {
        $invoice->update([
            'paid_at' => now()
        ]);

        return back();
    }

    public function deleteInvoice(Request $request, Invoice $invoice)
    {
        if (!empty($invoice->paid_at)) {
            $request->session()->flash('message', 'Không thể xoá hoá đơn đã thanh toán');
            $request->session()->flash('message_type', 'error');
            return redirect(route('manager.statistic.invoice'));
        }

        $invoice->delete();

        $request->session()->flash('message', 'Đã xoá hoá đơn');
        $request->session()->flash('message_type', 'success');
        return redirect(route('manager.statistic.invoice'));
    }
}
