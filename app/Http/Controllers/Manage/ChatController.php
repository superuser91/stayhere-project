<?php

namespace App\Http\Controllers\Manage;

use App\Conversation;
use App\Manager;
use App\Messagable;
use App\Message;
use App\Notifications\NewMessageNotification;
use App\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ChatController
{
    public function index()
    {
        $conversations = Conversation::has('messages')
            ->whereHas('participants', function ($participant) {
                $participant->where('messagable_type', Manager::class)
                    ->where('messagable_id', auth()->id());
            })
            ->orderBy('updated_at', 'desc')
            ->with([
                'participants' => function ($messagable) {
                    $messagable->where('messagable_type', '!=', Manager::class)
                        ->orWhere('messagable_id', '!=', auth()->id());
                },
                'participants.messagable',
                'lastMessage',
            ])
            ->get();

        $conversations = $conversations->map(function ($conversation) {
            return $conversation->load('recentMessages');
        });

        return view('manager.messenger')->with([
            'conversations' => $conversations,
        ]);
    }

    public function show(Request $request, Conversation $conversation)
    {
        $conversation->load(['participants' => function ($participant) {
            $participant->where('messagable_type', '!=', Manager::class)
                ->orWhere('messagable_id', '!=', auth()->id());
        }, 'participants.messagable']);

        return view('manager.inbox')->with([
            'conversation' => $conversation,
        ]);
    }

    public function sendMessage(Request $request, Conversation $conversation)
    {
        try {
            $isPartner = Participant::where('conversation_id', $conversation->id)
                ->where([
                    ['messagable_type', '=', Manager::class],
                    ['messagable_id', '=', auth()->id()],
                ])->exists();

            if (!$isPartner) {
                abort(403);
            }

            $validator = Validator::make($request->all(), [
                'type' => 'required|in:text,image,audio,video',
                'content' => 'required|string'
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => true,
                    'message' => $validator->messages()
                ], 400);
            }

            $message = Message::create([
                'conversation_id' => $conversation->id,
                'data' => ['type' => $request['type'], 'content' => $request['content']],
                'messagable_id' => auth()->id(),
                'messagable_type' => Manager::class
            ]);

            $conversation->touch();

            foreach ($conversation->participants as $participant) {
                if (
                    $participant['messagable_type'] != Manager::class
                    || $participant['messagable_id'] != auth()->id()
                ) {
                    $participant = ($participant['messagable_type'])::find($participant['messagable_id']);
                    $participant->notify(new NewMessageNotification($message, auth()->user()));
                }
            }

            return response()->json([
                'data' => $message
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }
    }

    public function listMessage(Request $request, Conversation $conversation)
    {
        $messages = $conversation->recentMessages;

        $participants = [];
        foreach ($conversation->participants as $participant) {
            if (
                $participant['messagable_type'] != Manager::class
                || $participant['messagable_id'] != auth('manager')->id()
            ) {
                $participants[] = [
                    'messagable_type' => $participant['messagable_type'],
                    'messagable' => ($participant['messagable_type'])::find($participant['messagable_id'])
                ];
            }
        }

        return response()->json([
            'messages' => $messages,
            'participants' => $participants
        ]);
    }

    public function searchMessagable(Request $request)
    {
        return Messagable::search($request['search'])
            ->paginateRaw(5);
    }

    public function createConversation(Request $request)
    {
        $request['messagable_type'] = explode("::", $request['messagable'])[0];
        $request['messagable_id'] = explode("::", $request['messagable'])[1];

        $validator = Validator::make($request->all(), [
            'messagable_type' => 'bail|required|string|in:' . implode(",", config('const.message.messagables')),
            'messagable_id' => 'bail|required|numeric',
            'messagable' => function ($val, $attr, $fail) use ($request) {
                if ($request['messagable_type'] == Manager::class && $request['messagable_id'] == auth()->id()) {
                    $fail('Không thể tạo cuộc hội thoại!');
                }
            }
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        if (is_null($request['messagable_type']::find($request['messagable_id']))) {
            return response()->json([
                'error' => true,
                'message' => [
                    'send_to' => ['Không tìm thấy người dùng này!']
                ]
            ], 404);
        }

        $participants = [
            ['messagable_type' => Manager::class, 'messagable_id' => auth()->id()],
            ['messagable_type' => $request['messagable_type'], 'messagable_id' => $request['messagable_id']],
        ];

        $conversation = Conversation::whereHas('participants', function ($participant) use ($request) {
            $participant->where('messagable_type', $request['messagable_type'])
                ->where('messagable_id', $request['messagable_id']);
        })->whereHas('participants', function ($participant) use ($request) {
            $participant->where('messagable_type', Manager::class)
                ->where('messagable_id', auth()->id());
        })->withCount('participants')
            ->having('participants_count',  2)
            ->first();

        if (is_null($conversation)) {
            $conversation = Conversation::create([]);
            foreach ($participants as $participant) {
                $conversation->participants()->save(new Participant([
                    'messagable_type' => $participant['messagable_type'],
                    'messagable_id' => $participant['messagable_id']
                ]));
            }
        }

        $conversation->load([
            'participants' => function ($messagable) {
                $messagable->where('messagable_type', '!=', Manager::class)
                    ->orWhere('messagable_id', '!=', auth()->id());
            },
            'participants.messagable',
            'lastMessage',
            'recentMessages'
        ]);

        return response([
            'conversation' => $conversation,
        ]);
    }
}
