<?php

namespace App\Http\Controllers\Manage\Action;

class CollapseSidebar
{
    public function __invoke()
    {
        session(['sidebar_collapse' => !session('sidebar_collapse')]);
    }
}
