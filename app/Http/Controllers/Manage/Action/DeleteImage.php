<?php

namespace App\Http\Controllers\Manage\Action;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DeleteImage
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'path' => 'required|string'
        ]);

        if ($this->canDelete($request['path'])) {
            Storage::delete([$request['path']]);
            return $request['path'];
        }

        return false;
    }

    private function canDelete($path)
    {
        return true;
    }
}
