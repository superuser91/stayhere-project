<?php

namespace App\Http\Controllers\Manage;

use App\Contract;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\LivingCost;
use App\Notifications\NewHouseInvoiceNotification;
use App\Notifications\NewLivingInvoiceNotification;
use App\Room;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function createLivingInvoice(Request $request, Room $room)
    {
        $room->load(['recentLivingInvoices']);

        return view('manager.invoices.living_invoice')->with([
            'room' => $room
        ]);
    }

    public function createHouseInvoice(Request $request, Room $room)
    {
        $room->load(['contract.user', 'recentHouseInvoices']);

        return view('manager.invoices.house_invoice')->with([
            'room' => $room
        ]);
    }

    public function storeLivingInvoice(Request $request, $id)
    {
        $room = Room::with('contract.user:id')->where('id', $id)->first();

        $date = explode('-', $request['month']);

        $month = $date[1] . '-' . $date[0] . '-01';

        $isExistInvoiceForMonth = Invoice::whereJsonContains('months', ['month' => $month])
            ->where('costable_type', config('const.invoice.types.living_cost.code'))
            ->where('room_id', $room->id)
            ->exists();

        if ($isExistInvoiceForMonth) {
            $request->session()->flash('message', 'Đã có hoá đơn tiền sinh hoạt phí cho tháng ' . $request['month']);
            $request->session()->flash('message_type', 'error');
            return back()->withInput();
        }

        $detail = [
            [
                'name' => 'Tiền điện',
                'consumption' => $request['electricity_consumption_diff'],
                'total_money' => $request['electricity_consumption_money'],
                'unit' => 'KWh'
            ],
            [
                'name' => 'Tiền nước',
                'consumption' => $request['water_consumption_diff'],
                'total_money' => $request['water_consumption_money'],
                'unit' => 'mét khối'
            ],
        ];

        foreach ($request['other'] ?? [] as $other) {
            $detail[] = [
                'name' => $other['name'],
                'total_money' => $other['money'],
                'consumption' => $other['comsumption'] ?? null,
                'unit' => $other['unit'] ?? ''
            ];
        }

        $livingCost = LivingCost::create([
            'room_id' => $room->id,
            'user_id' => $room->contract->user->id,
            'month' => $month,
            'detail' => $detail,
        ]);

        $invoice = Invoice::create([
            'room_id' => $room->id,
            'costable_type' => LivingCost::class,
            'costable_id' => $livingCost->id,
            'amount' => $amount = array_sum(array_column($detail, 'total_money')),
            'months' => [['month' => $month, 'amount' => $amount]],
        ]);

        $room->contract->user->notify(new NewLivingInvoiceNotification($invoice));

        return redirect(route('manager.statistic.invoice'));
    }

    public function storeHouseInvoice(Request $request, Room $room)
    {
        $request->validate([
            'months' => 'required|array',
            'months.*' => 'required|string',
            'amounts' => 'required|array',
            'amounts.*' => 'required|numeric'
        ]);

        $room->load('contract.user');

        $months = [];
        foreach ($request['months'] as $index => $month) {
            $date = explode('-', $month);
            $startOfMonth = $date[1] . '-' . $date[0] . '-01';
            $isExistInvoiceForMonth = Invoice::whereJsonContains('months', ['month' => $startOfMonth])
                ->where('costable_type', config('const.invoice.types.house_cost.code'))
                ->where('room_id', $room->id)
                ->exists();
            if ($isExistInvoiceForMonth) {
                $request->session()->flash('message', 'Đã có hoá đơn tiền phòng cho tháng ' . $month);
                $request->session()->flash('message_type', 'error');
                return back()->withInput();
            }
            $months[] = [
                'month' => $startOfMonth,
                'amount' => floatval($request['amounts'][$index])
            ];
        }

        $invoice = Invoice::create([
            'room_id' => $room->id,
            'costable_type' => Contract::class,
            'costable_id' => $room->contract->id,
            'amount' => array_sum($request['amounts']),
            'months' => $months,
        ]);

        $room->contract->user->notify(new NewHouseInvoiceNotification($invoice));

        return redirect(route('manager.statistic.invoice'));
    }
}
