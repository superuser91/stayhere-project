<?php

namespace App\Http\Controllers\Manage;

use App\Apartment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ManagementController extends Controller
{
    public function livingCost(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth()->id())
            ->with(['leasedRooms.contract.user'])
            ->get();

        return view('manager.management.living_cost')->with([
            'apartments' => $apartments
        ]);
    }

    public function houseCost(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth()->id())
            ->with(['leasedRooms.contract.user'])
            ->get();

        return view('manager.management.house_cost')->with([
            'apartments' => $apartments
        ]);
    }
}
