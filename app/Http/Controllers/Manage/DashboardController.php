<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Invoice;
use App\Tenant;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $manager = auth('manager')->user();
        $apartmentCount = $manager->loadCount('apartments')->apartments_count;
        $roomCount = $manager->apartments->loadCount('rooms')->sum('rooms_count');
        $emptyRoomCount = $manager->apartments->loadCount('emptyRooms')->sum('empty_rooms_count');
        $tenantCount = Tenant::whereHas('room', function ($room) {
            $room->wherehas('apartment', function ($apartment) {
                $apartment->whereHas('manager', function ($manager) {
                    $manager->where('id', auth()->id());
                });
            });
        })
            ->distinct()
            ->count('user_id');

        $notPaidInvoices = Invoice::whereHas('room', function ($room) {
            $room->whereHas('apartment', function ($apartment) {
                $apartment->where('manager_id', auth()->id());
            });
        })->where(function ($invoice) {
            return $invoice->whereNull('paid_at');
        })
            ->orderBy('created_at', 'desc')
            ->with('room')
            ->get()->groupBy('costable_type');

        return view('manager.dashboard')->with([
            'apartmentCount' => $apartmentCount,
            'roomCount' => $roomCount,
            'tenantCount' => $tenantCount,
            'emptyRoomCount' => $emptyRoomCount,
            'notPaidInvoices' => $notPaidInvoices
        ]);
    }
}
