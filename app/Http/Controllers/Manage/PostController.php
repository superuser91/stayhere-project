<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Jobs\NotifySubscription;
use App\Room;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function enablePost(Request $request, Room $room)
    {
        $apartment = $room->apartment;

        $room->update([
            'is_public' => config('const.room.public_room')
        ]);

        dispatch(new NotifySubscription($room));

        $apartment->searchable();

        return response()->json([
            'error' => false
        ]);
    }

    public function disablePost(Request $request, Room $room)
    {
        $apartment = $room->apartment;

        $room->update([
            'is_public' => config('const.room.private_room')
        ]);

        $apartment->searchable();

        return response()->json([
            'error' => false
        ]);
    }
}
