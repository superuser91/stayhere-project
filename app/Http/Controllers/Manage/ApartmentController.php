<?php

namespace App\Http\Controllers\Manage;

use App\Apartment;
use App\Http\Controllers\Controller;
use App\Utils\JsonDB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ApartmentController extends Controller
{
    public function index()
    {
        $uid = auth('manager')->id();

        $apartments =  Apartment::where('manager_id', $uid)->paginate();

        return view('manager.apartments.index')->with([
            'apartments' => $apartments
        ]);
    }

    public function create(Request $request)
    {
        $locations = json_decode(JsonDB::locations(), true);
        return view('manager.apartments.create')->with([
            'locations' => $locations['data']
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'location' => 'required|array',
            'location.lat' => 'required|numeric',
            'location.lon' => 'required|numeric',
            'location.desc' => 'required|string',
            'images' => 'nullable|array',
            'images.*' => 'nullable|string',
            'ultilities' => 'nullable|array',
            'description' => 'nullable|array'
        ]);

        $request['manager_id'] = auth('manager')->id();

        Apartment::create($request->all());

        $request->session()->flash('message', 'Thêm nhà trọ mới thành công');
        $request->session()->flash('message_type', 'success');

        return redirect(route('manager.apartments.index'));
    }

    public function show(Apartment $apartment)
    {
        $locations = json_decode(JsonDB::locations(), true);

        return view('manager.apartments.show')->with([
            'apartment' => $apartment,
            'locations' => $locations['data']
        ]);
    }

    public function edit(Request $request, Apartment $apartment)
    {
        $locations = json_decode(JsonDB::locations(), true);

        return view('manager.apartments.edit')->with([
            'apartment' => $apartment,
            'locations' => $locations['data']
        ]);
    }

    public function update(Request $request, Apartment $apartment)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'location' => 'required|array',
            'location.lat' => 'required|numeric',
            'location.lon' => 'required|numeric',
            'location.desc' => 'required|string',
            'images' => 'nullable|array',
            'images.*' => 'nullable|string',
            'ultilities' => 'nullable|array',
        ]);

        $apartment->update($request->except(['manager_id']));

        $apartment->rooms()->searchable();

        $request->session()->flash('message', 'Cập nhật thành công');
        $request->session()->flash('message_type', 'success');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\Response
     */
    public function destroy(Apartment $apartment)
    {
        $apartment->delete();

        return redirect(route('manager.apartments.index'));
    }

    public function uploadImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'images' => 'required|array',
            'images.*' => 'image|max:4096'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()
            ], 422);
        }

        $paths = [];
        foreach ($request->file('images') as $file) {
            $filePath = $file->storeAs('public/apartments/images', $file->getClientOriginalName() . time() . '.' . $file->extension());
            $paths[] = Storage::url($filePath);
        }

        return response()->json([
            'data' => $paths
        ]);
    }
}
