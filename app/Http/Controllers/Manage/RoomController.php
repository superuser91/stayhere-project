<?php

namespace App\Http\Controllers\Manage;

use App\Apartment;
use App\Http\Controllers\Controller;
use App\Room;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller
{
    public function index(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth('manager')->id())
            ->with('rooms')
            ->get();

        return view('manager.rooms.index')->with([
            'apartments' => $apartments
        ]);
    }

    public function leasedRoom(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth('manager')->id())
            ->has('leasedRooms')
            ->with('leasedRooms')
            ->get();

        return view('manager.rooms.list.leased')->with([
            'apartments' => $apartments
        ]);
    }

    public function emptyRoom(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth('manager')->id())
            ->has('emptyRooms')
            ->with('emptyRooms')
            ->get();

        return view('manager.rooms.list.empty')->with([
            'apartments' => $apartments
        ]);
    }

    public function listPosts(Request $request)
    {
        $apartments = Apartment::where('manager_id', auth('manager')->id())
            ->has('publicRooms')
            ->with('publicRooms')
            ->get();

            return view('manager.rooms.list.ads')->with([
            'apartments' => $apartments
        ]);
    }

    public function create(Request $request, Apartment $apartment)
    {
        return view('manager.rooms.create')->with([
            'apartment' => $apartment
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Apartment $apartment)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'room_number' => 'nullable|string|max:255',
            'description' => 'required|array',
            'description.type' => 'required|numeric|in:' . implode(",", array_keys(config('const.room.types'))),
            'description.ultilities' => 'nullable|array',
            'description.ultilities.*' => 'nullable|numeric|in:' . implode(",", array_keys(config('const.room.ultilities'))),
            'images' =>  'nullable|array',
            'images.*' =>  'nullable|string|max:2048',
            'price' => 'required|numeric',
            'acreage' => 'required|numeric',
        ]);

        $data = $request->all();

        $data['apartment_id'] = $apartment->id;

        $data['description']['tags'] = [];
        foreach ($data['description']['ultilities'] as $tag) {
            $data['description']['tags'][] = config('const.room.ultilities')[$tag];
        }
        $data['description']['tags'][] = config('const.room.types')[$data['description']['type']];

        $room =  Room::create($data);

        return redirect(route('manager.rooms.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        return view('manager.rooms.show')->with([
            'room' => $room
        ]);
    }

    public function edit(Request $request, Room $room)
    {
        return view('manager.rooms.edit')->with([
            'room' => $room
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'room_number' => 'nullable|string|max:255',
            'description' => 'required|array',
            'description.type' => 'required|numeric|in:' . implode(",", array_keys(config('const.room.types'))),
            'description.ultilities' => 'nullable|array',
            'description.ultilities.*' => 'nullable|numeric|in:' . implode(",", array_keys(config('const.room.ultilities'))),
            'images' =>  'nullable|array',
            'images.*' =>  'nullable|string|max:2048',
            'price' => 'required|numeric',
            'acreage' => 'required|numeric',
        ]);

        try {
            $data = $request->all();
            $data['description']['tags'] = [];
            if (isset($data['description']['ultilities'])) {
                foreach ($data['description']['ultilities'] as $tag) {
                    $data['description']['tags'][] = config('const.room.ultilities')[$tag];
                }
            } else {
                $data['description']['ultilities'] = [];
            }
            $data['description']['tags'][] = config('const.room.types')[$data['description']['type']];
            $room->update($data);

            $request->session()->flash('message', 'Cập nhật công');
            $request->session()->flash('message_type', 'success');
        } catch (\Exception $e) {
            $request->session()->flash('message', 'Cập nhật thất bại');
            $request->session()->flash('message_type', 'error');
        }

        return redirect(route('manager.rooms.edit', $room->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            Room::destroy($id);
            $request->session()->flash('message', 'Xoá thành công');
            $request->session()->flash('message_type', 'success');
        } catch (\Exception $e) {
            $request->session()->flash('message', 'Xoá thất bại');
            $request->session()->flash('message_type', 'error');
        }
        return redirect(route('manager.rooms.index'));
    }
}
