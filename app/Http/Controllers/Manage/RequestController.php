<?php

namespace App\Http\Controllers\Manage;

use App\Contract;
use App\Http\Controllers\Controller;
use App\Notifications\ContractDepositHasBeenConfirm;
use App\Notifications\ContractHasBeenCancel;
use App\Notifications\OrderHasBeenAccepted;
use App\Notifications\OrderHasBeenDeclined;
use App\Order;
use App\Tenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class RequestController extends Controller
{
    public function index(Request $request)
    {
        $requests = Order::whereHas('room', function ($room) {
            $room->whereHas('apartment', function ($apartment) {
                $apartment->where('manager_id', auth()->id());
            });
        })->with(['room', 'user', 'contract'])
            ->get();

        return view('manager.requests.index')->with([
            'requests' => $requests
        ]);
    }

    public function createContract(Request $request,  $id)
    {
        $orderRequest = Order::with(['room.apartment.manager.profile', 'user.profile', 'contract'])->where('id', $id)->first();

        return view('manager.contracts.create')->with([
            'request' => $orderRequest
        ]);
    }

    public function declineRequest(Request $request, $id)
    {
        $orderRequest = Order::with(['room', 'user', 'contract'])->where('id', $id)->first();

        $orderRequest->delete();

        $orderRequest->user->notify(new OrderHasBeenDeclined($orderRequest));

        return back();
    }

    public function acceptRequest(Request $request, $id)
    {
        $orderRequest = Order::with(['room.apartment', 'user', 'contract'])->where('id', $id)->first();

        if (empty($orderRequest->contract)) {
            Contract::create([
                'order_id' => $id,
                'manager_id' => auth()->id(),
                'user_id' => $orderRequest->user->id,
                'room_id' => $orderRequest->room->id,
                'room_info' => $orderRequest->room,
                'stackholder_info' => [
                    'host' => auth()->user()->load('profile'),
                    'guest' => $orderRequest->user->load('profile')
                ],
                'valid_from' => Carbon::parse($request['valid_from']),
                'valid_to' => Carbon::parse($request['valid_from'])->addMonths($request['contract_term']),
                'contract_term' => $request['contract_term'],
            ]);
        } else {
            $orderRequest->contract;
        }

        $orderRequest->update([
            'manager_approve_at' => now()
        ]);

        $orderRequest->user->notify(new OrderHasBeenAccepted($orderRequest));

        return redirect(route('manager.requests.index'));
    }

    public function confirmDeposit(Request $request, Contract $contract)
    {
        if (auth()->id() != $contract->manager_id) {
            abort(403);
        }

        if (empty($contract->user_confirm_at)) {
            abort(422);
        }

        $contract->load(['room.apartment.manager', 'order', 'user']);

        $contract->room->update([
            'contract_id' => $contract->id,
            'is_public' => config('const.room.private_room')
        ]);

        $contract->room->apartment->searchable();

        Tenant::updateOrCreate([
            'room_id' => $contract->room->id,
            'user_id' => $contract->user_id,
        ], [
            'full_name' => $contract->user->name,
            'birth_date' => $contract->user->profile->birth_date ?? null,
            'gender' =>  $contract->user->profile->gender ?? null,
            'province_id' =>  $contract->user->profile->province_id ?? null,
            'district_id' =>  $contract->user->profile->district_id ?? null,
            'ward_id' =>  $contract->user->profile->ward_id ?? null,
        ]);

        Order::where('room_id', $contract->room->id)
            ->delete();

        $contract->update([
            'deposit_confirmed_at' => now()
        ]);

        $contract->user->notify(new ContractDepositHasBeenConfirm($contract));

        return back();
    }

    public function cancelContract(Request $request, Contract $contract)
    {
        DB::beginTransaction();
        try {
            $contract->update([
                'manager_canceled_at' => now(),
            ]);

            $contract->user->notify(new ContractHasBeenCancel($contract));

            $contract->order->delete();

            $contract->delete();

            DB::commit();
            return back();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()]
            ], 500);
        }
    }
}
