<?php

namespace App\Http\Controllers;


class DashboardController extends Controller
{
    public function index()
    {
        $androidDownloadLink = '/storage/release/stayhere_app_v1.0.0.apk';
        return view('app.mobile')->with([
            'androidDownloadLink' => $androidDownloadLink
        ]);
    }
}
