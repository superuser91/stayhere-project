<?php

namespace App\Services\Room;

use App\Room;

class SearchService
{
    public function searchBuilder(array $filters)
    {
        $rooms =  Room::search($filters['address'] ?? '*');

        if (!empty($filters['province_id'])) {
            $rooms = $rooms->with([
                'filters' => 'apartment.province_id:' . $filters['province_id'],
            ]);
        }

        if (!empty($filters['district_id'])) {
            $rooms = $rooms->with([
                'filters' => 'apartment.district_id:' . $filters['district_id'],
            ]);
        }

        if (!empty($filters['ward_id'])) {
            $rooms = $rooms->with([
                'filters' => 'apartment.ward_id:' . $filters['ward_id'],
            ]);
        }

        if (!empty($filters['price_from'])) {
            $rooms = $rooms->where('price', '>=', $filters['price_from']);
        }

        if (!empty($filters['price_to'])) {
            $rooms = $rooms->where('price', '<=', $filters['price_to']);
        }

        if (!empty($filters['acreage_from'])) {
            $rooms = $rooms->where('acreage', '>=', $filters['acreage_from']);
        }

        if (!empty($filters['acreage_to'])) {
            $rooms = $rooms->where('acreage', '<=', $filters['acreage_to']);
        }

        return $rooms;
    }

    public function searchBuilderBak(array $filters)
    {
        $apartments =  Apartment::search($filters['address'] ?? '*')
            ->where('public_rooms_count', '>', 0);

        if (!empty($filters['province_id'])) {
            $apartments = $apartments->with([
                'filters' => 'province_id:' . $filters['province_id'],
            ]);
        }

        if (!empty($filters['district_id'])) {
            $apartments = $apartments->with([
                'filters' => 'district_id:' . $filters['district_id'],
            ]);
        }

        if (!empty($filters['ward_id'])) {
            $apartments = $apartments->with([
                'filters' => 'ward_id:' . $filters['ward_id'],
            ]);
        }

        if (!empty($filters['price_from'])) {
            $apartments = $apartments->where('public_rooms.price', '>=', $filters['price_from']);
        }

        if (!empty($filters['price_to'])) {
            $apartments = $apartments->where('public_rooms.price', '<=', $filters['price_to']);
        }

        if (!empty($filters['acreage_from'])) {
            $apartments = $apartments->where('public_rooms.acreage', '>=', $filters['acreage_from']);
        }

        if (!empty($filters['acreage_to'])) {
            $apartments = $apartments->where('public_rooms.acreage', '<=', $filters['acreage_to']);
        }

        return $apartments;
    }
}
