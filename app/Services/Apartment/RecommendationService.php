<?php

namespace App\Services\Apartment;

use App\Apartment;

class RecommendationService
{
    public function recommendationBuilder($geo)
    {
        $recommendations = Apartment::search('')
            ->aroundLatLng(floatval($geo['lat']), floatval($geo['lon']))
            ->with([
                'aroundRadius' => 5000
            ])->where('public_rooms_count', '>', 0);

        return $recommendations;
    }
}
