<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LivingCost extends Model
{
    protected $fillable = [
        'room_id',
        'user_id',
        'month',
        'detail',
        'report',
        'report_submit_at',
        'report_solved_at',
    ];

    protected $casts = [
        'detail' => 'array'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function invoice()
    {
        return $this->morphOne(Invoice::class, 'costable');
    }
}
