<?php

namespace App\Policies;

use App\Room;
use App\Manager;
use Illuminate\Auth\Access\HandlesAuthorization;

class RoomPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any rooms.
     *
     * @param  \App\Manager  $manager
     * @return mixed
     */
    public function viewAny(Manager $manager)
    {
        return true;
    }

    /**
     * Determine whether the user can view the room.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Room  $room
     * @return mixed
     */
    public function view(Manager $manager, Room $room)
    {
        return $room->apartment->manager_id == $manager->id;
    }

    /**
     * Determine whether the user can create rooms.
     *
     * @param  \App\Manager  $manager
     * @return mixed
     */
    public function create(Manager $manager)
    {
        return true;
    }

    /**
     * Determine whether the user can update the room.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Room  $room
     * @return mixed
     */
    public function update(Manager $manager, Room $room)
    {
        return $room->apartment->manager_id == $manager->id;
    }

    /**
     * Determine whether the user can delete the room.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Room  $room
     * @return mixed
     */
    public function delete(Manager $manager, Room $room)
    {
        return $room->apartment->manager_id == $manager->id;
    }
}
