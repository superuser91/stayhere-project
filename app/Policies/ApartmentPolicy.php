<?php

namespace App\Policies;

use App\Apartment;
use App\Manager;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApartmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any apartments.
     *
     * @param  \App\Manager  $manager
     * @return mixed
     */
    public function viewAny(Manager $manager)
    {
        return true;
    }

    /**
     * Determine whether the user can view the apartment.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Apartment  $apartment
     * @return mixed
     */
    public function view(Manager $manager, Apartment $apartment)
    {
        return $apartment->manager_id == $manager->id;
    }

    /**
     * Determine whether the user can create apartments.
     *
     * @param  \App\Manager  $manager
     * @return mixed
     */
    public function create(Manager $manager)
    {
        return true;
    }

    /**
     * Determine whether the user can update the apartment.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Apartment  $apartment
     * @return mixed
     */
    public function update(Manager $manager, Apartment $apartment)
    {
        return $apartment->manager_id == $manager->id;
    }

    /**
     * Determine whether the user can delete the apartment.
     *
     * @param  \App\Manager  $manager
     * @param  \App\Apartment  $apartment
     * @return mixed
     */
    public function delete(Manager $manager, Apartment $apartment)
    {
        return $apartment->manager_id == $manager->id;
    }
}
