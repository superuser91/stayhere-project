<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityHistory extends Model
{
    protected $fillable = [
        'user_id',
        'action_type',
        'resourcable',
    ];
}
