<?php

namespace App\Utils;

class JsonDB
{
    public static function locations()
    {
        $path = database_path("json/locations.json");
        try {
            return \Cache::rememberForever('locations', function () use ($path) {
                return file_get_contents($path);
            });
        } catch (\Exception $e) {
        }
    }

    public static function decodedLocations()
    {
        return json_decode(static::locations(), true)['data'];
    }
}
