<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'email',
        'password',
        'name'
    ];

    protected $hidden = [
        'password'
    ];

    public function toSearchableArray()
    {
        $array = $this->only(['name', 'email']);

        $array['messagable_type'] = get_class($this);

        return $array;
    }

    public function messages()
    {
        return $this->morphToMany(Message::class, 'messagable');
    }

    public function participants()
    {
        return $this->morphMany(Participant::class, 'messagable');
    }
}
