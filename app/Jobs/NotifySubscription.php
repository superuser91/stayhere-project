<?php

namespace App\Jobs;

use App\Notifications\NewPublishRoom;
use App\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifySubscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $room;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($room)
    {
        $this->room = $room;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $room = $this->room;

        $lat = $room->apartment->location['lat'];
        $lon = $room->apartment->location['lon'];

        $subscriptions = Subscription::where('is_active', 1)
            ->where(function ($filter) use ($room) {
                $filter->whereNull('min_acreage')
                    ->orWhere('min_acreage', '<=', $room->acreage);
            })
            ->where(function ($filter) use ($room) {
                $filter->whereNull('max_acreage')
                    ->orWhere('max_acreage', '>=', $room->acreage);
            })
            ->where(function ($filter) use ($room) {
                $filter->whereNull('min_budget')
                    ->orWhere('min_budget', '<=', $room->price);
            })
            ->where(function ($filter) use ($room) {
                $filter->whereNull('max_budget')
                    ->orWhere('max_budget', '>=', $room->price);
            })->where(function ($filter) use ($lat, $lon) {
                $filter->whereNull('radius')
                    ->orWhereRaw("6371 * acos(
                        cos( radians($lat) )
                      * cos( radians( JSON_EXTRACT(location, '$.lat') ) )
                      * cos( radians( JSON_EXTRACT(location, '$.lng') ) - radians($lon) )
                      + sin( radians($lat) )
                      * sin( radians( JSON_EXTRACT(location, '$.lat') ) )
                        ) <= radius ");
            })
            ->select(['id', 'user_id'])
            ->with('user')
            ->get();

        foreach ($subscriptions as $subscription) {
            $subscription->user->notify(new NewPublishRoom($room));
        }
    }
}
