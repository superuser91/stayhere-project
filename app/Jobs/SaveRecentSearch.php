<?php

namespace App\Jobs;

use App\RecentSearch;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveRecentSearch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $userId;
    public $queryText;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $queryText)
    {
        $this->userId = $userId;
        $this->queryText = $queryText;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->queryText && $this->userId) {
            $recentSearch = RecentSearch::updateOrCreate([
                'user_id' => $this->userId,
                'text' => $this->queryText
            ]);

            $recentSearch->touch();
        }
    }
}
