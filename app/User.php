<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'avatar',
        'phone_number',
        'fb_id',
        'google_id',
        'fcm_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function toSearchableArray()
    {
        $array = $this->only([
            'id',
            'name',
            'email',
            'phone_number',
            'avatar'
        ]);

        $array['messagable_type'] = get_class($this);

        return $array;
    }

    public function receivesBroadcastNotificationsOn()
    {
        return 'user.' . $this->id;
    }

    public function routeNotificationForFcm()
    {
        return $this->fcm_token;
    }

    public function messages()
    {
        return $this->morphToMany(Message::class, 'messagable');
    }

    public function participants()
    {
        return $this->morphMany(Participant::class, 'messagable');
    }

    public function favoriteRoomIds()
    {
        return $this->hasMany(FavoriteRoom::class);
    }

    public function favoriteRooms()
    {
        return $this->hasManyThrough(Room::class, FavoriteRoom::class, 'user_id', 'id', 'id', 'room_id')
            ->with('apartment:id,location')
            ->latest();
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function recentSearch()
    {
        return $this->hasMany(RecentSearch::class)->limit(5);
    }

    public function subscribe()
    {
        return $this->hasOne(Subscription::class);
    }
}
