<?php

namespace App\Fcm;

use App\Jobs\FcmNotificationJob;
use Kreait\Firebase\Messaging\CloudMessage;
use NotificationChannels\Fcm\FcmChannel as FcmBase;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use NotificationChannels\Fcm\Exceptions\CouldNotSendNotification;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Messaging\Message;

class FcmChannel extends FcmBase
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return array
     * @throws \NotificationChannels\Fcm\Exceptions\CouldNotSendNotification
     * @throws \Kreait\Firebase\Exception\FirebaseException
     */
    public function send($notifiable, Notification $notification)
    {
        $fcmToken = $notifiable->routeNotificationFor('fcm', $notification);

        Log::debug("fcmtoken", [$fcmToken]);

        if (empty($fcmToken)) {
            return;
        }

        // Get the message from the notification class
        $data = $notification->toFcm($notifiable);

        // build message
        $fcmMessage = CloudMessage::withTarget('token', $fcmToken)
            ->withData($data['data'])
            ->withNotification($data['notification']);

        try {
            dispatch(new FcmNotificationJob($fcmMessage));
        } catch (MessagingException $exception) {
            $this->failedNotification($notifiable, $notification, $exception);
            throw CouldNotSendNotification::serviceRespondedWithAnError($exception);
        }
    }
}
