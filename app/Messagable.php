<?php

namespace App;

use Algolia\ScoutExtended\Searchable\Aggregator;

class Messagable extends Aggregator
{
    protected $models = [
        User::class,
        Manager::class,
   ];
}
