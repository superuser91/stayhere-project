<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_id',
        'manager_id',
        'user_id',
        'room_id',
        'room_info',
        'stackholder_info',
        'valid_from',
        'valid_to',
        'user_confirm_at',
        'contract_term',
        'deposit_confirmed_at',
        'manager_canceled_at',
        'user_canceled_at',
    ];

    protected $casts = [
        'stackholder_info' => 'json',
        'user_confirm_at' => 'datetime',
        'room_info' => 'json',
        'valid_from' => 'datetime',
        'valid_to' => 'datetime',

    ];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class)->withTrashed();
    }

    public function invoices()
    {
        return $this->morphMany(Invoice::class, 'costable');
    }
}
