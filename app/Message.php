<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'conversation_id',
        'data',
        'messagable_type',
        'messagable_id',
    ];

    protected $casts = [
        'data' => 'json'
    ];

    protected $appends = [
        'avatar',
        'isMyMessage'
    ];

    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function messagable()
    {
        return $this->morphTo();
    }

    public function getAvatarAttribute()
    {
        return $this->messagable->avatar;
    }

    public function getIsMyMessageAttribute()
    {
        if (auth()->user()) {
            return $this->messagable_type == get_class(auth()->user())
                && $this->messagable_id == auth()->id();
        }
        return false;
    }
}
