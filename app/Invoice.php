<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'room_id',
        'costable_type',
        'costable_id',
        'amount',
        'months',
        'paid_at',
    ];

    protected $casts = [
        'months' => 'array',
        'costable_reference' => 'json'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function costable()
    {
        return $this->morphTo();
    }
}
