<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Room extends Model
{
    use Searchable;
    use SoftDeletes;

    protected $fillable = [
        'apartment_id',
        'title',
        'room_number',
        'description',
        'images',
        'price',
        'acreage',
        'is_public',
        'contract_id',
        'thumbnail',
    ];

    protected $casts = [
        'description' => 'json',
        'images' => 'json',
    ];

    protected $appends = [
        'isInFavoriteList'
    ];

    // protected $touches = ['apartment'];

    public function toSearchableArray()
    {
        $this->load(['apartment.manager']);

        $array = $this->toArray();

        $array['_geoloc'] = [
            'lat' => $array['apartment']['location']['lat'],
            'lng' => $array['apartment']['location']['lon'],
        ];

        return $array;
    }

    public function apartment()
    {
        return $this->belongsTo(Apartment::class);
    }

    public function post()
    {
        return $this->hasOne(Post::class)->latest();
    }

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function livingInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->where('costable_type', config('const.invoice.types.living_cost.code'));
    }

    public function houseInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->where('costable_type', config('const.invoice.types.house_cost.code'));
    }

    public function recentLivingInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->where('costable_type', config('const.invoice.types.living_cost.code'))
            ->latest()
            ->limit(5);
    }

    public function recentHouseInvoices()
    {
        return $this->hasMany(Invoice::class)
            ->where('costable_type', config('const.invoice.types.house_cost.code'))
            ->latest()
            ->limit(5);
    }

    public function getIsInFavoriteListAttribute()
    {
        $user = auth('api')->user();

        if (!is_null($user) && $user->favoriteRoomIds()->where('room_id',  $this->id)->exists()) {
            return true;
        }

        return false;
    }
}
