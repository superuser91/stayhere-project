<?php

use App\Apartment;
use App\Manager;
use App\Room;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $managers = factory(Manager::class, 3)
            ->create();

        foreach ($managers as $manager) {
            $apartments =
                factory(Apartment::class, 3)
                ->make(['manager_id' => $manager->id]);
            foreach ($apartments as $apartment) {
                $apartment->save();
                factory(Room::class, 3)
                    ->create(['apartment_id' => $apartment->id]);
            }
        }
    }
}
