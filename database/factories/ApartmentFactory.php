<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Apartment;
use App\Utils\JsonDB;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

$factory->define(Apartment::class, function (Faker $faker) {
    $provinces = JsonDB::decodedLocations();
    $province = $provinces[0];
    $district = $faker->randomElement($province['districts']);
    $ward = $faker->randomElement($district['wards']);
    $address = join(' ', [
        $ward['name'], $district['name'], $province['name']
    ]);
    $location = getLatLng($address);
    return [
        'title' =>  join(', ', [$ward['name'], $district['name'], $province['name']]),
        'location' => [
            'lat' => $location['lat'],
            'lon' => $location['lng'],
            'desc' => join(', ', [$ward['name'], $district['name'], $province['name']])
        ],
        'images' => [$faker->imageUrl()],
        'province_id' => $province['province_id'],
        'district_id' => $district['district_id'],
        'ward_id' => $ward['ward_id'],
        'thumbnail' => $faker->imageUrl(),
    ];
});

function getLatLng($address)
{
    $client = new Client();

    $response = $client->get('https://maps.googleapis.com/maps/api/geocode/json', [
        'query' => [
            'address' => $address,
            'key' => config('google.maps.api_key'),
        ]
    ]);

    $data = json_decode($response->getBody(), true)['results'];

    if (count($data)) {
        return $data[0]['geometry']['location'];
    }

    return [
        'lat' => 21.0277644,
        'lng' => 105.8341598,
    ];
}
