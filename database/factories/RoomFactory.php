<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Apartment;
use App\Room;
use Buihuycuong\Vnfaker\VNFaker;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'title' => 'Phòng ' . $roomNumber = $faker->numerify('###'),
        'room_number' => $roomNumber,
        'description' => [
            'type' => $type = $faker->randomElement(array_keys(config('const.room.types'))),
            'ultilities' => $ultilities = $faker->randomElements(array_keys(config('const.room.ultilities'))),
            'tags' => array_merge(
                $faker->randomElements(config('const.room.ultilities')),
                [config('const.room.types')[$type]]
            ),
            'payment' => [
                'deposit' => floatval($faker->numerify('##00000')),
                'recurring' => $faker->randomElement([1, 3, 6, 12, 24, 36, 60])
            ],
            'limit' => $faker->randomElement([1, 2, 3, 4, 5, null])
        ],
        'images' => [$faker->imageUrl(), $faker->imageUrl(), $faker->imageUrl(), $faker->imageUrl()],
        'price' => floatval($faker->numerify('##00000')),
        'acreage' => rand(10, 100),
        'is_public' => config('const.room.public_room'),
        'thumbnail' => $faker->imageUrl(),
        'apartment_id' => factory(Apartment::class)
    ];
});
