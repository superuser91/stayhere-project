<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Apartment;
use App\Manager;
use Faker\Generator as Faker;

$factory->define(Manager::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'email' => $faker->safeEmail,
        'password' => bcrypt('12345678'),
        'address' => $faker->address,
        'phone_number' => $faker->phoneNumber,
    ];
});
