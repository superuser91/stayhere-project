<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('apartment_id');
            $table->string('title');
            $table->string('room_number')->nullable();
            $table->json('description')->nullable();
            $table->json('images')->nullable();
            $table->float('price', 19, 2);
            $table->float('acreage', 8, 2);
            $table->unsignedBigInteger('contract_id')->nullable();
            $table->integer('is_public')->default(config('const.room.private_room'));
            $table->string('thumbnail', 2048)->default('/images/default_thumbnail.png');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
