<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('manager_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('room_id');
            $table->json('room_info');
            $table->json('stackholder_info');
            $table->date('valid_from');
            $table->date('valid_to');
            $table->unsignedInteger('contract_term');
            $table->timestamp('user_confirm_at')->nullable();
            $table->timestamp('deposit_confirmed_at')->nullable();
            $table->timestamp('manager_canceled_at')->nullable();
            $table->timestamp('user_canceled_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
