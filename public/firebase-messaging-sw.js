// [START messaging_init_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyC5KU_Z8sSINUrK0qCqwlgxIRyzZpzZrmI",
    authDomain: "fcm-demo-40c9f.firebaseapp.com",
    projectId: "fcm-demo-40c9f",
    storageBucket: "fcm-demo-40c9f.appspot.com",
    messagingSenderId: "681501275251",
    appId: "1:681501275251:web:a100af95619a78a3ffe606"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// [END messaging_init_in_sw]
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: 'https://images.theconversation.com/files/93616/original/image-20150902-6700-t2axrz.jpg' //your logo here
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
