const staticCacheName = 'site-static-v1';
const staticAssest = [
    '/icons/64.png',
    '/icons/144.png',
    '/icons/192.png',
    '/icons/512.png',
    '/app.webmanifest',
];



self.addEventListener('install', (evt) => {
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            cache.addAll(staticAssest).catch(err => console.log('err', err));
        }).catch(err => {
            console.log('err', err)
        })
    )
});

self.addEventListener('activate', (evt) => {
    evt.waitUntil(
        caches.keys().then(keys => {
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key)));
        })
    )
});

self.addEventListener('fetch', (evt) => {
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request);
        }).catch(() =>
        //caches.match('/html/fallback.html')
        { }
        )
    )
});
