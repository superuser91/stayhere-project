<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Broadcast::routes(['middleware' => ['auth:api']]);

Route::prefix('v1')->name('api.')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::get('/profile', 'Auth\ProfileController@profile')->name('profile.show');
        Route::post('/profile', 'Auth\ProfileController@update')->name('profile.update');
        Route::post('/change-password', 'Auth\ChangePasswordController@changePassword')->name('password.change');
        Route::post('/avatar', 'UploadAvatarController@update')->name('avatar.update');
        Route::get('favorites', 'FavoriteRoomController@index');
        Route::post('/rooms/d/{room}/favorites', 'FavoriteRoomController@store');
        Route::delete('/rooms/d/{room}/favorites', 'FavoriteRoomController@destroy');
        Route::post('/subscribe', 'SubscribeController@subscribe');
        Route::get('/conversations', 'ChatController@listConversation');
        Route::post('/conversations', 'ChatController@createConversation');
        Route::post('/conversations/managers/{manager}', 'ChatController@createConversationWithManager');
        Route::get('/conversations/d/{conversation}', 'ChatController@getConversation');
        Route::post('/conversations/d/{conversation}', 'ChatController@sendMessage')->name('message.send');
        Route::post('/rooms/d/{room}/order', 'OrderController@createOrder')->name('rooms.order');
        Route::get('/contracts/d/{contract}', 'ContractController@showContract')->name('contracts.show');
        Route::post('/contracts/d/{contract}/accept', 'ContractController@acceptContract')->name('contracts.accept');
        Route::post('/contracts/d/{contract}/decline', 'ContractController@declineContract')->name('contracts.decline');
        Route::get('/notifications', 'NotificationController@index');
        Route::post('/notifications/d/{notification}/mark/read', 'NotificationController@markAsRead');
        Route::post('/notifications/mark/read', 'NotificationController@markAllAsRead');
        Route::get('/search/recent', 'SearchController@getRecentSearch');
        Route::get('/rooms/requests', 'OrderController@index');
        Route::get('/rooms/requests/d/{id}', 'OrderController@show');
        Route::get('/invoices', 'InvoiceController@index');
        Route::get('/invoices/d/{invoice}', 'InvoiceController@show');
        Route::get('/living-cost', 'TrackingController@livingCostList');
        Route::get('/living-cost/d/{id}', 'TrackingController@showLivingCost');
        Route::get('/subscription', 'TrackingController@subscription');
        Route::post('/subscription', 'TrackingController@updateSubscription');
        Route::post('/connect/facebook', 'Auth\ConnectController@connectToFacebook');
        Route::post('/connect/google', 'Auth\ConnectController@connectToGoogle');
        Route::post('/fcm/token', 'Auth\FcmController@updateToken');
        Route::delete('/fcm/token', 'Auth\FcmController@removeToken');
    });
    Route::get('/search', 'SearchController@index')->name('search');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::post('/register', 'Auth\RegisterController@register')->name('register');
    Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.forgot');
    Route::get('/recommendations', 'RecommendationController@recommendation');
    Route::post('/login/facebook', 'Auth\LoginController@loginWithFacebook');
    Route::post('/login/google', 'Auth\LoginController@loginWithGoogle');
    Route::get('/rooms/d/{room}', 'RoomController@show');
});

Route::get('/', function () {
    return redirect(route('app.dashboard'));
});
