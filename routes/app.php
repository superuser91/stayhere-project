<?php

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "DashboardController@index")->name('dashboard');

Route::get('/apartments/d/{apartment}', 'ApartmentController@show')->name('apartments.show');
Route::get('/rooms/d/{room}', 'RoomController@show')->name('rooms.show');

Route::middleware(['auth'])->group(function () {
    Route::post('/rooms/d/{room}/order', 'OrderController@createOrder')->name('rooms.order');
    Route::post('/rooms/d/{room}/favorite', 'RoomController@addFavoriteRoom')->name('rooms.favorite.add');
    Route::delete('/rooms/d/{room}/favorite', 'RoomController@removeFavoriteRoom')->name('rooms.favorite.remove');
    Route::get('/contracts/d/{contract}/accept', 'ContractController@acceptContract')->name('contracts.accept');
    Route::get('/contracts/d/{contract}/decline', 'ContractController@declineContract')->name('contracts.decline');
});
