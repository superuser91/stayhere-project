<?php

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Conversation;

Auth::routes();

Broadcast::routes(['middleware' => ['auth:api']]);

Route::get('/', 'DashboardController@index');

Route::get('/home', 'HomeController@index')->name('home');
