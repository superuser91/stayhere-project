<?php

/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Broadcast::routes(['middleware' => ['auth:manager']]);
Route::name('auth.')->group(function () {
    Route::get('/logout', 'Auth\LogoutController@logout');
    Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
    Route::post('/register', 'Auth\RegisterController@register')->name('register');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('register');
});

Route::middleware(['auth:manager'])->group(function () {
    Route::group([], function () {
        Route::get('/', 'DashboardController@index');
        Route::get('/collapse-sidebar', 'Action\CollapseSidebar');
        Route::delete('/uploaded/images', 'Action\DeleteImage');
    });

    Route::group([], function () {
        Route::get('/messages', 'ChatController@index')->name('messages');
        Route::post('/conversations', 'ChatController@createConversation')->name('conversation.create.ajax');
        Route::get('/conversations/d/{conversation}', 'ChatController@show')->name('conversation.show');
        Route::post('/conversations/d/{conversation}', 'ChatController@sendMessage')->name('message.send');
        Route::get('/conversations/d/{conversation}/ajax', 'ChatController@listMessage')->name('message.show.ajax');
        Route::get('/messagables', 'ChatController@searchMessagable');
        Route::get('/requests', 'RequestController@index')->name('requests.index');
        Route::get('/requests/d/{order}/accept', 'RequestController@createContract')->name('requests.accept');
        Route::get('/requests/d/{order}/decline', 'RequestController@declineRequest')->name('requests.decline');
        Route::post('/requests/d/{order}/accept', 'RequestController@acceptRequest')->name('contracts.create');
        Route::get('/contracts/d/{contract}/deposit/confirm', 'RequestController@confirmDeposit')->name('contract.deposit.confirm');
        Route::get('/contracts/d/{contract}/cancel', 'RequestController@cancelContract')->name('contract.cancel');
    });

    Route::group([], function () {
        Route::resource('apartments', 'ApartmentController');
        Route::get('/apartments/{apartment}/rooms', 'RoomController@create')->name('apartments.rooms.create');
        Route::post('/apartments/images', 'ApartmentController@uploadImage')->name('apartments.images.upload');
        Route::post('/apartments/{apartment}/rooms', 'RoomController@store')->name('rooms.store');
    });
    Route::prefix('rooms')->name('rooms.')->group(function () {
        Route::get('/', 'RoomController@index')->name('index');
        Route::get('/leased', 'RoomController@leasedRoom')->name('list.leased');
        Route::get('/empty', 'RoomController@emptyRoom')->name('list.empty');
        Route::get('/posts', 'RoomController@listPosts')->name('list.posts');
        Route::post('/d/{room}/posts/enable/ajax', 'PostController@enablePost')->name('post.enable');
        Route::post('/d/{room}/posts/disable/ajax', 'PostController@disablePost')->name('post.disable');
        Route::get('/d/{room}', 'RoomController@show')->name('show');
        Route::get('/d/{room}/edit', 'RoomController@edit')->name('edit');
        Route::put('/d/{room}/edit', 'RoomController@update')->name('update');
        Route::delete('/d/{room}', 'RoomController@destroy')->name('destroy');
    });
    Route::prefix('statistics')->name('statistic.')->group(function () {
        Route::get('/income', 'StatisticController@incomeSummary')->name('income');
        Route::get('/tenants', 'StatisticController@tenantSummary')->name('tenant');
        Route::get('/invoices', 'StatisticController@invoiceSummary')->name('invoice');
        Route::get('/invoices/d/{invoice}', 'StatisticController@showInvoice')->name('invoice.detail');
        Route::post('/invoices/d/{invoice}', 'StatisticController@confirmPaid')->name('invoice.confirm.paid');
        Route::delete('/invoices/d/{invoice}', 'StatisticController@deleteInvoice')->name('invoice.delete');
    });
    Route::prefix('management')->name('management.')->group(function () {
        Route::get('/living-cost', 'ManagementController@livingCost')->name('living.cost');
        Route::get('/house-cost', 'ManagementController@houseCost')->name('house.cost');
        Route::get('/rooms/d/{room}/living-cost/invoices/create', 'InvoiceController@createLivingInvoice')->name('living.invoices.create');
        Route::post('/rooms/d/{room}/living-cost/invoices', 'InvoiceController@storeLivingInvoice')->name('living.invoices.store');
        Route::get('/rooms/d/{room}/house-cost/invoices/create', 'InvoiceController@createHouseInvoice')->name('house.invoices.create');
        Route::post('/rooms/d/{room}/house-cost/invoices', 'InvoiceController@storeHouseInvoice')->name('house.invoices.store');
    });
});
