importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js');
 // Your web app's Firebase configuration
 var firebaseConfig = {
    apiKey: "AIzaSyC5KU_Z8sSINUrK0qCqwlgxIRyzZpzZrmI",
    authDomain: "fcm-demo-40c9f.firebaseapp.com",
    projectId: "fcm-demo-40c9f",
    storageBucket: "fcm-demo-40c9f.appspot.com",
    messagingSenderId: "681501275251",
    appId: "1:681501275251:web:a100af95619a78a3ffe606"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// obtain messaging class from Firebase
const messaging = firebase.messaging();
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/firebase-messaging-sw.js')
        .then((registration) => {
            messaging.useServiceWorker(registration);
            // request notification permission and get token
            console.log('Registration successful, scope is:',
                registration.scope);
            //TODO: ask For Permission To Receive Notifications
        }).catch(function (err) {
            console.log('Service worker registration failed, error:', err);
        });
}
messaging.getToken({ vapidKey: 'BFPIoGtB3bVO94aSm0is6yR6R12AdFNX2taao1JXueqbKTdtirD3uomOWBaXcBTHuLd9H_DlRskQPl7L1ojCLqE' }).then((currentToken) => {
    if (currentToken) {
        // Send the token to your server and update the UI if necessary
        // ...
        console.log(currentToken)
    } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        // ...
    }
}).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
});

messaging.onMessage(function (payload) {
    console.log(payload);
    var notify;
    notify = new Notification("OK nhe", {
        body: "ok",
        icon: 'images/avatar.png',
        tag: "Dummy"
    });
    console.log(payload.notification);
});
