<div class="content-wrapper">
    <!-- Main content -->
    <section class="content pb-3">
        <div class="container-fluid">
            @yield('content')
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
