<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- SEO Meta Tags -->
    <meta name="description" content="Stayhere is web & app platform help people can find house, hotel, motel free" />
    <meta name="author" content="dung199551" />

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="" />
    <!-- website name -->
    <meta property="og:site" content="Stayhere" />
    <!-- website link -->
    <meta property="og:title" content="Stayhere" />
    <!-- title shown in the actual shared post -->
    <meta property="og:description" content="" />
    <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" />
    <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" />
    <!-- where do you want your post to link to -->
    <meta property="og:type" content="article" />

    <!-- Webpage Title -->
    <title>Stayhere - Tìm để ở lại</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap"
        rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <link href="css/swiper.css" rel="stylesheet" />
    <link href="css/magnific-popup.css" rel="stylesheet" />
    <link href="css/styles.css" rel="stylesheet" />

    <!-- Favicon  -->
    <link rel="icon" href="images/favicon.png" />
</head>

<body data-spy="scroll" data-target=".fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark">
        <div class="container">
            <!-- Text Logo - Use this if you don't have a graphic logo -->
            <!-- <a class="navbar-brand logo-text" href="index.html">Leno</a> -->

            <!-- Image Logo -->
            {{-- <a class="navbar-brand logo-image" href="/"><img src="images/logo.png" alt="stayhere" /></a> --}}
            <a class="navbar-brand logo-image" href="/"><i class="fas fa-home"></i></a>
            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="https://manage.stayhere.tk">Quản lý phòng</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Liên hệ</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Điều khoản</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Điều khoản sử dụng</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Chính sách riêng tư</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://api.stayhere.tk/register">Đăng ký</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://api.stayhere.tk/login">Đăng nhập</a>
                    </li>
                </ul>
                <span class="nav-item social-icons">
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-facebook-f fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-twitter fa-stack-1x"></i>
                        </a>
                    </span>
                </span>
            </div>
            <!-- end of navbar-collapse -->
        </div>
        <!-- end of container -->
    </nav>
    <!-- end of navbar -->
    <!-- end of navigation -->

    <!-- Header -->
    <header id="header" class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h1>
                            TÌM PHÒNG <br />

                        </h1>
                        <h3><span id="js-rotating">Ở GẦN BẠN, MỌI LÚC, NGAY TỨC THÌ, ĐẾN KHI ƯNG THÌ THÔI</span></h3>
                        <p class="p-large">
                            Truy cập trang web hoặc tải ứng dụng của chúng tôi để tiếp tục
                            sử dụng các tính năng giúp bạn tìm kiếm và quản lý phòng trọ của
                            mình
                        </p>
                        <a class="btn-solid-lg p-4" href="#your-link"><i class="fab fa-apple"></i>Coming soon</a>
                        <a class="btn-solid-lg mb-3 p-4" href="{{$androidDownloadLink}}"><i
                                class="fab fa-google-play"></i>Tải xuống</a>
                        <a class="d-block text-decoration-none" href="/"><i
                                class="fas fa-globe-americas mr-2"></i></i>Tiếp
                            tục dùng phiên bản web</a>
                    </div>
                    <!-- end of text-container -->
                </div>
                <!-- end of col -->
                <div class="col-lg-6">
                    {{-- <img class="img-fluid" src="images/header-smartphones.png" alt="alternative" /> --}}
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </header>
    <!-- end of header -->
    <!-- end of header -->

    <!-- Features -->
    {{-- <div class="tabs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading">CHỨC NĂNG</h2>
                    <div class="p-heading">
                        Stayhere được xây dựng để giúp mọi người có một trải nghiệm tìm
                        kiếm và quản lý phòng trọ dễ dàng và tiện lợi nhất.
                    </div>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <!-- Tabs Links -->
                <ul class="nav nav-tabs" id="templateTabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="nav-tab-1" data-toggle="tab" href="#tab-1" role="tab"
                            aria-controls="tab-1" aria-selected="true"><i class="fas fa-search"></i>TÌM KIẾM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="nav-tab-2" data-toggle="tab" href="#tab-2" role="tab"
                            aria-controls="tab-2" aria-selected="false"><i class="fas fa-binoculars"></i>ĐĂNG KÝ THEO
                            DÕI</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="nav-tab-3" data-toggle="tab" href="#tab-3" role="tab"
                            aria-controls="tab-3" aria-selected="false"><i class="fas fa-cog"></i>QUẢN LÝ</a>
                    </li>
                </ul>
                <!-- end of tabs links -->

                <!-- Tabs Content-->
                <div class="tab-content" id="templateTabsContent">
                    <!-- Tab -->
                    <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                        <div class="container">
                            <div class="row">
                                <!-- Icon Cards Pane -->
                                <div class="col-lg-4">
                                    <ul class="list-unstyled li-space-lg first">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-compass fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Goal Setting</h4>
                                                <p>
                                                    Like any self improving process, everything starts
                                                    with setting your goals and committing to them
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-code fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Visual Editor</h4>
                                                <p>
                                                    Leno provides a well designed and ergonomic visual
                                                    editor for you to edit your quick notes
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-gem fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Refined Options</h4>
                                                <p>
                                                    Each option packaged in the app's menus is provided
                                                    in order to improve you personally
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end of col -->
                                <!-- end of icon cards pane -->

                                <!-- Image Pane -->
                                <div class="col-lg-4">
                                    <img class="img-fluid" src="images/features-smartphone-1.png" alt="alternative" />
                                </div>
                                <!-- end of col -->
                                <!-- end of image pane -->

                                <!-- Icon Cards Pane -->
                                <div class="col-lg-4">
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-calendar-alt fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Calendar Input</h4>
                                                <p>
                                                    Schedule your appointments, meetings and periodical
                                                    evaluations using the tools
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-book fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Easy Reading</h4>
                                                <p>
                                                    Reading focus mode for long form articles, ebooks
                                                    and other materials with long text
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-cube fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Good Foundation</h4>
                                                <p>
                                                    Get a solid foundation for your self development
                                                    efforts. Try Leno mobile app for devices
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end of col -->
                                <!-- end of icon cards pane -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of container -->
                    </div>
                    <!-- end of tab-pane -->
                    <!-- end of tab -->

                    <!-- Tab -->
                    <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="tab-2">
                        <div class="container">
                            <div class="row">
                                <!-- Image Pane -->
                                <div class="col-lg-4">
                                    <img class="img-fluid" src="images/features-smartphone-2.png" alt="alternative" />
                                </div>
                                <!-- end of col -->
                                <!-- end of image pane -->

                                <!-- Text And Icon Cards Area -->
                                <div class="col-lg-8">
                                    <h3>Track Result Based On Your</h3>
                                    <p class="sub-heading">
                                        After you've configured the app and settled on the data
                                        gathering techniques you can start the information
                                        trackers and begin collecting those long awaited
                                        interesting details.
                                    </p>
                                    <ul class="list-unstyled li-space-lg first">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-cube fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Good Foundation</h4>
                                                <p>
                                                    Get a solid foundation for your self development
                                                    efforts. Try Leno mobile app now
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-book fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Easy Reading</h4>
                                                <p>
                                                    Reading focus mode for long form articles, ebooks
                                                    and other materials with long text
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-compass fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Goal Setting</h4>
                                                <p>
                                                    Like any self improving process, everything starts
                                                    with setting goals and comiting
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-calendar-alt fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Calendar Input</h4>
                                                <p>
                                                    Schedule your appointments, meetings and periodical
                                                    evaluations using the tools
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-code fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Visual Editor</h4>
                                                <p>
                                                    Leno provides a well designed and ergonomic visual
                                                    editor for you to edit your notes
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-gem fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Refined Options</h4>
                                                <p>
                                                    Each option packaged in the app's menus is provided
                                                    in order to improve you personally
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end of col -->
                                <!-- end of text and icon cards area -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of container -->
                    </div>
                    <!-- end of tab-pane -->
                    <!-- end of tab -->

                    <!-- Tab -->
                    <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="tab-3">
                        <div class="container">
                            <div class="row">
                                <!-- Text And Icon Cards Area -->
                                <div class="col-lg-8">
                                    <ul class="list-unstyled li-space-lg first">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-cube fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Good Foundation</h4>
                                                <p>
                                                    Get a solid foundation for your self development
                                                    efforts. Try Leno mobile app today
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-book fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Easy Reading</h4>
                                                <p>
                                                    Reading focus mode for long form articles, ebooks
                                                    and other materials with long text
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-compass fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Goal Setting</h4>
                                                <p>
                                                    Like any self improving process, everything starts
                                                    with setting your goals and comiting
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled li-space-lg">
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-calendar-alt fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Calendar Input</h4>
                                                <p>
                                                    Schedule your appointments, meetings and periodical
                                                    evaluations using the tools
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-code fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Visual Editor</h4>
                                                <p>
                                                    Leno provides a well designed and ergonomic visual
                                                    editor for you to edit your notes
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="fa-stack">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="far fa-gem fa-stack-1x"></i>
                                            </span>
                                            <div class="media-body">
                                                <h4>Refined Options</h4>
                                                <p>
                                                    Each option packaged in the app's menus is provided
                                                    in order to improve you personally
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3>Monitoring Tools Evaluation</h3>
                                    <p class="sub-heading">
                                        Monitor the evolution of your finances and health state
                                        using tools integrated in Leno. The generated real time
                                        reports can be filtered based on any desired criteria.
                                    </p>
                                </div>
                                <!-- end of col -->
                                <!-- end of text and icon cards area -->

                                <!-- Image Pane -->
                                <div class="col-lg-4">
                                    <img class="img-fluid" src="images/features-smartphone-3.png" alt="alternative" />
                                </div>
                                <!-- end of col -->
                                <!-- end of image pane -->
                            </div>
                            <!-- end of row -->
                        </div>
                        <!-- end of container -->
                    </div>
                    <!-- end of tab-pane -->
                    <!-- end of tab -->
                </div>
                <!-- end of tab-content -->
                <!-- end of tabs content -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div> --}}
    <!-- end of tabs -->
    <!-- end of features -->

    <!-- Details 1 -->
    {{-- <div class="basic-2 bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="images/details-1.png" alt="alternative" />
                    </div>
                    <!-- end of image-container -->
                </div>
                <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>Start using Leno today and set your long term goals</h2>
                        <p>
                            Leno can easily help you track your personal development
                            evolution if you take the time to properly setup your goals at
                            the beginning of the training process. Check it out
                        </p>
                        <a class="btn-solid-reg popup-with-move-anim" href="#details-1-lightbox">Lightbox</a>
                    </div>
                    <!-- end of text-container -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div> --}}
    <!-- end of basic-2 -->
    <!-- end of details 1 -->

    <!-- Details 1 Lightbox -->
    <!-- Lightbox -->
    {{-- <div id="details-1-lightbox" class="lightbox-basic zoom-anim-dialog mfp-hide">
        <div class="row">
            <button title="Close (Esc)" type="button" class="mfp-close x-button">
                ×
            </button>
            <div class="col-lg-8">
                <div class="image-container">
                    <img class="img-fluid" src="images/details-1-lightbox.jpg" alt="alternative" />
                </div>
                <!-- end of image-container -->
            </div>
            <!-- end of col -->
            <div class="col-lg-4">
                <h3>Goals Setting</h3>
                <hr />
                <p>
                    The app can easily help you track your personal development
                    evolution if you take the time to set it up.
                </p>
                <h4>User Feedback</h4>
                <p>
                    This is a great app which can help you save time and make your live
                    easier. And it will help improve your productivity.
                </p>
                <ul class="list-unstyled li-space-lg">
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Splash screen panel</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Statistics graph report</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Events calendar layout</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Location details screen</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Onboarding steps interface</div>
                    </li>
                </ul>
                <a class="btn-solid-reg mfp-close" href="#header">Download</a>
                <button class="btn-outline-reg mfp-close as-button" type="button">
                    Back
                </button>
            </div>
            <!-- end of col -->
        </div>
        <!-- end of row -->
    </div> --}}
    <!-- end of lightbox-basic -->
    <!-- end of lightbox -->
    <!-- end of details 1 lightbox -->

    <!-- Screenshots -->
    {{-- <div class="slider-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="h2-heading">SCREENS</h2>
                    <hr class="hr-heading" />
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Image Slider -->
                    <div class="slider-container">
                        <div class="swiper-container image-slider">
                            <div class="swiper-wrapper">
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-1.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-1.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-2.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-2.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-3.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-3.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-4.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-4.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-5.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-5.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-6.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-6.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-7.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-7.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-8.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-8.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-9.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-9.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <a href="images/screenshot-10.png" class="popup-link" data-effect="fadeIn">
                                        <img class="img-fluid" src="images/screenshot-10.png" alt="alternative" />
                                    </a>
                                </div>
                                <!-- end of slide -->
                            </div>
                            <!-- end of swiper-wrapper -->

                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->
                        </div>
                        <!-- end of swiper-container -->
                    </div>
                    <!-- end of slider-container -->
                    <!-- end of image slider -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div> --}}
    <!-- end of slider-2 -->
    <!-- end of screenshots -->

    <!-- Statistics -->
    {{-- <div class="counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Counter -->
                    <div id="counter">
                        <div class="cell">
                            <i class="fas fa-users"></i>
                            <div class="counter-value number-count" data-count="10031">
                                1
                            </div>
                            <p class="counter-info p-small">Khách hàng hài lòng</p>
                        </div>
                        <div class="cell">
                            <i class="fas fa-code green"></i>
                            <div class="counter-value number-count" data-count="12090">
                                1
                            </div>
                            <p class="counter-info p-small">Lượt đánh giá tốt</p>
                        </div>
                        <div class="cell">
                            <i class="fas fa-cog red"></i>
                            <div class="counter-value number-count" data-count="1591">
                                1
                            </div>
                            <p class="counter-info p-small">Đối tác</p>
                        </div>
                        <div class="cell">
                            <i class="fas fa-comments yellow"></i>
                            <div class="counter-value number-count" data-count="12708">
                                1
                            </div>
                            <p class="counter-info p-small">Số phòng đã cho thuê</p>
                        </div>
                        <div class="cell">
                            <i class="fas fa-rocket blue"></i>
                            <div class="counter-value number-count" data-count="63">1</div>
                            <p class="counter-info p-small">Tỉnh thành</p>
                        </div>
                    </div>
                    <!-- end of counter -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div> --}}
    <!-- end of counter -->
    <!-- end of statistics -->

    <!-- Footer -->
    <div class="footer bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-col first">
                        <h6>About Stayhere</h6>
                        <p class="p-small">
                            Stayhere được thiết kế để giúp mọi người có một trải nghiệm tìm
                            kiếm và quản lý một căn phòng trọ dễ dàng và tiện lợi nhất. Bằng
                            cách làm cầu nối giữa các đối tác kinh doanh dịch vụ phòng trọ,
                            chúng tôi đảm bảo có được những phòng trọ chất lượng, giá cả
                            cạnh tranh và dịch vụ đa dạng.
                        </p>
                    </div>
                    <!-- end of footer-col -->
                    <div class="footer-col second">
                        <h6>Links</h6>
                        <ul class="list-unstyled li-space-lg p-small">
                            <li>
                                <a href="terms.html">Điều khoản sử dụng</a>
                            </li>
                            <li>
                                <a href="privacy.html">Chính sách riêng tư</a>
                            </li>
                            <li>
                                <a href="#">Chức năng</a>
                            </li>
                            <li>
                                <a href="#">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end of footer-col -->
                    <div class="footer-col third">
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-pinterest-p fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <p class="p-small">
                            Chúng tôi rất mong nhận được phản hồi và góp ý của bạn
                            <a href="mailto:dung199551@gmail.com"><strong>dung199551@gmail.com</strong></a>
                        </p>
                    </div>
                    <!-- end of footer-col -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of footer -->
    <!-- end of footer -->

    <!-- Copyright -->
    <div class="copyright bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">
                        Copyright ©
                        <a href="https://stayhere.tk">Stayhere - Inspried by Leno</a>
                    </p>
                </div>
                <!-- end of col -->
            </div>
            <!-- enf of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of copyright -->
    <!-- end of copyright -->

    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script>
    <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script>
    <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Magnific Popup for lightboxes -->
    <script src="js/morphext.min.js"></script>
    <!-- Morphtext rotating text in the header -->
    <script src="js/validator.min.js"></script>
    <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script>
    <!-- Custom scripts -->
</body>

</html>
