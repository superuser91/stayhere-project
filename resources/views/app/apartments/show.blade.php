@extends('app.layouts.app')

@section('content')
<div class="d-none" id="latData" data-lat="{{ $apartment->location['lat'] }}"></div>
<div class="d-none" id="lonData" data-lon="{{ $apartment->location['lon'] }}"></div>
<div class="row pt-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Thông tin nhà trọ</div>
            <div class="card-body">
                <p>Địa chỉ: {{ $apartment->location['desc'] }}</p>
                <p>Họ tên chủ nhà: {{ $apartment->manager->name }}</p>
                <p>Số điện thoại: {{ $apartment->manager->phone_number }}</p>
                <p>Email: {{ $apartment->manager->email }}</p>
            </div>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-header">
                Danh sách phòng còn trống
            </div>
            <div class="card-body">
                @foreach ($apartment->publicRooms as $room)
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col" style="max-width: 250px;">
                            <img src="{{$room->thumbnail}}" class="card-img h-100 w-100" style="object-fit: cover">
                        </div>
                        <div class="col col-grow-1">
                            <div class="card-body">
                                <a href="{{ route('app.rooms.show', $room->id) }}"
                                    class="card-title">{{ $room->room_number }}</a>
                                <p class="card-text">Diện tích: {{ $room->acreage }} m<sup>2</sup></p>
                                <p class="card-text">
                                    Số người ở tối đa:
                                    {{ $room->description['limit'] ?? 'Không giới hạn' }}
                                </p>
                                <p class="card-text">
                                    <small class="text-muted">
                                        Giá
                                        {{number_format($room->price)}} VND
                                    </small>
                                </p>
                                <p>
                                    @foreach ($room->description['tags'] as $tag)
                                    <a class="badge badge-success">{{$tag}}</a>
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-header">Bản đồ</div>
            <div class="card-body">
                <div id="map-canvas" style="height: 500px"></div>
            </div>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-header">Ảnh minh hoạ căn nhà</div>
            <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" style="max-height: 500px;">
                        @foreach ($apartment->images ?? [] as $image)
                        <div class="carousel-item active">
                            <img src="{{$image}}" class="d-block w-100 h-100" style="object-fit: cover;">
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key={{ config('google.maps.api_key') }}&libraries=places&language=vi&region=VN&{{time()}}">
</script>
<script>
    var lat = $('#latData').data('lat');
    var lon = $('#lonData').data('lon');
    $(document).ready(function () {
        google.maps.event.addDomListener(window, 'load', initialize);
    })
    function initialize() {
        var position = new google.maps.LatLng(lat, lon);
        var mapOptions = {
            center: position,
            zoom: 15
        };
        const map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        var marker = new google.maps.Marker({
            position: position,
            map: map,
        });
    }
</script>

@endpush
