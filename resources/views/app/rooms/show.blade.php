@extends('app.layouts.app')

@section('content')
<div class="d-none" id="latData" data-lat="{{ $room->apartment->location['lat'] }}"></div>
<div class="d-none" id="lonData" data-lon="{{ $room->apartment->location['lon'] }}"></div>
<div class="row pt-3">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Thông tin phòng
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <label for="">Địa chỉ:</label>
                        {{ $room->apartment->location['desc'] }}
                    </div>
                    <div class="col-12">
                        <label for="acreage">Diện tích</label>
                        {{ $room->acreage }} m <sup>2</sup>
                    </div>
                    <div class="col-12">
                        <label for="">Giá phòng từng tháng:</label>
                        {{ number_format($room->price) }} VND
                    </div>
                    <div class="col-12">
                        <label for="">Chu kỳ thanh toán:</label>
                        @foreach (config('const.payment.recurring') as $id => $recurring)
                        @if($room->description['payment']['recurring']==$id)
                        {{$recurring}}
                        @endif
                        @endforeach
                    </div>
                    <div class="col-12">
                        <label for="">Tiền đặt cọc:</label>
                        {{ number_format($room->description['payment']['deposit']) }} VND
                    </div>
                    <div class="col-12">
                        <label for="">Số phòng, vị trí trong nhà:</label>
                        {{ $room->room_number }}
                    </div>
                    <div class="col-12">
                        <label for="people_limit">Số người ở tối đa</label>
                        {{ $room->description['limit'] ?? 'Không giới hạn' }}
                    </div>
                    <div class="col-12">
                        <label for="room-type">Loại phòng:</label>
                        @foreach (config('const.room.types') as $id => $type)
                        @if($room->description['type']== $id )
                        {{ $type }}
                        @endif
                        @endforeach
                    </div>
                    <div class="col-12">
                        <label for="ultilities">Tiện ích: </label>
                        @foreach ($room->description['tags'] as $tag)
                        <a class="badge badge-success">{{$tag}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <form class="d-inline" action="{{ route('app.rooms.order', $room->id )}}" method="POST">
                    @csrf
                    <button class="btn btn-success">Đặt phòng</button>
                </form>
                <form class="d-inline" action="{{ route('app.rooms.favorite.add', $room->id )}}" method="POST">
                    @csrf
                    <button class="btn btn-primary">Đánh dấu</button>
                </form>
                <a href="tel:+84368949601" class="btn btn-info float-right">Liên hệ</a>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Ảnh chụp phòng trọ</div>
            <div class="card body py-0">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" style="height: 500px;">
                        @foreach ($room->images ?? [] as $image)
                        <div class="carousel-item @if($loop->first) active @endif">
                            <img src="{{$image}}" class="d-block h-100">
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Bản đồ</div>
            <div class="card-body">
                <div id="map-canvas" style="height: 500px"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key={{ config('google.maps.api_key') }}&libraries=places&language=vi&region=VN&{{time()}}">
</script>
<script>
    $(function () {
        google.maps.event.addDomListener(window, 'load', initialize);
        $('#carouselExampleIndicators').carousel({
            interval: 3000,
            cycle: true
        });
    })
</script>
<script>
    var lat = $('#latData').data('lat');
    var lon = $('#lonData').data('lon');
    function initialize() {
        var position = new google.maps.LatLng(lat, lon);
        var mapOptions = {
            center: position,
            zoom: 15
        };
        const map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        var marker = new google.maps.Marker({
            position: position,
            map: map,
        });
    }
</script>

@endpush
