<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- SEO Meta Tags -->
    <meta name="description" content="Stayhere is web & app platform help people can find house, hotel, motel free" />
    <meta name="author" content="dung199551" />

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="stayherevn" />
    <!-- website name -->
    <meta property="og:site" content="https://stayhere.tk" />
    <!-- website link -->
    <meta property="og:title" content="Stayhere" />
    <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Tìm kiếm nhà trọ" />
    <!-- description shown in the actual shared post -->
    <meta property="og:image" content="/images/article-details-small.jpg" />
    <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" />
    <!-- where do you want your post to link to -->
    <meta property="og:type" content="page" />

    <!-- Webpage Title -->
    <title>Stayhere - Tìm để ở lại</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap"
        rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet" />
    <link href="/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/fontawesome-all.css" rel="stylesheet" />
    <link href="/css/swiper.css" rel="stylesheet" />
    <link href="/css/magnific-popup.css" rel="stylesheet" />
    <link href="/css/styles.css" rel="stylesheet" />
    <link href="/css/main.css" rel="stylesheet" />

    <!-- Favicon  -->
    <link rel="icon" href="images/favicon.png" />
</head>

<body data-spy="scroll" data-target=".fixed-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark">
        <div class="container">
            <!-- Text Logo - Use this if you don't have a graphic logo -->
            <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Leno</a> -->

            <!-- Image Logo -->
            <a class="navbar-brand logo-image" href="/"><img src="images/logo.png" alt="stayhere" /></a>

            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Liên hệ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('app.dashboard')}}">Ứng dụng</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Điều khoản</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="#">Điều khoản sử dụng</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Chính sách riêng tư</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/register">Đăng ký</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/login">Đăng nhập</a>
                    </li>
                </ul>
                <span class="nav-item social-icons">
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-facebook-f fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-twitter fa-stack-1x"></i>
                        </a>
                    </span>
                </span>
            </div>
            <!-- end of navbar-collapse -->
        </div>
        <!-- end of container -->
    </nav>
    <!-- end of navbar -->
    <!-- end of navigation -->

    <!-- Header -->
    <div class="s007">
        <form class="container">
            <h1 class="text-center d-none d-md-block d-lg-block d-xl-block">Tìm là thấy - kiểu gì cũng thấy, kiểu gì
                cũng thấy </h1>
            <div class="inner-form">
                <div class="basic-search">
                    <div class="input-field">
                        <div class="icon-wrap">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                                <path
                                    d="M18.869 19.162l-5.943-6.484c1.339-1.401 2.075-3.233 2.075-5.178 0-2.003-0.78-3.887-2.197-5.303s-3.3-2.197-5.303-2.197-3.887 0.78-5.303 2.197-2.197 3.3-2.197 5.303 0.78 3.887 2.197 5.303 3.3 2.197 5.303 2.197c1.726 0 3.362-0.579 4.688-1.645l5.943 6.483c0.099 0.108 0.233 0.162 0.369 0.162 0.121 0 0.242-0.043 0.338-0.131 0.204-0.187 0.217-0.503 0.031-0.706zM1 7.5c0-3.584 2.916-6.5 6.5-6.5s6.5 2.916 6.5 6.5-2.916 6.5-6.5 6.5-6.5-2.916-6.5-6.5z">
                                </path>
                            </svg>
                        </div>
                        <input id="search" type="text" placeholder="Yêu cầu..." name="address" />
                        <div class="result-count d-flex justify-content-end">
                            <i class="fas fa-sliders-h btn" data-toggle="collapse" data-target="#advance-search"
                                aria-expanded="false" aria-controls="advanceSearch"></i>
                        </div>
                    </div>
                </div>
                <div class="advance-search collapse" id="advance-search">
                    <span class="desc">Tìm kiếm nâng cao</span>
                    <div class="row mb-3" id="app">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="province_id">Tỉnh/Thành phố</label>
                            <select class="form-control select2" name="province_id" id="province_id"
                                v-model="selectedProvinceId" @change="changeProvince">
                                <option v-for="province in provinces" :value="province.province_id">
                                    @{{ province.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="district_id">Quận/Huyện</label>
                            <select class="form-control" name="district_id" id="district_id"
                                v-model="selectedDistrictId" @change="changeDistrict">
                                <option v-for="district in districts" :value="district.district_id">
                                    @{{ district.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Xã/Phường</label>
                            <select class="form-control" name="ward_id" id="ward_id" v-model="selectedWardId">
                                <option v-for="ward in wards" :value="ward.ward_id">
                                    @{{ ward.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row second justify-content-start mb-3">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Giá từ</label>
                            <input type="text" class="form-control money" placeholder="1,000,000">
                        </div>
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Đến</label>
                            <input type="text" class="form-control money" placeholder="5,000,000">
                        </div>
                    </div>
                    <div class="row third justify-content-end">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <button class="btn btn-success float-right">Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Footer -->
    <div class="footer bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-col first">
                        <h6>About Stayhere</h6>
                        <p class="p-small">
                            Stayhere được thiết kế để giúp mọi người có một trải nghiệm tìm
                            kiếm và quản lý một căn phòng trọ dễ dàng và tiện lợi nhất. Bằng
                            cách làm cầu nối giữa các đối tác kinh doanh dịch vụ phòng trọ,
                            chúng tôi đảm bảo có được những phòng trọ chất lượng, giá cả
                            cạnh tranh và dịch vụ đa dạng.
                        </p>
                    </div>
                    <!-- end of footer-col -->
                    <div class="footer-col second">
                        <h6>Links</h6>
                        <ul class="list-unstyled li-space-lg p-small">
                            <li>
                                <a href="terms.html">Điều khoản sử dụng</a>
                            </li>
                            <li>
                                <a href="privacy.html">Chính sách riêng tư</a>
                            </li>
                            <li>
                                <a href="features.html">Chức năng</a>
                            </li>
                            <li>
                                <a href="contact.html">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                    <!-- end of footer-col -->
                    <div class="footer-col third">
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-pinterest-p fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <p class="p-small">
                            Chúng tôi rất mong nhận được phản hồi và góp ý của bạn
                            <a href="mailto:dung199551@gmail.com"><strong>dung199551@gmail.com</strong></a>
                        </p>
                    </div>
                    <!-- end of footer-col -->
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container -->
    </div>
    <!-- end of footer -->
    <!-- end of footer -->

    <!-- Copyright -->
    <div class="copyright bg-dark-blue">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">
                        Copyright ©
                        <a href="https://stayhere.tk">Stayhere - Inspried by Leno</a>
                    </p>
                </div>
                <!-- end of col -->
            </div>
            <!-- enf of row -->
        </div>
        <!-- end of container -->
    </div>
    <div class="d-none" id="locations-data" data-locations="{{ json_encode($locations) }}"></div>

    <!-- end of copyright -->
    <!-- end of copyright -->

    <!-- Scripts -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- Axios -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script>
    <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/swiper.min.js"></script>
    <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- Magnific Popup for lightboxes -->
    <script src="js/morphext.min.js"></script>
    <!-- Morphtext rotating text in the header -->
    <script src="js/validator.min.js"></script>
    <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- ChoiceJS -->
    <script src="js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
        integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
        crossorigin="anonymous"></script>
    <script type="text/javascript">
        var provinces = $('#locations-data').data('locations');
        var vue = new Vue({
            el: '#app',
            data: {
                provinces: provinces,
                districts: provinces[0].districts,
                wards: provinces[0].districts[0].wards,
                selectedProvinceId: {},
                selectedDistrictId: {},
                selectedWardId: {},
                selectedProvince: {},
                selectedDistrict: {},
            },
            methods: {
                changeProvince: function () {
                    this.wards = [];
                    this.selectedProvince = provinces.find((province) => {
                        return province.province_id == this.selectedProvinceId
                    });
                    this.districts = this.selectedProvince.districts
                },
                changeDistrict: function () {
                    this.selectedDistrict = this.selectedProvince.districts.find((district) => {
                        return district.district_id == this.selectedDistrictId
                    });
                    this.wards = this.selectedDistrict.wards
                }
            },
        });

        $(document).ready(function () {
            $('.money').mask('000,000,000,000,000', { reverse: true });
        })
    </script>
</body>

</html>
