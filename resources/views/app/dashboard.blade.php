@extends('app.layouts.app')

@push('head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous"></script>
@endpush

@section('content')
<div class="d-none" id="locations-data" data-locations="{{ json_encode($locations) }}"></div>
<div class="row">
    <form class="col-12 form-row" id="app">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group col-12 mb-1 p-0">
                        <input id="search" class="form-control" type="text" placeholder="Nhập yêu cầu tìm kiếm..."
                            name="address" value="{{request('address') ?? ''}}">
                    </div>
                    <a data-toggle="collapse" data-target="#advance-search">
                        <i class="fas fa-sliders-h mr-1"></i>
                        Tìm kiếm nâng cao
                    </a>
                </div>
                <div class="card-footer collapse" id="advance-search">
                    <div class="row mb-0" id="app">
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="province_id">Tỉnh/Thành phố</label>
                            <select class="form-control select2" name="province_id" id="province_id"
                                v-model="selectedProvinceId" @change="changeProvince">
                                <option value="0"></option>
                                <option v-for="province in provinces" :value="province.province_id">
                                    @{{ province.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="district_id">Quận/Huyện</label>
                            <select class="form-control" name="district_id" id="district_id"
                                v-model="selectedDistrictId" @change="changeDistrict">
                                <option value="0"></option>
                                <option v-for="district in districts" :value="district.district_id">
                                    @{{ district.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label for="ward_id">Xã/Phường</label>
                            <select class="form-control" name="ward_id" id="ward_id" v-model="selectedWardId">
                                <option value="0"></option>
                                <option v-for="ward in wards" :value="ward.ward_id">
                                    @{{ ward.name }}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-start mb-3">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Giá từ</label>
                            <input type="text" class="form-control money" placeholder="1,000,000" name="price_from">
                        </div>
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Đến</label>
                            <input type="text" class="form-control money" placeholder="5,000,000" name="price_to">
                        </div>
                    </div>
                    <div class="row third justify-content-start mb-3">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Diện tích từ</label>
                            <input type="text" class="form-control money" placeholder="15m2" name="acreage_from">
                        </div>
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <label for="ward_id">Đến</label>
                            <input type="text" class="form-control money" placeholder="30m2" name="acreage_to">
                        </div>
                    </div>
                    <div class="row fourth justify-content-end">
                        <div class="form-group col-md-4 col-sm-12 mb-3">
                            <button class="btn btn-success ml-3 float-right">Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @foreach ($apartments as $apartment)
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col" style="max-width: 250px;max-height:150px;">
                            <img src="{{$apartment->thumbnail}}" class="card-img h-100 w-100">
                        </div>
                        <div class="col col-grow-1">
                            <div class="card-body">
                                <a href="{{ route('app.apartments.show', $apartment->id) }}" class="card-title">{{ $apartment->location['desc'] }}</a>
                                <p class="card-text">Còn {{ $apartment->publicRooms->count() }} phòng</p>
                                <p class="card-text">
                                    <small class="text-muted">
                                        Giá từ
                                        {{number_format($apartment->publicRooms->min('price'))}} VND
                                    </small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    var provinces = $('#locations-data').data('locations');
    var vue = new Vue({
        el: '#app',
        data: {
            provinces: provinces,
            districts: provinces[0].districts,
            wards: provinces[0].districts[0].wards,
            selectedProvinceId: {},
            selectedDistrictId: {},
            selectedWardId: {},
            selectedProvince: {},
            selectedDistrict: {},
        },
        methods: {
            changeProvince: function () {
                this.wards = [];
                this.selectedProvince = provinces.find((province) => {
                    return province.province_id == this.selectedProvinceId
                });
                this.districts = this.selectedProvince.districts
            },
            changeDistrict: function () {
                this.selectedDistrict = this.selectedProvince.districts.find((district) => {
                    return district.district_id == this.selectedDistrictId
                });
                this.wards = this.selectedDistrict.wards
            }
        },
    });

    $(document).ready(function () {
        $('.money').mask('000,000,000,000,000', { reverse: true });
    })

    $('form').submit(function(){
        $('.money').unmask();
    })
</script>
@endpush
