@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                Đăng tin cho thuê phòng
            </div>
            <div class="card-body">
                <form action="{{route('manager.rooms.post.store', $room->id)}}" class="form-row" method="POST">
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Tiêu đề bài đăng</label>
                        <input required type="text" class="form-control" id="title" name="title"
                            value="{{old('title', $room->title)}}">
                    </div>
                    <div class="col-12 row">

                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
