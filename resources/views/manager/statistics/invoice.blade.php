@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <!-- Main content -->
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-globe"></i>
                        @foreach (config('const.invoice.types') as $type)
                        @if ($invoice->costable_type == $type['code'])
                        <span>Hoá đơn thanh toán:</span>
                        <span>{{ $type['text'] }}</span>
                        @endif
                        @endforeach
                        <small class="float-right">
                            @empty($invoice->paid_at)
                            <i class="badge badge-warning">
                                {{ config('const.invoice.statuses.not_paid.text') }}
                            </i>
                            @else
                            <i class="badge badge-success">
                                <span>{{ config('const.invoice.statuses.paid.text') }}</span>
                            </i>
                            @endempty
                        </small>
                    </h4>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-12 col-md invoice-col">
                    <address>
                        <span>Phòng:</span>
                        <span>{{ $invoice->room->room_number }}</span>
                        <br>
                        <span>Địa chỉ:</span>
                        <i>{{ $invoice->room->apartment->location['desc'] }}</i>
                        <br>
                        <span>Người thanh toán: </span>
                        <strong>Nguyễn Tiến Dũng</strong>
                        <br>
                        Phone: (804) 123-5432<br>
                        Email: info@almasaeedstudio.com
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-12 col-md invoice-col mb-3">
                    Hoá đơn số <b>#{{$invoice->id}}</b><br>
                    Tình trạng:
                    @empty($invoice->paid_at)
                    <b>{{ config('const.invoice.statuses.not_paid.text') }}</b>
                    @else
                    <b>{{ config('const.invoice.statuses.paid.text') }}</b>
                    <br>
                    Ngày thanh toán:
                    <b>{{ Carbon\Carbon::parse($invoice->paid_at)->format('d/m/Y') }}</b>
                    @endempty

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Thanh toán cho tháng</th>
                                <th>Số tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($invoice->months as $item)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ Carbon\Carbon::parse($item['month'])->format('m/Y') }}</td>
                                <td>{{ number_format($item['amount']) }} VND</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{ number_format(array_sum(array_column($invoice->months,'amount'))) }} VND</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-12">
                    @empty($invoice->paid_at)
                    <form action="{{route('manager.statistic.invoice.confirm.paid', $invoice->id)}}"
                        class="d-inline-block" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-success float-left" style="margin-right: 5px;">
                            <i class="fas fa-check-double"></i> Xác nhận thanh toán
                        </button>
                    </form>
                    @endempty

                    <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;"
                        onclick="window.print();">
                        <i class="fas fa-print"></i> In hoá đơn
                    </button>
                </div>
            </div>
        </div>
        <!-- /.invoice -->
    </div><!-- /.col -->
</div><!-- /.row -->
@endsection
