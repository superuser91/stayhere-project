@extends('manager.layouts.app')

@section('pageHeader')
Danh sách hoá đơn
@endsection

@push('head')
<link rel="stylesheet" href="/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Danh sách hoá đơn</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form class="form-row" method="GET" id="submit-form">
                            <div class="form-group col-12 col-sm-4 col-md-3">
                                <label for="costable_type">Loại hoá đơn</label>
                                <select type="text" class="form-control" name="costable_type" id="costable_type">
                                    @foreach (config('const.invoice.types') as $type)
                                    <option value="{{$type['code']}}" @if(request('costable_type')==$type['code'])
                                        selected @endif>
                                        {{$type['text']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-sm-4 col-md-3">
                                <label for="payment_status">Tình trạng thanh toán</label>
                                <select type="text" class="form-control" name="payment_status" id="payment_status">
                                    @foreach (config('const.invoice.statuses') as $status)
                                    <option value="{{$status['code']}}" @if(request('payment_status')==$status['code'])
                                        selected @endif>
                                        {{$status['text']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-sm-4 col-md-3 align-self-end">
                                <label>&nbsp</label>
                                <button class="btn btn-primary">
                                    <i class="fas fa-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 table-responsive">
                        <table class="table table-hover table-striped table-bordered text-truncate">
                            <colgroup>
                                <col style="width: 50px;">
                                <col style="width: 150px;">
                                <col style="width: 150px;">
                                <col style="width: 200px;">
                                <col style="width: 200px;">
                                <col style="width: auto;">
                                <col style="width: 100px;">
                            </colgroup>
                            <thead class="text-center">
                                <tr>
                                    <th>STT</th>
                                    <th>Loại hoá đơn</th>
                                    <th>Số tiền</th>
                                    <th>Tình trạng</th>
                                    <th>Phòng</th>
                                    <th>Thanh toán cho tháng</th>
                                    <th>Chức năng</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @if ($invoices->count() == 0)
                                <tr>
                                    <td colspan="7" class="text-center">Danh sách trống</td>
                                </tr>
                                @endif
                                @foreach ($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoices->firstItem() + $loop->index }}</td>
                                    <td class="text-left">
                                        @foreach (config('const.invoice.types') as $type)
                                        @if ($invoice->costable_type == $type['code'])
                                        {{ $type['text'] }}
                                        @endif
                                        @endforeach
                                    </td>
                                    <td class="text-right">{{ number_format($invoice->amount) }}</td>
                                    <td>
                                        @empty($invoice->paid_at)
                                        {{ config('const.invoice.statuses.not_paid.text') }}
                                        @else
                                        {{ config('const.invoice.statuses.paid.text') }}
                                        @endempty
                                    </td>
                                    <td>{{ $invoice->room->room_number }}</td>
                                    <td class="text-center">
                                        {{ implode(', ', array_map(function($i){ return Carbon\Carbon::parse($i['month'])->format('m/Y');},$invoice->months)) }}
                                    </td>
                                    <td class="text-left">
                                        <a href="{{ route('manager.statistic.invoice.detail', $invoice->id) }}"
                                            class="btn btn-info">
                                            <i class="fas fa-info-circle mr-1"></i>
                                            Chi tiết</a>
                                        @empty($invoice->paid_at)
                                        <a data-action="{{route('manager.statistic.invoice.delete', $invoice->id)}}"
                                            class="btn btn-danger btn-delete-invoice">
                                            <i class="fas fa-trash"></i>
                                            Xoá</a>
                                        @endempty
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="links">
                        {{ $invoices->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" id="form-delete-invoice">
    @csrf
    @method('DELETE')
</form>
@endsection

@push('scripts')
<script>
    $(document).on('click', '.btn-delete-invoice', function (e) {
        e.preventDefault();
        let action = $(this).data('action');
        Swal.fire({
            title: 'Bạn có chắc chắn muốn xoá?',
            text: "Sau khi xoá sẽ không còn được hiển thị!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xoá!'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#form-delete-invoice').attr('action', action);
                $('#form-delete-invoice').submit();
            }
        })
    });
</script>
@endpush
