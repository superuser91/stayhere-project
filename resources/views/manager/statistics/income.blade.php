@extends('manager.layouts.app')

@section('pageHeader')
Thống kê thu nhập
@endsection

@section('content')
<div class="d-none" id="houseCost" data-house-cost="{{ json_encode($houseCost) }}"></div>
<div class="d-none" id="livingCost" data-living-cost="{{ json_encode($livingCost) }}"></div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Hiện tại: {{ date('d/m/Y') }}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row ">
                    <div class="col-12">
                        <form method="GET" id="change-year">
                            <div class="form-group col-12 col-sm-4 col-md-3 text-right">
                                <select required type="text" class="form-control" name="year"
                                    onchange="$('#change-year').submit();">
                                    @foreach ($years as $year)
                                    <option value="{{$year}}" @if((empty(request('year')) && date('Y')==$year) ||
                                        request('year')==$year) selected @endif>Năm {{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="clear-fix"></div>
                        </form>
                    </div>
                    <div class="chart col-12">
                        <div class="chartjs-size-monitor">
                            <div class="chartjs-size-monitor-expand">
                                <div class=""></div>
                            </div>
                            <div class="chartjs-size-monitor-shrink">
                                <div class=""></div>
                            </div>
                        </div>
                        <canvas id="stackedBarChart"
                            style="min-height: 250px; height: 450px; max-height: 550px; max-width: 100%; display: block; width: 471px;"
                            width="471" height="250" class="chartjs-render-monitor"></canvas>
                        <h5 class="m-3">Tổng thu nhập trong năm:
                            {{ number_format(array_sum($houseCost) + array_sum($livingCost))}} VND</h5>

                    </div>
                </div>


            </div>
            <!-- /.card-body -->
        </div>

    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        //---------------------
        //- STACKED BAR CHART -
        //---------------------
        var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
        var stackedBarChartData = {
            labels: ['Tháng 1',
                'Tháng 2',
                'Tháng 3',
                'Tháng 4',
                'Tháng 5',
                'Tháng 6',
                'Tháng 7',
                'Tháng 8',
                'Tháng 9',
                'Tháng 10',
                'Tháng 11',
                'Tháng 12'],
            datasets: [
                {
                    label: 'Tiền phòng',
                    backgroundColor: 'rgba(60,141,188,0.9)',
                    borderColor: 'rgba(60,141,188,0.8)',
                    pointRadius: false,
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: $('#houseCost').data('houseCost')
                },
                {
                    label: 'Tiền sinh hoạt phí, dịch vụ',
                    backgroundColor: 'rgba(210, 214, 222, 1)',
                    borderColor: 'rgba(210, 214, 222, 1)',
                    pointRadius: false,
                    pointColor: 'rgba(210, 214, 222, 1)',
                    pointStrokeColor: '#c1c7d1',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: $('#livingCost').data('livingCost')
                },
            ]
        }

        var stackedBarChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        return tooltipItem.yLabel.toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, '$&,')
                        .replace(/(\w.?)(\.00)/,'$1')
                    }
                }
            }
        }

        new Chart(stackedBarChartCanvas, {
            type: 'bar',
            data: stackedBarChartData,
            options: stackedBarChartOptions
        })
    });
</script>
@endpush
