@extends('manager.layouts.app')

@section('content')
<div class="d-none" id="locations-data" data-locations="{{ json_encode($locations) }}"></div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Danh sách người ở trọ</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form class="form-row" method="GET" id="submit-form">
                            <div class="form-group col-12 col-sm-6">
                                <label for="apartment_id">Địa chỉ</label>
                                <select type="text" class="form-control" name="apartment_id" id="apartment_id">
                                    <option value="" selected>Tất cả</option>
                                    @foreach ($apartments as $apartment)
                                    <option value="{{$apartment->id}}" @if(request('apartment_id')==$apartment->id)
                                        selected @endif>
                                        {{$apartment->location['desc']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-sm-4 col-md-3 align-self-end">
                                <label>&nbsp</label>
                                <button class="btn btn-primary">
                                    <i class="fas fa-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-truncate">
                            <colgroup>
                                <col style="max-width: 50px;">
                                <col style="min-width: 200px;">
                                <col style="min-width: 50px;">
                                <col style="min-width: 75px;">
                                <col style="min-width: 175px;">
                                <col style="width: 175px;">
                                <col style="width: 275px;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ tên</th>
                                    <th>Giới tính</th>
                                    <th>Ngày sinh</th>
                                    <th>Quê quán</th>
                                    <th>Phòng</th>
                                    <th>Nhà trọ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($tenants->count() == 0)
                                <tr>
                                    <td colspan="7" class="text-center">
                                        Danh sách người thuê nhà ở địa chỉ này đang trống
                                    </td>
                                </tr>
                                @endif
                                @foreach ($tenants as $tenant)
                                <tr>
                                    <td>{{ $tenants->firstItem() + $loop->index }}</td>
                                    <td>
                                        {{ $tenant->full_name }}
                                    </td>
                                    <td>
                                        @foreach (config('const.gender.types') as $type)
                                        @if ($tenant->gender == $type['code'])
                                        {{ $type['text'] }}
                                        @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $tenant->birth_date ?? '' }}
                                    </td>
                                    <td>
                                        @php
                                        $province = $locations->firstWhere('province_id', $tenant->province_id);
                                        @endphp
                                        {{ $province['name'] ?? '' }}
                                    </td>
                                    <td>
                                        {{ $tenant->room->room_number }}
                                    </td>
                                    <td>
                                        {{ $tenant->room->apartment->location['desc'] }}
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
