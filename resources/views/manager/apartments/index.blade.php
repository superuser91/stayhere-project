@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Danh sách nhà trọ của bạn</h4>
            </div>
            <div class="card-body">
                <a href="{{ route('manager.apartments.create') }}" class="btn btn-success mb-3 float-right">
                    <i class="fas fa-plus mr-1"></i>
                    Thêm nhà
                </a>
                <div class="table-responsive text-nowrap">
                    <table class="table table-bordered table-striped table-hover">
                        <colgroup>
                            <col style="min-width: 50px;">
                            <col style="width: auto;">
                            <col style="width: 200px;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Vị trí</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($apartments->count() == 0)
                            <tr>
                                <td colspan="4" class="text-center">Danh sách trống</td>
                            </tr>
                            @endif
                            @foreach ($apartments as $apartment)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{ $apartment->location['desc'] }}
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Chức năng">
                                        <a class="btn btn-info"
                                            href="{{ route('manager.apartments.show', $apartment->id) }}">Chi
                                            tiết</a>
                                        <a class="btn btn-warning"
                                            href="{{ route('manager.apartments.edit', $apartment->id) }}">Sửa</a>
                                        <a class="btn btn-danger btn-delete-apartment"
                                            href="{{ route('manager.apartments.show', $apartment->id) }}"
                                            data-action="{{ route('manager.apartments.show', $apartment->id) }}">Xoá</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<form method="POST" id="form-delete-apartment" action="#">
    @csrf
    @method('DELETE')
</form>
@endsection

@push('scripts')
<script>
    $(document).on('click', '.btn-delete-apartment', function (e) {
        e.preventDefault();
        let action = $(this).data('action');
        Swal.fire({
            title: 'Bạn có chắc chắn muốn xoá?',
            text: "Sau khi xoá sẽ không còn được hiển thị!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xoá!'
        }).then((result) => {
            if (result.isConfirmed) {
                $('#form-delete-apartment').attr('action', action);
                console.log($('#form-delete-apartment').attr('action'));
                $('#form-delete-apartment').submit();
            }
        })
    });
</script>
@endpush
