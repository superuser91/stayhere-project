@extends('manager.layouts.app')

@push('head')
<style>
    .uploaded-image {
        height: 100px;
    }

    .remove-image-marker {
        color: red;
        right: 8px;
        top: -8px;
        cursor: pointer;
    }
</style>
<link href="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.css" rel="stylesheet">
<script src="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.js"></script>
<script src="https://releases.transloadit.com/uppy/locales/v1.19.0/vi_VN.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"
    integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ=="
    crossorigin="anonymous"></script>

@endpush

@section('content')
<div class="d-none" id="locations-data" data-locations="{{ json_encode($locations) }}"></div>
<div class="d-none" id="selectedProvinceId" data-province-id="{{ $apartment->province_id }}"></div>
<div class="d-none" id="selectedDistrictId" data-district-id="{{ $apartment->district_id }}"></div>
<div class="d-none" id="selectedWardId" data-ward-id="{{ $apartment->ward_id }}"></div>
<div class="d-none" id="latData" data-lat="{{ $apartment->location['lat'] }}"></div>
<div class="d-none" id="lonData" data-lon="{{ $apartment->location['lon'] }}"></div>
<div class="row" id="app">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Thông tin nhà trọ</h4>
            </div>
            <div class="card-body">
                <form class="row" action="{{ route('manager.apartments.store') }}" id="form-create-apartment"
                    method="POST">
                    @csrf
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="province_id">Tỉnh/Thành phố</label>
                        <select disabled class="form-control select2" name="province_id" id="province_id"
                            v-model="selectedProvinceId">
                            <option v-for="province in provinces" :value="province.province_id">
                                @{{ province.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="district_id">Quận/Huyện</label>
                        <select disabled class="form-control" name="district_id" id="district_id"
                            v-model="selectedDistrictId">
                            <option v-for="district in districts" :value="district.district_id">
                                @{{ district.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="ward_id">Xã/Phường</label>
                        <select disabled class="form-control" name="ward_id" id="ward_id" v-model="selectedWardId">
                            <option v-for="ward in wards" :value="ward.ward_id">
                                @{{ ward.name }}
                            </option>
                        </select>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="map_address">Vị trí trên bản đồ</label>
                        <input disabled type="text" class="form-control mb-3" id="map_address"
                            value="{{ $apartment->location['desc'] }}">
                        <div id="map-canvas" style="height: 500px"></div>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Giá điện (VND/Kwh)</label>
                        <input disabled type="number" class="form-control" id="electric_price" name="description[electric_price]" min="0" value="{{$apartment->description['electric_price'] ?? ''}}">
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Giá nước (VND/m3)</label>
                        <input disabled type="number" class="form-control" id="water_price" name="description[water_price]" min="0" value="{{$apartment->description['water_price']  ?? ''}}">
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Internet (phòng)</label>
                        <input disabled type="number" class="form-control" id="internet_price" name="description[internet_price]" min="0" value="{{$apartment->description['internet_price'] ?? ''}}">
                    </div>
                </form>
                <div class="col-12 mb-3">
                    <label for="map_address">Ảnh minh hoạ</label>
                    <div class="responsive overflow-auto">
                        <div class="uploaded-files mb-3" style="height: 100px;min-width: max-content;">
                            @foreach ($apartment->images ?? [] as $image)
                            <div class="image-wrapper h-100 position-relative d-inline-block">
                                <img class="uploaded-image mr-3 mb-3" src="{{$image}}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-success float-right" href="{{ url()->previous() }}">Quay
                    lại</a>
            </div>
        </div>
    </div>
</div>
<img src="google-map-api-not-loading-if-every-thing-well-loaded-therefore-i-must-add-an-error-img-at-here" alt="">
@endsection
@push('scripts')
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key={{ config('google.maps.api_key') }}&libraries=places&language=vi&region=VN&{{time()}}">
    </script>
<script type="text/javascript">
    var provinces = $('#locations-data').data('locations');
    var selectedProvinceId = $('#selectedProvinceId').data('province-id');
    var selectedDistrictId = $('#selectedDistrictId').data('district-id');
    var selectedWardId = $('#selectedWardId').data('ward-id');
    var lat = $('#latData').data('lat');
    var lon = $('#lonData').data('lon');
    var selectedProvince = provinces.find((province) => {
        return province.province_id == selectedProvinceId
    });
    var districts = selectedProvince.districts;
    var selectedDistrict = selectedProvince.districts.find((district) => {
        return district.district_id == selectedDistrictId
    });
    var wards = selectedDistrict.wards;
</script>
<script defer>
    $(document).ready(function () {
        google.maps.event.addDomListener(window, 'load', initialize);
        var vue = new Vue({
            el: '#app',
            data: {
                provinces: provinces,
                districts: districts,
                wards: wards,
                selectedProvinceId: selectedProvinceId,
                selectedDistrictId: selectedDistrictId,
                selectedWardId: selectedWardId,
            },
        });
    });

    function initialize() {
        var position = new google.maps.LatLng(lat, lon);
        var mapOptions = {
            center: position,
            zoom: 15
        };
        const map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        var marker = new google.maps.Marker({
            position: position,
            map: map,
        });
    }

</script>
@endpush
