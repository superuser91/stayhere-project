@extends('manager.layouts.app')

@push('head')
<style>
    .uploaded-image {
        height: 100px;
    }

    .remove-image-marker {
        color: red;
        right: 8px;
        top: -8px;
        cursor: pointer;
    }
</style>
<link href="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.css" rel="stylesheet">
<script src="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.js"></script>
<script src="https://releases.transloadit.com/uppy/locales/v1.19.0/vi_VN.min.js"></script>

@endpush

@section('content')
<div class="d-none" id="locations-data" data-locations="{{ json_encode($locations) }}"></div>
<div class="row" id="app">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Thông tin nhà trọ</h4>
            </div>
            <div class="card-body">
                <form class="row" action="{{ route('manager.apartments.store') }}" id="form-create-apartment"
                    method="POST">
                    @csrf
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="province_id">Tỉnh/Thành phố</label>
                        <select required class="form-control select2" name="province_id" id="province_id"
                            v-model="selectedProvinceId" @change="changeProvince">
                            <option v-for="province in provinces" :value="province.province_id">
                                @{{ province.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="district_id">Quận/Huyện</label>
                        <select required class="form-control" name="district_id" id="district_id"
                            v-model="selectedDistrictId" @change="changeDistrict">
                            <option v-for="district in districts" :value="district.district_id">
                                @{{ district.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <label for="ward_id">Xã/Phường</label>
                        <select required class="form-control" name="ward_id" id="ward_id" v-model="selectedWardId">
                            <option v-for="ward in wards" :value="ward.ward_id">
                                @{{ ward.name }}
                            </option>
                        </select>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="map_address">Vị trí trên bản đồ</label>
                        <input required type="text" class="form-control mb-3" id="map_address">
                        <div id="map-canvas" style="height: 500px"></div>
                    </div>
                    <input type="hidden" name="location[lat]">
                    <input type="hidden" name="location[lon]">
                    <input type="hidden" name="location[desc]">
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Giá điện (VND/Kwh)</label>
                        <input required type="number" class="form-control" id="electric_price" name="description[electric_price]" min="0" value="{{old('description.electric_price')}}">
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Giá nước (VND/m3)</label>
                        <input required type="number" class="form-control" id="water_price" name="description[water_price]" min="0" value="{{old('description.water_price')}}">
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Internet (phòng)</label>
                        <input required type="number" class="form-control" id="internet_price" name="description[internet_price]" min="0" value="{{old('description.internet_price')}}">
                    </div>
                </form>
                <div class="col-12 mb-3">
                    <label for="map_address">
                        Thêm ảnh minh hoạ
                        <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top"
                            title="Kích thước ảnh không quá 4 Mb và phải là định dạng hình ảnh"></i>
                    </label>
                    <form action="{{ route('manager.apartments.images.upload') }}" class="UppyForm" method="POST">
                        <input type="file" id="images" name="images[]" multiple>
                    </form>
                    <div class="UppyProgressBar mb-3"></div>
                    <div class="responsive overflow-auto">
                        <div class="uploaded-files mb-3 mt-3" style="height: 100px;min-width: max-content;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary float-right" id="submitForm" form="form-create-apartment">Lưu
                    lại</button>
            </div>
        </div>
    </div>
</div>
<img src="google-map-api-not-loading-if-every-thing-well-loaded-therefore-i-must-add-an-error-img-at-here" alt="">
@endsection
@push('scripts')
<script type="text/javascript"
    src="https://maps.googleapis.com/maps/api/js?key={{ config('google.maps.api_key') }}&libraries=places&language=vi&region=VN&{{time()}}">
    </script>
<script type="text/javascript">
    var provinces = $('#locations-data').data('locations');
    var vue = new Vue({
        el: '#app',
        data: {
            provinces: provinces,
            districts: provinces[0].districts,
            wards: provinces[0].districts[0].wards,
            selectedProvinceId: {},
            selectedDistrictId: {},
            selectedWardId: {},
            selectedProvince: {},
            selectedDistrict: {},
        },
        methods: {
            changeProvince: function () {
                this.wards = [];
                this.selectedProvince = provinces.find((province) => {
                    return province.province_id == this.selectedProvinceId
                });
                this.districts = this.selectedProvince.districts
            },
            changeDistrict: function () {
                this.selectedDistrict = this.selectedProvince.districts.find((district) => {
                    return district.district_id == this.selectedDistrictId
                });
                this.wards = this.selectedDistrict.wards
            }
        },
    });
</script>
<script async>
    var position;
    $(document).ready(function () {
        google.maps.event.addDomListener(window, 'load', initialize);
    });

    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(20.9760364, 105.7897446),
            zoom: 15
        };
        const map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        const input = document.getElementById("map_address");
        const options = {
            // componentRestrictions: { country: "vn" },
            // fields: ["formatted_address", "geometry", "name"],
            origin: map.getCenter(),
            strictBounds: false,
            // types: ["establishment"],
        };
        const autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.bindTo("bounds", map);
        const marker = new google.maps.Marker({
            map,
            anchorPoint: new google.maps.Point(0, -29),
        });
        autocomplete.addListener("place_changed", () => {
            marker.setVisible(false);
            const place = autocomplete.getPlace();

            if (!place.geometry || !place.geometry.location) {
                window.alert("Không tìm thấy địa chỉ: '" + place.name + "'");
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            position = place.geometry.location;
        });
    }

</script>
<script>
    var uppy = Uppy.Core({
        locale: Uppy.locales.vi_VN,
        debug: true,
        autoProceed: true,
        restrictions: {
            // maxFileSize: 1000000,
            allowedFileTypes: ['image/*', 'video/*']
        }
    });

    uppy.use(Uppy.FileInput, {
        target: '.UppyForm',
        replaceTargetContent: true
    })

    uppy.use(Uppy.ProgressBar, {
        target: '.UppyProgressBar',
        hideAfterFinish: true
    })

    uppy.use(Uppy.XHRUpload, {
        endpoint: "{{ route('manager.apartments.images.upload') }}",
        formData: true,
        headers: {
            'X-CSRF-Token': " {{ csrf_token() }} "
        },
        fieldName: 'images[]'
    })

    uppy.on('upload-success', (file, res) => {
        let images = res.body.data;
        images.forEach(function (image) {
            $(`#form-create-apartment`).append(`<input type="hidden" name="images[]" value="${image}" data-image-path="${image}">`);
            $('.uploaded-files').append(templateImage(image));
        })
    })

    function templateImage(src) {
        return `<div class="image-wrapper h-100 position-relative d-inline-block">
                    <span class='remove-image-marker position-absolute' data-image-path="${src}">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <img class="uploaded-image mr-3 mb-3"
                        src="${src}">
                </div>`;
    }

</script>
<script>
    $('#form-create-apartment').submit(function (e) {
        let lat = position.lat();
        let lng = position.lng();
        let address = $('#map_address').val();
        if (!lat) return false;
        if (!lng) return false;
        if (!address) return false;
        $(`#form-create-apartment input[name="location[lat]"]`).val(lat);
        $(`#form-create-apartment input[name="location[lon]"]`).val(lng);
        $(`#form-create-apartment input[name="location[desc]"]`).val(address);
    });
    $(document).on('click', '.remove-image-marker', function (e) {
        let imagePath = $(this).data('image-path');
        $(this).parent().remove();
        $(`input[data-image-path="${imagePath}"]`).remove();
        axios.delete('/uploaded/images', {
            params: {
                path: imagePath
            }
        })
    });
</script>
@endpush
