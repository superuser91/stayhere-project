@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Quản lý thu tiền phòng</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form class="form-row" method="GET" id="submit-form">
                            <div class="form-group col-12 col-sm-6">
                                <label for="apartment_id">Địa chỉ</label>
                                <select type="text" class="form-control" name="apartment_id" id="apartment_id">
                                    <option value="" selected>Tất cả</option>
                                    @foreach ($apartments as $apartment)
                                    <option value="{{$apartment->id}}" @if(request('apartment_id')==$apartment->id)
                                        selected @endif>
                                        {{$apartment->location['desc']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12 col-sm-4 col-md-3 align-self-end">
                                <label>&nbsp</label>
                                <button class="btn btn-primary">
                                    <i class="fas fa-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-truncate">
                            <colgroup>
                                <col style="max-width: 50px;">
                                <col style="min-width: 200px;">
                                <col style="min-width: 50px;">
                                <col style="min-width: 75px;">
                                <col style="min-width: 175px;">
                                <col style="width: 175px;">
                                <col style="width: 275px;">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Phòng</th>
                                    <th>Địa chỉ</th>
                                    <th>Người đại diện thuê phòng</th>
                                    <th>SĐT</th>
                                    <th>Chức năng</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($apartments->every(function($apartment){return $apartment->leasedRooms->count()
                                ==0;}))
                                <tr>
                                    <td colspan="6" class="text-center">
                                        Danh sách trống
                                    </td>
                                </tr>
                                @endif
                                @foreach ($apartments as $apartment)
                                @foreach ($apartment->leasedRooms as $room)
                                <tr>
                                    <td>{{ $loop->parent->index + $loop->index + 1 }}</td>
                                    <td>
                                        {{ $room->room_number }}
                                    </td>
                                    <td>
                                        {{ $apartment->location['desc'] }}
                                    </td>
                                    <td>
                                        {{ $room->contract->user->name ?? ''}}
                                    </td>
                                    <td>
                                        {{ $room->contract->user->phone_number ?? ''}}
                                    </td>
                                    <td>
                                        <a href="{{ route('manager.management.house.invoices.create', $room->id) }}"
                                            class="btn btn-success">
                                            <i class="fas fa-file-invoice-dollar mr-2"></i>
                                            Tạo hoá đơn
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
