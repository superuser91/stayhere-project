@extends('manager.layouts.app')

@section('pageHeader')
Hợp đồng thuê nhà
@endsection

@section('content')
<form class="row" method="POST">
    @csrf
    <div class="col-12">
        <div class="card">
            <div class="card-header">Các bên liên quan</div>
            <div class="card-body">
                <h5>Đại diện bên cho thuê (Bên A)</h5>
                <div class="callout callout-info">
                    <p class="mb-1">
                        <label for="">Ông (Bà):</label>
                        {{ auth()->user()->profile->full_name ?? auth()->user()->name ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Sinh năm:</label>
                        {{ $request->room->apartment->manager->profile->birth_date ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">CMND/CCCD số:</label>
                        {{ $request->room->apartment->manager->profile->identity_number ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Hộ khẩu thường trú:</label>
                        {{ $request->room->apartment->manager->profile->permanent_residence ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Số điện thoại:</label>
                        {{ $request->room->apartment->manager->phone_number ?? '' }}
                    </p>
                </div>
                <h5>Đại diện bên thuê nhà (Bên B)</h5>
                <div class="callout callout-warning">
                    <p class="mb-1">
                        <label for="">Ông (Bà):</label>
                        {{ $request->user->profile->full_name ?? $request->user->name ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Sinh năm:</label>
                        {{ $request->user->profile->birth_date ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">CMND/CCCD số:</label>
                        {{ $request->user->profile->identity_number ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Hộ khẩu thường trú:</label>
                        {{ $request->user->profile->permanent_residence ?? '' }}
                    </p>
                    <p class="mb-1">
                        <label for="">Số điện thoại:</label>
                        {{ $request->user->phone_number ?? $request->user->phone_number ?? '' }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 1: Đối tượng hợp đồng</div>
            <div class="card-body">
                {{ $request->room->title }} , {{ $request->room->apartment->location['desc']}}
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 2: Thoả thuận về giá thuê nhà và phương thức thanh toán</div>
            <div class="card-body">
                <p>Giá phòng: {{number_format($request->room->price)}} VND/tháng</p>
                <p>Chu kỳ thanh toán: {{ $request->room->description['payment']['recurring'] ?? 1}} tháng</p>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 3: Thoả thuận về số tiền đặt cọc</div>
            <div class="card-body">
                Đặt cọc: {{ number_format($request->room->description['payment']['deposit'] ?? 0) }} VND
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 4: Thời hạn thuê và thời điểm giao nhận nhà ở</div>
            <div class="card-body row">
                <div class="form-group col-12 col-md-4">
                    <label for="contract_term">Thời hạn thuê nhà (tháng)</label>
                    <input required type="number" class="form-control" id="contract_term" name="contract_term">
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="valid_from">Ngày giao nhận phòng</label>
                    <input required type="text" class="form-control datemask" id="valid_from" name="valid_from">
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 5: Nghĩa vụ và quyền lợi của bên A</div>
            <div class="card-body">
                <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Điều 6: Nghĩa vụ và quyền lợi của bên B</div>
            <div class="card-body">
                <textarea name="" id="" cols="30" rows="5" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="col-12">
        <button class="btn btn-primary">
            <i class="fas fa-share-square mr-2"></i>
            Gửi hợp đồng cho người thuê nhà
        </button>
    </div>
</form>

@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $('.datemask').datepicker();
    });
</script>
@endpush
