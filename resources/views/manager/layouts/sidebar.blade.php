<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="/admin-lte/dist/img/AdminLTELogo.png" alt="Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">Bảng điều khiển</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ auth('manager')->user()->avatar ?? '/images/avatar.png' }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="javascript:void(0)" class="d-block">{{ auth('manager')->user()->name }}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar nav-child-indent flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                           with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="@if(request()->routeIs('manager.messages')) javascript:void(0) @else {{route('manager.messages')}} @endif"
                        class="nav-link @if(request()->routeIs('manager.messages')) active @endif">
                        <i class="nav-icon fas fa-comments"></i>
                        <p>
                            Tin nhắn
                            <span class="right badge badge-danger">{{ $unreadMessages->count() }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="@if(request()->routeIs('manager.requests.index')) javascript:void(0) @else {{route('manager.requests.index')}} @endif"
                        class="nav-link @if(request()->routeIs('manager.requests*')) active @endif">
                        <i class="nav-icon fas fa-bell"></i>
                        <p>
                            Yêu cầu thuê phòng
                            <span class="right badge badge-danger">{{ $roomRequestsCount }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="@if(request()->routeIs('manager.apartments.index')) javascript:void(0) @else {{route('manager.apartments.index')}} @endif"
                        class="nav-link @if(request()->routeIs('manager.apartments.index')) active @endif">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Danh sách nhà
                        </p>
                    </a>
                </li>
                <li class="nav-item menu-open">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Danh sách phòng trọ
                            <i class="right fas fa-angle-left nav-icon"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.rooms.index')) javascript:void(0) @else {{route('manager.rooms.index')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.rooms.index')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tất cả phòng</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.rooms.list.leased')) javascript:void(0) @else {{route('manager.rooms.list.leased')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.rooms.list.leased')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Phòng đã cho thuê</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.rooms.list.empty')) javascript:void(0) @else {{route('manager.rooms.list.empty')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.rooms.list.empty')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Phòng chưa cho thuê</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.rooms.list.posts')) javascript:void(0) @else {{route('manager.rooms.list.posts')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.rooms.list.posts')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Phòng đang rao</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item menu-open">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon fas fa-clipboard-list"></i>
                        <p>
                            Thống kê
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.statistic.income')) javascript:void(0) @else {{route('manager.statistic.income')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.statistic.income')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Thu nhập</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.statistic.tenant')) javascript:void(0) @else {{route('manager.statistic.tenant')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.statistic.tenant')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Người thuê phòng</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item menu-open">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Quản lý thu phí
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.management.living.cost')) javascript:void(0) @else {{route('manager.management.living.cost')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.management.living.cost')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Sinh hoạt phí</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.management.house.cost')) javascript:void(0) @else {{route('manager.management.house.cost')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.management.house.cost')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tiền phòng</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="@if(request()->routeIs('manager.statistic.invoice')) javascript:void(0) @else {{route('manager.statistic.invoice')}} @endif"
                                class="nav-link @if(request()->routeIs('manager.statistic.invoice*')) active @endif">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Hoá đơn</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item mt-5">
                    <a href="/logout" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Đăng xuất
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
