<!-- jQuery UI 1.11.4 -->
<script src="/admin-lte/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 4 -->
<script src="/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="/admin-lte/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="/admin-lte/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="/admin-lte/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="/admin-lte/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="/admin-lte/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/admin-lte/plugins/moment/moment.min.js"></script>
<script src="/admin-lte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="/admin-lte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/admin-lte/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<!-- Select2 -->
<script src="/admin-lte/plugins/select2/js/select2.full.min.js"></script>
<script src="/admin-lte/plugins/toastr/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
    integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<script>
    var conversationId = null;
    var userId = $('#user-id').data('user-id');
    var myAvatar = $('#my-avatar').data('my-avatar');
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            // "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            // "showDuration": "300",
            // "hideDuration": "1000",
            // "timeOut": "5000",
            // "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            // "showMethod": "fadeIn",
            // "hideMethod": "fadeOut"
        }
    })

    function notify(notification) {
        toastr["info"](`${notification.human_readable}`)

    }
</script>
<script>
    var autoScrollBottomWhenReceiveNewMessage = true;
    var notifyNewMessage = true;
    // Pusher.logToConsole = true;

    Echo.private('manager.' + userId)
        .notification((notification) => {
            if (notification.type == "App\\Notifications\\NewMessageNotification") {
                if (notifyNewMessage) {
                    notify(notification);
                }
            } else {
                notify(notification);
            }
        });
</script>
<script>
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js', {scope: '/'}).then(function(registration) {
          // Registration was successful
          console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
          // registration failed :(
          console.log('ServiceWorker registration failed: ', err);
        }).catch(function(err) {
          console.log(err)
        });
      });
    } else {
      console.log('service worker is not supported');
    }
</script>
