<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">{{$unreadNotificationsCount}}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <div class="dropdown-divider"></div>
                @foreach ($notifications->take(3) as $notification)
                <a href="#" class="dropdown-item text-truncate">
                    <i class="fas fa-bell"></i>
                    {{ $notification->data['human_readable'] ?? 'Thông báo hệ thống'}}
                    <span class="float-right text-muted text-sm">
                        {{$notification->created_at->diffForHumans(now())}}
                    </span>
                </a>
                <div class="dropdown-divider"></div>
                @endforeach
                @if ($notifications->count() == 0)
                <a href="#" class="dropdown-item dropdown-footer">Bạn không có thông báo nào</a>
                @else
                <a href="#" class="dropdown-item dropdown-footer">Xem toàn bộ thông báo</a>
                @endif
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
    </ul>
</nav>
<script>
    function collapseSidebar(){
        fetch('/collapse-sidebar').then(res => console.log(res)).catch(err => console.log(err));
    }
</script>
