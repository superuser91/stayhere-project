@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                Danh sách yêu cầu thuê phòng
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if ($requests->count() == 0)
                    <p class="text-center">Không có yêu cầu nào</p>
                    @endif
                    @foreach ($requests->groupBy('room_id') as $roomRequests)
                    <legend>
                        {{ $roomRequests->first()->room->room_number }}
                        <small class="text-muted ml-2">
                            <i>{{ $roomRequests->first()->room->apartment->location['desc'] }}</i>
                        </small>
                    </legend>

                    <table class="table table-striped table-bordered table-hover text-truncate">
                        <colgroup>
                            <col style="max-width: 50px;">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ tên người yêu cầu</th>
                                <th>SĐT liên hệ</th>
                                <th>Email</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roomRequests as $request)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>
                                    {{ $request->user->name }}
                                </td>
                                <td>
                                    {{ $request->user->phone_number }}
                                </td>
                                <td>
                                    {{ $request->user->email }}
                                </td>
                                <td>
                                    @if (empty($request->contract))
                                    <a href="{{ route('manager.requests.accept', $request->id) }}"
                                        class="btn btn-primary">Chấp nhận</a>
                                    <a href="{{ route('manager.requests.decline', $request->id) }}"
                                        class="btn btn-danger">Từ chối</a>
                                    @else
                                    @if ($request->contract->user_confirm_at)
                                    <a href="{{ route('manager.contract.deposit.confirm', $request->contract->id) }}"
                                        class="btn btn-primary">Xác nhận đóng tiền cọc</a>
                                    <a href="{{ route('manager.contract.cancel', $request->contract->id) }}"
                                        class="btn btn-danger">Huỷ hợp đồng</a>
                                    @else
                                    <i>
                                        Đang đợi chấp nhận hợp đồng thuê nhà
                                    </i>
                                    @endif
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
