@extends('manager.layouts.app')

@push('head')
<style>
    .uploaded-image {
        height: 100px;
    }

    .remove-image-marker {
        color: red;
        right: 8px;
        top: -8px;
        cursor: pointer;
    }
</style>
<link href="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.css" rel="stylesheet">
<script src="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.js"></script>
<script src="https://releases.transloadit.com/uppy/locales/v1.19.0/vi_VN.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous"></script>
@endpush

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-outline card-primary">
            <div class="card-header">
                Thông tin phòng trọ
                <span class="float-right d-inline-block">
                    @if ($room->status == config('const.room.statuses.leased'))
                    <i class="fas fa-circle text-success"></i>
                    <span>Đã cho thuê</span>
                    @elseif ($room->status == config('const.room.statuses.empty'))
                    <i class="fas fa-circle text-warning"></i>
                    <span>Còn trống</span>
                    @endif
                </span>
            </div>
            <div class="card-body row">
                <div class="form-group col-12 col-md-4">
                    <label for="title">Tên hiển thị</label>
                    <input disabled type="text" class="form-control" id="title" name="title" value="{{ $room->title }}">
                    <small class="text-muted">
                        Tiêu đề mặc định khi tạo tin cho thuê phòng
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="room_number">Số phòng</label>
                    <input disabled type="text" class="form-control" id="room_number" name="room_number"
                        value="{{ $room->room_number }}">
                    <small class="text-muted">
                        Số phòng, vị trí trong nhà
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="acreage">Diện tích</label>
                    <input disabled type="number" class="form-control" id="acreage" name="acreage"
                        value="{{ $room->acreage }}">
                    <small class="text-muted">
                        Đơn vị: mét vuông
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="people_limit">Số người ở tối đa</label>
                    <input disabled type="number" class="form-control" id="people_limit" name="description[limit]"
                        placeholder="Không giới hạn" value="{{ $room->description['limit'] ?? '' }}">
                    <small class="text-muted">
                        Để trống nếu không giới hạn số lượng người ở trong phòng
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="room-type">Loại phòng</label>
                    <select disabled name="description[type]" class="form-control" id="room-type">
                        @foreach (config('const.room.types') as $id => $type)
                        <option value="{{$id}}" @if($room->description['type']==$id) selected @endif>{{$type}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="recurring">Chu kỳ thanh toán tiền phòng</label>
                    <input disabled type="number" class="form-control" id="recurring"
                        name="description[payment][recurring]" value="{{$room->description['payment']['recurring']}}">
                    <small class="text-muted">
                        Số tháng tiền phòng phải trả trong 1 lần thanh toán
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="price">Giá phòng</label>
                    <input disabled type="text" class="form-control money" id="price" name="price"
                        placeholder="2,000,000 VND" value="{{ $room->price ?? '' }}">
                    <small class="text-muted">
                        Số tiền cần phải thanh toán tính riêng từng tháng
                    </small>
                </div>
                <div class="form-group col-12 col-md-4">
                    <label for="deposit">Đặt cọc</label>
                    <input disabled type="text" class="form-control money" id="deposit"
                        name="description[payment][deposit]" placeholder="2,000,000 VND"
                        value="{{ $room->description['payment']['deposit'] ?? '' }}">
                    <small class="text-muted">
                        Số tiền đặt cọc để thuê phòng
                    </small>
                </div>
                <div class="form-group col-12">
                    <label for="ultilities">Nội thất, tiện ích</label>
                    @foreach (config('const.room.ultilities') as $id => $ultility)
                    <div class="custom-control custom-checkbox mb-2">
                        <input disabled type="checkbox" class="custom-control-input" name="description[ultilities][]"
                            id="ultilities-{{$id}}" value="{{$id}}" @if(in_array($id, $room->description['ultilities']))
                        checked
                        @endif>
                        <label class="custom-control-label" for="ultilities-{{$id}}">{{$ultility}}</label>
                    </div>
                    @endforeach
                </div>
                <div class="col-12 mb-3">
                    <label for="map_address">Ảnh minh hoạ</label>
                    <div class="responsive overflow-auto">
                        <div class="uploaded-files mb-3" style="height: 100px;min-width: max-content;">
                            @foreach ($room->images as $image)
                            <div class="image-wrapper h-100 position-relative d-inline-block">
                                <img class="uploaded-image mr-3 mb-3" src="{{$image}}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary float-right mx-2" href="{{ url()->previous() }}">Quay lại</a>
                <a class="btn btn-warning float-right" href="{{ route('manager.rooms.edit', $room->id) }}">Chỉnh sửa</a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
            $('.money').mask('000,000,000,000,000', { reverse: true });
        })
</script>
@endpush
