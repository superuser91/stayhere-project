@extends('manager.layouts.app')

@push('head')
<style>
    .uploaded-image {
        height: 100px;
    }

    .remove-image-marker {
        color: red;
        right: 8px;
        top: -8px;
        cursor: pointer;
    }
</style>
<link href="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.css" rel="stylesheet">
<script src="https://releases.transloadit.com/uppy/v1.28.0/uppy.min.js"></script>
<script src="https://releases.transloadit.com/uppy/locales/v1.19.0/vi_VN.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous"></script>
@endpush

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-outline card-primary">
            <div class="card-header">
                Chỉnh sửa thông tin phòng trọ
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="form-row" action="{{route('manager.rooms.update', $room->id)}}" method="POST"
                    id="form-update-room">
                    @csrf
                    @method('PUT')
                    <div class="form-group col-12 col-md-4">
                        <label for="title">Tên hiển thị</label>
                        <input required type="text" class="form-control" id="title" name="title"
                            value="{{ old('title', $room->title) }}">
                        <small class="text-muted">
                            Tiêu đề mặc định khi tạo tin cho thuê phòng
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="room_number">Số phòng</label>
                        <input required type="text" class="form-control" id="room_number" name="room_number"
                            value="{{ old('room_number', $room->room_number) }}">
                        <small class="text-muted">
                            Số phòng, vị trí trong nhà
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="acreage">Diện tích</label>
                        <input required type="number" class="form-control" id="acreage" name="acreage"
                            value="{{ old('acreage', $room->acreage) }}">
                        <small class="text-muted">
                            Đơn vị: mét vuông
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="people_limit">Số người ở tối đa</label>
                        <input type="number" class="form-control" id="people_limit" name="description[limit]"
                            placeholder="Không giới hạn"
                            value="{{ old('description.limit', $room->description['limit']  ?? '')}}">
                        <small class="text-muted">
                            Để trống nếu không giới hạn số lượng người ở trong phòng
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="room-type">Loại phòng</label>
                        <select required name="description[type]" class="form-control" id="room-type">
                            @foreach (config('const.room.types') as $id => $type)
                            <option value="{{$id}}" @if(old('description.type', $room->description['type'])==$id)
                                selected @endif>{{$type}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="recurring">Chu kỳ thanh toán tiền phòng</label>
                        <input required type="number" class="form-control" id="recurring"
                            name="description[payment][recurring]"
                            value="{{$room->description['payment']['recurring']}}">
                        <small class="text-muted">
                            Số tháng tiền phòng phải trả trong 1 lần thanh toán
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="price">Giá phòng</label>
                        <input required type="text" class="form-control money" id="price" name="price"
                            placeholder="2,000,000 VND" value="{{ old('price', $room->price) }}">
                        <small class="text-muted">
                            Số tiền cần phải thanh toán tính riêng từng tháng
                        </small>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="deposit">Đặt cọc</label>
                        <input required type="text" class="form-control money" id="deposit"
                            name="description[payment][deposit]" placeholder="2,000,000 VND"
                            value="{{ old('description.payment.deposit', $room->description['payment']['deposit']) }}">
                        <small class="text-muted">
                            Số tiền đặt cọc để thuê phòng
                        </small>
                    </div>
                    <div class="form-group col-12">
                        <label for="ultilities">Nội thất, tiện ích</label>
                        @foreach (config('const.room.ultilities') as $id => $ultility)
                        <div class="custom-control custom-checkbox mb-2">
                            <input type="checkbox" class="custom-control-input" name="description[ultilities][]"
                                id="ultilities-{{$id}}" value="{{$id}}" @if(in_array($id,
                                old('description.ultilities',$room->description['ultilities'])))
                            checked
                            @endif>
                            <label class="custom-control-label" for="ultilities-{{$id}}">{{$ultility}}</label>
                        </div>
                        @endforeach
                    </div>
                    @foreach ($room->images as $image)
                    <input type="hidden" name="images[]" value="{{ $image }}" data-image-path="{{ $image }}">
                    @endforeach
                </form>
                <div class="col-12 mb-3">
                    <label for="map_address">
                        Ảnh minh hoạ
                        <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top"
                            title="Kích thước ảnh không quá 4 Mb và phải là định dạng hình ảnh"></i>
                    </label>
                    <form action="{{ route('manager.apartments.images.upload') }}" class="UppyForm" method="POST">
                        <input type="file" id="images" name="images[]" multiple>
                    </form>
                    <div class="UppyProgressBar mb-3"></div>
                    <div class="responsive overflow-auto pt-1">
                        <div class="uploaded-files mb-3 mt-3" style="height: 100px;min-width: max-content;">
                            @foreach ($room->images as $image)
                            <div class="image-wrapper h-100 position-relative d-inline-block">
                                <span class='remove-image-marker position-absolute' data-image-path="{{$image}}">
                                    <i class="fas fa-times-circle"></i>
                                </span>
                                <img class="uploaded-image mr-3 mb-3" src="{{$image}}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary float-right" type="submit" form="form-update-room">Lưu lại</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
            $('.money').mask('000,000,000,000,000', { reverse: true });
        })
</script>
<script>
    var uppy = Uppy.Core({
        locale: Uppy.locales.vi_VN,
        debug: true,
        autoProceed: true,
        restrictions: {
            // maxFileSize: 1000000,
            allowedFileTypes: ['image/*', 'video/*']
        }
    });

    uppy.use(Uppy.FileInput, {
        target: '.UppyForm',
        replaceTargetContent: true
    })

    uppy.use(Uppy.ProgressBar, {
        target: '.UppyProgressBar',
        hideAfterFinish: true
    })

    uppy.use(Uppy.XHRUpload, {
        endpoint: "{{ route('manager.apartments.images.upload') }}",
        formData: true,
        headers: {
            'X-CSRF-Token': " {{ csrf_token() }} "
        },
        fieldName: 'images[]'
    })

    uppy.on('upload-success', (file, res) => {
        let images = res.body.data;
        images.forEach(function (image) {
            $(`#form-update-room`).append(`<input type="hidden" name="images[]" value="${image}" data-image-path="${image}">`);
            $('.uploaded-files').append(templateImage(image));
        })
    })

    function templateImage(src) {
        return `<div class="image-wrapper h-100 position-relative d-inline-block">
                    <span class='remove-image-marker position-absolute' data-image-path="${src}">
                        <i class="fas fa-times-circle"></i>
                    </span>
                    <img class="uploaded-image mr-3 mb-3"
                        src="${src}">
                </div>`;
    }

</script>
<script>
    $('#form-update-room').submit(function (e) {
        $(`.money`).unmask();
    });
    $(document).on('click', '.remove-image-marker', function (e) {
        let imagePath = $(this).data('image-path');
        $(this).parent().remove();
        $(`input[data-image-path="${imagePath}"]`).remove();
        axios.delete('/uploaded/images', {
            params: {
                path: imagePath
            }
        })
    });
</script>
@endpush
