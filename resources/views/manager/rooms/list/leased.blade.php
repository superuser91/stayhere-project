@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        @if ($apartments->count() == 0)
        <p class="text-center">
            Không có phòng trọ nào đang có người thuê
        </p>
        @endif
        @foreach ($apartments as $apartment)
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h5 class="d-inline-block mr-1">{{ $apartment->title }}</h5>
                <small class="text-muted">
                    {{ $apartment->location['desc'] }}
                </small>
            </div>
            <div class="card-body" style="overflow-x:auto">
                <div class="card-deck my-auto" style="height: 150px; width: max-content;">
                    @foreach ($apartment->leasedRooms as $room)
                    <div class="room-item d-inline-block mr-2" style="width: 200px;">
                        <div class="card flex-column bg-light" style="height: 150px;">
                            <div class="text-center text-truncate d-inline-block px-3">
                                <b>{{$room->room_number}}</b>
                            </div>
                            <hr class="my-1">
                            <div class="mx-1 text-left flex-grow-1">
                                <p class="my-1">
                                    <i class="fas fa-square text-info"></i>
                                    Diện tích: {{$room->acreage}} (m<sup>2</sup>)
                                </p>
                                <p class="my-1">
                                    @if ($room->contract_id)
                                    <i class="fas fa-circle text-success"></i>
                                    <span>Đã cho thuê</span>
                                    @else
                                    <i class="fas fa-circle text-warning"></i>
                                    <span>Còn trống</span>
                                    @endif
                                </p>
                            </div>
                            <div class="text-right m-1 action-menu">
                                @if ($room->is_public == config('const.room.private_room'))
                                <a data-action="{{ route('manager.rooms.post.enable', $room->id) }}" data-enable="true" data-room-id="{{$room->id}}"
                                    class="btn btn-sm btn-success float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Đăng tin cho thuê phòng">
                                    <i class="fas fa-upload"></i>
                                </a>
                                @else
                                <a data-action="{{ route('manager.rooms.post.disable', $room->id) }}" data-room-id="{{$room->id}}"
                                    class="btn btn-sm btn-primary float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Tắt bài đăng">
                                    <i class="fas fa-history"></i>
                                </a>
                                @endif
                                <a href="{{ route('manager.rooms.show', $room->id) }}" class="btn btn-sm btn-info"
                                    data-toggle="tooltip" data-placement="top" title="Xem chi tiết">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <a href="{{ route('manager.rooms.edit', $room->id) }}" class="btn btn-sm btn-warning"
                                    data-toggle="tooltip" data-placement="top" title="Sửa thông tin phòng">
                                    <i class="fas fa-cog"></i>
                                </a>
                                @if (empty($room->contract_id))
                                <a class="btn btn-sm btn-danger btn-delete-room"
                                    data-action="{{route('manager.rooms.destroy', $room->id)}}" data-toggle="tooltip"
                                    data-placement="top" title="Xoá phòng">
                                    <i class="fas fa-trash"></i>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
