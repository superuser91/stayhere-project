@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        @if ($apartments->count() == 0)
        <p class="text-center">
            Không có phòng nào hiện còn trống!
        </p>
        @endif
        @foreach ($apartments as $apartment)
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h5 class="d-inline-block mr-1">{{ $apartment->title }}</h5>
                <small class="text-muted">
                    {{ $apartment->location['desc'] }}
                </small>
            </div>
            <div class="card-body" style="overflow-x:auto">
                <div class="card-deck my-auto" style="height: 150px; width: max-content;">
                    @foreach ($apartment->emptyRooms as $room)
                    <div class="room-item d-inline-block mr-2" style="width: 200px;">
                        <div class="card flex-column bg-light" style="height: 150px;">
                            <div class="text-center text-truncate d-inline-block px-3">
                                <b>{{$room->room_number}}</b>
                            </div>
                            <hr class="my-1">
                            <div class="mx-1 text-left flex-grow-1">
                                <p class="my-1">
                                    <i class="fas fa-square text-info"></i>
                                    Diện tích: {{$room->acreage}} (m<sup>2</sup>)
                                </p>
                                <p class="my-1">
                                    @if ($room->contract_id)
                                    <i class="fas fa-circle text-success"></i>
                                    <span>Đã cho thuê</span>
                                    @else
                                    <i class="fas fa-circle text-warning"></i>
                                    <span>Còn trống</span>
                                    @endif
                                </p>
                            </div>
                            <div class="text-right m-1">
                                @if ($room->is_public == config('const.room.private_room'))
                                <a data-action="{{ route('manager.rooms.post.enable', $room->id) }}" data-enable="true"
                                    data-room-id="{{$room->id}}"
                                    class="btn btn-sm btn-success float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Đăng tin cho thuê phòng">
                                    <i class="fas fa-upload"></i>
                                </a>
                                @else
                                <a data-action="{{ route('manager.rooms.post.disable', $room->id) }}"
                                    data-room-id="{{$room->id}}"
                                    class="btn btn-sm btn-primary float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Tắt bài đăng">
                                    <i class="fas fa-history"></i>
                                </a>
                                @endif
                                <a href="{{ route('manager.rooms.show', $room->id) }}" class="btn btn-sm btn-info"
                                    data-toggle="tooltip" data-placement="top" title="Xem chi tiết">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <a href="{{ route('manager.rooms.edit', $room->id) }}" class="btn btn-sm btn-warning"
                                    data-toggle="tooltip" data-placement="top" title="Sửa thông tin phòng">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <a class="btn btn-sm btn-danger btn-delete-room"
                                    data-action="{{route('manager.rooms.destroy', $room->id)}}" data-toggle="tooltip"
                                    data-placement="top" title="Xoá phòng">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<form method="POST" id="form-delete-room" action="#">
    @csrf
    @method('DELETE')
</form>
@endsection
@push('scripts')
<script>
    $(document).on('click', '.btn-delete-room', function (e) {
        e.preventDefault();
        let action = $(this).data('action');
        Swal.fire({
            title: 'Bạn có chắc chắn muốn xoá?',
            text: "Sau khi xoá sẽ không còn được hiển thị!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xoá!',
            focusCancel: true
        }).then((result) => {
            if (result.isConfirmed) {
                $('#form-delete-room').attr('action', action);
                $('#form-delete-room').submit();
            }
        })
    });

    $(document).on('click', '.btn-update-post', function (e) {
        let action = $(this).data('action');
        let isEnable = $(this).data('enable');
        let roomId = $(this).data('room-id');
        let parentElement = $(this).parent();
        $('[data-toggle="tooltip"]').tooltip('hide');
        $(this).remove();
        axios.post(action)
            .then(res => {
                Swal.fire({
                    icon: 'success',
                    text: 'Cập nhật trạng thái bài đăng thành công!',
                })

                if(isEnable){
                    $(parentElement).append(`
                                <a data-action="/rooms/d/${roomId}/posts/disable/ajax" data-room-id="${roomId}"
                                    class="btn btn-sm btn-primary float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Tắt bài đăng">
                                    <i class="fas fa-history"></i>
                                </a>
                    `);
                } else {
                    $(parentElement).append(`
                                <a data-action="/rooms/d/${roomId}/posts/enable/ajax"  data-room-id="${roomId}" data-enable="true"
                                    class="btn btn-sm btn-success float-left btn-update-post" data-toggle="tooltip"
                                    data-placement="top" title="Đăng tin cho thuê phòng">
                                    <i class="fas fa-upload"></i>
                                </a>
                    `);
                }
                $('[data-toggle="tooltip"]').tooltip();
            }).catch(err => {
                Swal.fire({
                    icon: 'error',
                    text: 'Cập nhật trạng thái bài đăng thất bại!',
                })
            })
    });
</script>
@endpush
