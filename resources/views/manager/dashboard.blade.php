@extends('manager.layouts.app')

@push('scripts')
@endpush

@section('content')
<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3>{{ $apartmentCount }}</h3>

                <p>Nhà trọ</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="{{ route('manager.apartments.index') }}" class="small-box-footer">Xem thêm <i
                    class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3>{{ $roomCount }}</h3>

                <p>Phòng trọ</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ route('manager.rooms.index') }}" class="small-box-footer">Xem thêm<i
                    class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>{{ $tenantCount }}</h3>

                <p>Người thuê phòng</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ route('manager.statistic.tenant') }}" class="small-box-footer">Xem thêm <i
                    class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>{{ $emptyRoomCount }}</h3>

                <p>Phòng còn trống</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">Xem thêm<i class="fas fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-md-7 col-12 connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-pills mr-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Chưa đóng tiền phòng</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#sales-chart" data-toggle="tab">Chưa đóng tiền phí sinh hoạt</a>
                    </li>
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content p-0">
                    <!-- Morris chart - Sales -->
                    <div class="chart tab-pane active overflow-auto" id="revenue-chart"
                        style="position: relative; height: 300px;overflow: auto">
                        <p>Danh sách phòng trọ chưa đóng tiền phòng </p>
                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Số phòng</th>
                                    <th>Tháng</th>
                                    <th>Số tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($notPaidInvoices['App\\Contract'] ?? []) == 0)
                                <tr>
                                    <td colspan="4" class="text-center">Danh sách trống</td>
                                </tr>
                                @endif
                                @foreach ($notPaidInvoices['App\\Contract'] ?? [] as $invoice)
                                <tr>
                                    <td>{{$loop->index + 1}}</td>
                                    <td>
                                        <a href="{{route('manager.statistic.invoice.detail', $invoice->id)}}">
                                            {{$invoice->room->room_number}}
                                        </a>
                                    </td>
                                    <td>
                                        {{ implode(', ', array_map(function($i){ return Carbon\Carbon::parse($i['month'])->format('m/Y');},$invoice->months)) }}
                                    </td>
                                    <td>{{number_format($invoice->amount)}} VND</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;overflow: auto">
                        <p>Danh sách phòng trọ chưa đóng sinh hoạt phí </p>
                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Số phòng</th>
                                    <th>Tháng</th>
                                    <th>Số tiền</th>
                                </tr>
                            </thead>
                            <tbody style="overflow: auto">
                                @if (count($notPaidInvoices['App\\LivingCost'] ?? []) == 0)
                                <tr>
                                    <td colspan="4" class="text-center">Danh sách trống</td>
                                </tr>
                                @endif
                                @foreach ($notPaidInvoices['App\\LivingCost'] ?? [] as $invoice)
                                <tr>
                                    <td>{{$loop->index + 1}}</td>
                                    <td>
                                        <a href="{{route('manager.statistic.invoice.detail', $invoice->id)}}">
                                            {{$invoice->room->room_number}}
                                        </a>
                                    </td>
                                    <td>
                                        {{ implode(', ', array_map(function($i){ return Carbon\Carbon::parse($i['month'])->format('m/Y');},$invoice->months)) }}
                                    </td>
                                    <td>{{number_format($invoice->amount)}} VND</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-md-5 col-12 connectedSortable">

        <!-- Calendar -->
        <div class="card bg-gradient-success">
            <div class="card-header border-0">

                <h3 class="card-title">
                    <i class="far fa-calendar-alt"></i>
                    Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                    <!-- button with a dropdown -->
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"
                            data-offset="-52">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a href="#" class="dropdown-item">Add new event</a>
                            <a href="#" class="dropdown-item">Clear events</a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">View calendar</a>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
                <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
    <!-- right col -->
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#calendar').datetimepicker({
            format: 'L',
            inline: true
        })
    });
</script>
@endpush
