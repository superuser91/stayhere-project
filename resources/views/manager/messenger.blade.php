@extends('manager.layouts.app')

@section('pageHeader')
Tin nhắn
@endsection

@push('head')
<style>
    .right>.direct-chat-text::after {
        border-left-color: #4e57d6;
    }

    .right>.direct-chat-text {
        background-color: #4e57d6;
        border-color: #4e57d6;
        color: #ffffff;
    }

    .active-conversation {
        background-color: #e8edef;
    }

    .direct-chat-text {
        max-width: 75%;
    }
</style>
@endpush

@section('content')
<div class="d-none" id="conversation-data" data-conversation="{{ $conversations->first() }}"></div>
<div class="d-none" id="conversations-data" data-conversations="{{ json_encode($conversations) }}"></div>
<div class="d-none" id="user-id-data" data-user-id="{{ json_encode(auth()->id()) }}"></div>
<div class="d-none" id="user-avatar-data" data-user-avatar="{{ auth()->user()->avatar }}"></div>
<div class="row" style="min-height: 50vh" id="app">
    <div class="col-12 d-block d-sm-block d-md-none d-lg-none d-xl-none">
        <div class="card card-outline card-primary h-100 card-contact-list">
            <div class="card-header">
                <div class="input-group input-group-sm">
                    <input v-model="searchInput" type="text" class="form-control search-contact-input"
                        placeholder="Tìm kiếm" @input="triggerSearch">
                    <div class="input-group-append">
                        <div class="btn btn-info">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                </div>
                <!-- /.card-tools -->
            </div>
            <div class="card-body p-0">
                <div class="overflow-auto contact-list h-100">
                    <ul class="contacts-list main-contact">
                        <li v-for="conversation in newestConversations" class="contact-item">
                            <a class="goto-conversation" :href="conversation.link">
                                <img class="contacts-list-img" :src="conversation.participants[0].messagable.avatar"
                                    alt="">
                                <div class="contacts-list-info pl-2">
                                    <span class="contacts-list-name text-dark">
                                        @{{ conversation.participants[0].messagable.name }}
                                        <small class="contacts-list-date float-right">
                                            @{{ moment(conversation?.last_message?.created_at).format('Y-MM-DD') ?? ''
                                            }}
                                        </small>
                                    </span>
                                    <p class="contacts-list-msg text-truncate">
                                        @{{ conversation?.last_message?.data?.content }}
                                    </p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="search-list d-none">
                    <a class="btn btn-sm btn-primary m-2 back-to-contact-list">
                        ❮ Quay lại
                    </a>
                    <div class="overflow-auto h-100">
                        <ul class="contacts-list search-result contact-search-result">
                            <li v-for="contact in orderedSearchList"
                                class="contact-item d-block d-sm-block d-md-none d-lg-none d-xl-none">
                                <a class="d-flex contact-search-result-item"
                                    @click="createConversation(contact.objectID)">
                                    <img class="contacts-list-img" :src="contact.avatar">
                                    <div class="contacts-list-info align-self-center ml-2">
                                        <span class="contacts-list-name text-dark">
                                            @{{contact.name}}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4  d-none d-sm-none d-md-block d-lg-block d-xl-block" style="max-width: 300px;">
        <div class="card card-outline card-primary h-100 card-contact-list">
            <div class="card-header">
                <div class="input-group input-group-sm">
                    <input v-model="searchInput" type="text" class="form-control search-contact-input"
                        placeholder="Tìm kiếm" @input="triggerSearch">
                    <div class="input-group-append">
                        <div class="btn btn-info">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                </div>
                <!-- /.card-tools -->
            </div>
            <div class="card-body p-0" style="height: 50vh;">
                <div class="overflow-auto contact-list h-100">
                    <ul class="contacts-list main-contact">
                        <li v-for="contact in newestConversations" @click="changeConversation(contact.id)"
                            :class="[contact.id === conversation.id ? 'active-conversation' : '','contact-item']">
                            <a class="goto-conversation">
                                <img class="contacts-list-img" :src="contact.participants[0].messagable.avatar" alt="">
                                <div class="contacts-list-info pl-2">
                                    <span class="contacts-list-name text-dark">
                                        @{{ contact.participants[0].messagable.name }}
                                        <small class="contacts-list-date float-right">
                                            @{{ moment(contact?.last_message?.created_at).format('Y-MM-DD') }}
                                        </small>
                                    </span>
                                    <p class="contacts-list-msg text-truncate">
                                        @{{ contact?.last_message?.data.content }}
                                    </p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="search-list d-none">
                    <a class="btn btn-sm btn-primary m-2 back-to-contact-list">
                        ❮ Quay lại
                    </a>
                    <div class="overflow-auto h-100">
                        <ul class="contacts-list search-result contact-search-result">
                            <li v-for="contact in orderedSearchList" @click="createConversation(contact.objectID)"
                                class="contact-item d-none d-sm-none d-md-block d-lg-block d-xl-block">
                                <a class="d-flex contact-search-result-item" href="javascript:void(0)">
                                    <img class="contacts-list-img" :src="contact.avatar">
                                    <div class="contacts-list-info align-self-center ml-2">
                                        <span class="contacts-list-name text-dark">
                                            @{{contact.name}}
                                        </span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col -->
    <div class="col d-none d-md-block">
        <div class="card card-primary card-outline mb-0">
            <div class="card-header">
                <h3 class="card-title" v-if="conversation">
                    @{{ conversation.participants[0].messagable.name ?? Inbox }}
                </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="direct-chat-messages" id="direct-chat-messages" style="height: 50vh;">
                    <div v-for="message in currentConversation"
                        :class="[message.messagable_type == userType && message.messagable_id == userId ? 'right' : '', 'direct-chat-msg']">
                        <img class="direct-chat-img" :src="message.avatar" alt="">
                        <div
                            :class="[message.messagable_type == userType && message.messagable_id == userId ? 'float-right mr-3' : 'float-left ml-3' ,'direct-chat-text']">
                            @{{ message.data.content }}
                        </div>
                        <div class="clearfix"></div>
                        <small>
                            <span
                                :class="[message.messagable_type == userType && message.messagable_id == userId ? 'float-right mr-4 pr-4' : 'float-left ml-4 pl-4' ,'direct-chat-timestamp']">
                                @{{ message.created_at }}
                            </span>
                        </small>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <form id="chat-form" @submit.prevent="sendMessage">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Nhập tin nhắn ..." class="form-control">
                        <span class="input-group-append">
                            <button type="submit" class="btn btn-primary" id="send-message">
                                <i class="fas fa-paper-plane mr-2"></i>
                                Gửi</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
@push('scripts')
<script>
    notifyNewMessage = false;
    const userType = 'App\\Manager';
    var userId = $('#user-id-data').data('user-id');
    var userAvatar = $('#user-avatar-data').data('user-avatar');
    var conversationList = $('#conversations-data').data('conversations');
    var conversation = $('#conversation-data').data('conversation');
    var conversations = conversationList.map(function (conversation) {
        conversation.link = `/conversations/d/${conversation.id}`;
        return conversation;
    });
    new Vue({
        el: '#app',
        data: {
            conversations: conversations,
            conversation: conversation,
            userType: userType,
            userId: userId,
            searchList: [],
            searchInput: ''
        },
        created() {
            Echo.private('manager.' + userId)
                .notification(function (notification) {
                    if (notification.type == "App\\Notifications\\NewMessageNotification") {
                        let i = this.conversations.findIndex(conversation => conversation.id == notification.message.conversation_id);
                        if (i === undefined) {
                            return;
                        }
                        let conversation = this.conversations[i];
                        conversation.last_message = notification.message;
                        conversation.recent_messages.push(notification.message);
                        conversation.updated_at = moment().format('Y-MM-DD HH:mm:ss')
                        this.conversations.splice(i, 1, conversation);
                        setTimeout(scrollMessage, 100);
                    }
                });
        },
        methods: {
            changeConversation: function (conversationId) {
                if (conversationId == this.conversation.id) return;
                this.conversation = this.conversations.find(conversation => conversation.id == conversationId);
                setTimeout(scrollMessage, 100);
            },
            sendMessage: function () {
                let message = $('input[name="message"]').val();
                setTimeout(scrollMessage, 100);
                if (message && this.conversation.id) {
                    $('input[name="message"]').val('');
                    this.conversation.recent_messages.push({
                        avatar: userAvatar,
                        messagable_type: userType,
                        messagable_id: userId,
                        data: {
                            type: 'text',
                            content: message
                        },
                        created_at: moment().format('Y-MM-DD HH:mm:ss')
                    });
                    axios.post(`/conversations/d/${this.conversation.id}`, {
                        "type": "text",
                        'content': message
                    })
                        .then((response) => {
                            scrollMessage();
                            this.conversation.last_message = response.data.data;
                            this.conversation.updated_at = moment().format('Y-MM-DD HH:mm:ss');
                            let i = this.conversations.findIndex(conversation => conversation.id == this.conversation.id);
                            this.conversations.splice(i, 1, this.conversation);
                        })
                        .catch((error) => {
                            console.log(error)
                        });
                }
            },
            triggerSearch: _.debounce(function () {
                axios.get('/messagables', {
                    params: {
                        search: this.searchInput
                    }
                }).then((res) => {
                    console.log(res.data.data.hits)
                    this.searchList = res.data.data.hits;
                }).catch(err => {
                    console.log(err)
                })
            }, 100),
            createConversation: function (messagable) {
                axios.post('/conversations', {
                    messagable: messagable
                }).then(res => {
                    let i = this.conversations.findIndex(conversation => conversation.id == res.data.conversation.id);
                    if (i != -1) {
                        this.conversations.splice(i, 1, res.data.conversation)
                    } else {
                        this.conversations.push(res.data.conversation)
                    }
                    this.conversation = res.data.conversation
                }).catch(err => {
                    Swal.fire(
                        'Lỗi!',
                        'Không thể tạo cuộc hội thoại với người này!',
                        'error'
                    )
                })
            }
        },
        computed: {
            newestConversations() {
                if (this.conversation) {
                    return this.conversations.sort((a, b) => {
                        if (a.updated_at > b.updated_at) return -1;
                        return 1;
                    });
                }
            },
            currentConversation() {
                if (this.conversation) {
                    return this.conversation.recent_messages.sort((a, b) => {
                        if (a.created_at > b.created_at) return 1;
                        return -1;
                    });
                }
            },
            orderedSearchList() {
                return this.searchList;
            }
        }
    });

</script>
<script>
    $(function () {
        scrollMessage();
    });

    function pushMessageBox(messageNotification) {
        if (conversationId == messageNotification.message.conversation_id) {
            $('.direct-chat-messages').append(responseTemplate(messageNotification));
        }
    }

    function responseTemplate(data) {
        return `<div class="direct-chat-msg">
            <img class="direct-chat-img"
                src="${data.sender.avatar ?? '/images/avatar.png'}"
                alt="">
            <div class="direct-chat-text float-left ml-3">
                ${data.message.data.content}
            </div>
            <div class="clearfix"></div>
            <small>
                <span class="direct-chat-timestamp float-left ml-4 pl-4">${data.message.created_at}</span>
            </small>
        </div>`;
    }

    function scrollMessage() {
        if (autoScrollBottomWhenReceiveNewMessage) {
            var element = document.getElementById('direct-chat-messages');
            element.scrollTop = element.scrollHeight;
        }
    }

</script>
<script>
    // Search messagable
    $('.search-contact-input').focus(function () {
        $('.contact-list').addClass('d-none');
        $('.search-list').removeClass('d-none');
    });

    $('.back-to-contact-list').click(function () {
        $('.contact-list').removeClass('d-none');
        $('.search-list').addClass('d-none');
    });

</script>
@endpush
