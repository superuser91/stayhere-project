@extends('manager.layouts.app')

@section('pageHeader')
Inbox
@endsection

@push('head')
<style>
    .right>.direct-chat-text::after {
        border-left-color: #4e57d6;
    }

    .right>.direct-chat-text {
        background-color: #4e57d6;
        border-color: #4e57d6;
        color: #ffffff;
    }

    .direct-chat-text {
        max-width: 75%;
    }

    .active-conversation {
        background-color: #4e57d6;
    }
</style>
@endpush

@section('content')
<div class="d-none" id="user-id" data-user-id="{{ auth('manager')->id() }}"></div>
<div class="d-none" id="conversation-id" data-conversation-id="{{ $conversation->id ?? '' }}"></div>
<div class="row" style="min-height: 50vh">
    <div class="col-12">
        <div class="card card-primary card-outline mb-0">
            <div class="card-header">
                <h3 class="card-title" id="chat-with">{{ $conversation->participants->first()->name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <div class="direct-chat-messages" id="direct-chat-messages" style="height: 50vh;">
                    @foreach ($conversation->recentMessages->sortBy('created_at') as $message)
                    @if ($message->messagable_type == "App\\Manager" && $message->messagable_id ==
                    auth('manager')->id())
                    <div class="direct-chat-msg right">
                        <img class="direct-chat-img" src="{{ auth()->user()->avatar ?? '/images/avatar.png' }}"
                            alt="user">
                        <div class="direct-chat-text float-right mr-3">
                            {{ $message->data['content'] }}
                        </div>
                        <div class="clearfix"></div>
                        <small>
                            <span class="direct-chat-timestamp float-right mr-4 pr-4">
                                {{$message->created_at->format('H:i:s d/m/Y') }}
                            </span>
                        </small>
                    </div>
                    @else
                    <div class="direct-chat-msg">
                        <img class="direct-chat-img" src="{{ $conversation->participants->first()->avatar  ?? '/images/avatar.png' }}" alt="">
                        <div class="direct-chat-text float-left ml-3">
                            {{ $message->data['content'] }}
                        </div>
                        <div class="clearfix"></div>
                        <small>
                            <span class="direct-chat-timestamp float-left ml-4 pl-4">{{
                                $message->created_at->format('H:i:s
                                d/m/Y') }}</span>
                        </small>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <form id="chat-form">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Nhập tin nhắn ..." class="form-control">
                        <span class="input-group-append">
                            <button type="submit" class="btn btn-primary" id="send-message">
                                <i class="fas fa-paper-plane mr-2"></i>
                                Gửi</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
@push('scripts')
<script>
    var userId = $('#user-id').data('user-id');
    var conversationId = $('#conversation-id').data('conversation-id');
    $(document).ready(function () {
        scrollMessage();
    });
</script>
<script>
    var autoScrollBottomWhenReceiveNewMessage = true;

    Echo.private('manager.' + userId)
        .notification((notification) => {
            if (notification.type == "App\\Notifications\\NewMessageNotification") {
                pushMessageBox(notification);
                scrollMessage();
            }
        });

    function pushMessageBox(messageNotification) {
        if (conversationId == messageNotification.message.conversation_id) {
            $('.direct-chat-messages').append(responseTemplate(messageNotification));
        }
    }


    function responseTemplate(data) {
        return `<div class="direct-chat-msg">
            <img class="direct-chat-img"
                src="${data.sender.avatar ?? '/images/avatar.png'}"
                alt="">
            <div class="direct-chat-text float-left ml-3">
                ${data.message.data.content}
            </div>
            <div class="clearfix"></div>
            <small>
                <span class="direct-chat-timestamp float-left ml-4 pl-4">${data.message.created_at}</span>
            </small>
        </div>`;
    }

    function messageTemplate(message) {
        return `<div class="direct-chat-msg right">
                    <img class="direct-chat-img" src="{{ auth('manager')->user()->avatar ?? '/images/avatar.png' }}" alt="">
                    <div class="direct-chat-text float-right mr-3">
                        ${message}
                    </div>
                    <div class="clearfix"></div>
                    <small>
                        <span class="direct-chat-timestamp float-right mr-4 pr-4">${moment().format('YYYY-MM-DD, h:mm:ss')}</span>
                    </small>
                </div>`;
    }

    $("#chat-form").submit(function (e) {
        e.preventDefault();

        let message = $('input[name="message"]').val();

        if (message && conversationId) {
            $('.direct-chat-messages').append(messageTemplate(message));

            $('input[name="message"]').val('');

            axios.post(`/conversations/d/${conversationId}`, {
                "type": "text",
                'content': message
            })
                .then(function (response) {
                })
                .catch(function (error) {
                    console.log(err)
                });

            scrollMessage();
        }
    });

    function scrollMessage() {
        if (autoScrollBottomWhenReceiveNewMessage) {
            var element = document.getElementById('direct-chat-messages');
            element.scrollTop = element.scrollHeight;
        }
    }

</script>
@endpush
