@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Tạo hoá đơn thu tiền sinh hoạt phí - dịch vụ</div>
            <div class="card-body">
                <form method="POST" class="px-0" id="form-create-invoice"
                    action="{{ route('manager.management.living.invoices.store', $room->id) }}">
                    @csrf
                    <div class="col-12 pl-0 pr-2">
                        <label for="electricity_consumption">Phòng</label>
                        {{ $room->room_number}}
                        <br>
                        <label for="">Địa chỉ: </label>
                        {{ $room->apartment->location['desc'] }}
                        <br>
                    </div>
                    <div class="form-group col-12 col-md-4 pl-0 pr-2">
                        <label for="month">Thanh toán cho tháng</label>
                        <input required type="text" class="form-control datepicker" id="month" name="month">
                    </div>
                    <div class="row">
                        <div class="form-group col-12 col-md-4">
                            <label for="electricity_consumption">Số đồng hồ điện</label>
                            <input required type="number" class="form-control" id="electricity_consumption"
                                name="electricity_consumption">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="electricity_consumption_diff">Chênh lệnh kì trước</label>
                            <input required type="number" class="form-control" id="electricity_consumption_diff"
                                name="electricity_consumption_diff">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="electricity_consumption_money">Tiền điện cần thanh toán</label>
                            <input required type="text" class="form-control money" id="electricity_consumption_money"
                                name="electricity_consumption_money">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="water_consumption">Số đồng hồ nước</label>
                            <input required type="number" class="form-control" id="water_consumption"
                                name="water_consumption">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="water_consumption_diff">Chênh lệnh kì trước</label>
                            <input required type="number" class="form-control" id="water_consumption_diff"
                                name="water_consumption_diff">
                        </div>
                        <div class="form-group col-12 col-md-4">
                            <label for="water_consumption_money">Tiền nước cần thanh toán</label>
                            <input required type="text" class="form-control money" id="water_consumption_money"
                                name="water_consumption_money">
                        </div>

                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success float-right" form="form-create-invoice">Gửi thông
                    báo
                </button>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">Danh sách hoá đơn thu sinh hoạt phí gần đây</div>
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover text-truncate">
                    <colgroup>
                        <col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tháng</th>
                            <th>Số tiền</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($room->recentLivingInvoices->count() == 0)
                        <tr>
                            <td colspan="4" class="text-center">Không có hoá đơn nào tạo gần đây</td>
                        </tr>
                        @endif
                        @foreach ($room->recentLivingInvoices as $invoice)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>
                                {{ implode(', ', array_map(function($i){ return Carbon\Carbon::parse($i['month'])->format('m/Y');},$invoice->months)) }}
                            </td>
                            <td>{{ number_format($invoice->amount) }}</td>
                            <td>
                                @empty($invoice->paid_at)
                                {{ config('const.invoice.statuses.not_paid.text') }}
                                @else
                                {{ config('const.invoice.statuses.paid.text') }}
                                @endempty
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $.fn.datepicker.dates['vi'] = {
            days: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            daysShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            months: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthsShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
            today: "Hôm nay",
            clear: "Xoá",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $('.money').mask('000,000,000,000,000', { reverse: true });
        $('.datepicker').datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            language: 'vi'
        });
    })

    $('form').submit(function () {
        $('.money').unmask();
    })
</script>
@endpush
