@extends('manager.layouts.app')

@section('content')
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-header">Tạo hoá đơn thu tiền phòng</div>
            <div class="card-body">
                <form method="POST" class="px-0" id="form-create-invoice"
                    action="{{ route('manager.management.house.invoices.store', $room->id) }}">
                    @csrf
                    <div class="col-12 pl-0 pr-2">
                        <label>Phòng: </label>
                        {{ $room->room_number}}
                    </div>
                    <div class="col-12 pl-0 pr-2">
                        <label>Địa chỉ: </label>
                        {{ $room->apartment->location['desc'] }}
                    </div>
                    <div class="col-12 pl-0 pr-2">
                        <label>Giá phòng từng tháng: </label>
                        {{ number_format($room->contract->room_info['price'] ?? 0) }} VND
                    </div>
                    <div class="col-12 pl-0 pr-2">
                        <label>Chu kỳ thanh toán: </label>
                        {{ $room->contract->room_info['description']['payment']['recurring'] }} tháng
                    </div>
                    <div class="col-12 pl-0 pr-2">
                        <label>Ngày bắt đầu hợp đồng thuê nhà: </label>
                        {{ $room->contract->valid_from->format('d/m/Y')  }}
                    </div>
                    <div class="col-12 col-md-4 pl-0 pr-2">
                        <label>Thanh toán cho tháng</label>
                        <div class="table-responsive mb-2">
                            <table class="table-bordered table-striped table-hover text-truncate text-center">
                                <thead>
                                    <tr>
                                        <th>Tháng</th>
                                        <th>Số tiền</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody  id="list-months">
                                    <tr>
                                        <td style="width:200px;">
                                            <input required type="text" class="form-control datepicker" name="months[]">
                                        </td>
                                        <td style="width:250px">
                                            <input required type="text" class="form-control money text-right"
                                                name="amounts[]" value="{{$room->contract->room_info['price'] ?? 0}}">
                                        </td style="width:100px">
                                        <td style="width:100px">
                                            <a class="btn btn-danger">Xoá</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a class="btn btn-primary btn-add-month">
                            <i class="fas fa-plus mr-2"></i>
                            Thêm
                        </a>
                    </div>
            </div>
            </form>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-header">Danh sách hoá đơn thu tiền phòng gần đây</div>
            <div class="card-body">
                <table class="table table-striped table-bordered table-hover text-truncate">
                    <colgroup>
                        <col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tháng</th>
                            <th>Số tiền</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($room->recentHouseInvoices->count() == 0)
                        <tr>
                            <td colspan="4" class="text-center">Không có hoá đơn nào tạo gần đây</td>
                        </tr>
                        @endif
                        @foreach ($room->recentHouseInvoices as $invoice)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>
                                {{ implode(', ', array_map(function($i){ return Carbon\Carbon::parse($i['month'])->format('m/Y');},$invoice->months)) }}
                            </td>
                            <td>{{ number_format($invoice->amount) }}</td>
                            <td>
                                @empty($invoice->paid_at)
                                {{ config('const.invoice.statuses.not_paid.text') }}
                                @else
                                {{ config('const.invoice.statuses.paid.text') }}
                                @endempty
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 mb-3">
        <button type="submit" class="btn btn-success float-right" form="form-create-invoice">
            Gửi thông báo
        </button>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $.fn.datepicker.dates['vi'] = {
            days: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            daysShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            months: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthsShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
            today: "Hôm nay",
            clear: "Xoá",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $('.money').mask('000,000,000,000,000', { reverse: true });
        $('.datepicker').datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            language: 'vi'
        });
    })

    $('form').submit(function () {
        $('.money').unmask();
    })

    $('.btn-add-month').click(function(e){
        $('#list-months').append(`
        <tr>
            <td style="width:200px;">
                <input required type="text" class="form-control datepicker" name="months[]">
            </td>
            <td style="width:250px">
                <input required type="text" class="form-control money text-right" name="amounts[]"
                    value="{{$room->contract->room_info['price'] ?? 0}}">
            </td style="width:100px">
            <td style="width:100px">
                <a class="btn btn-danger">Xoá</a>
            </td>
        </tr>
        `);
        $('.money').mask('000,000,000,000,000', { reverse: true });
        $('.datepicker').datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            language: 'vi'
        });
    })
</script>
@endpush
